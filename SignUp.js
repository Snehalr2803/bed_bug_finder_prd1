import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  ScrollView,
  NavigatorIOS,
  Alert,
  Image,
  
  ToastAndroid,
  ActivityIndicator,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  StatusBar
} from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import DatePicker from "react-native-datepicker";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
//import Icon from "react-native-vector-icons/FontAwesome";
import { withNavigation } from "react-navigation";
var ImagePicker = require("react-native-image-picker");
import { ScaledSheet } from "react-native-size-matters";
import { scale, verticalScale, moderateScale } from "react-native-size-matters";
import APISingletonClass, { SIGNUP_API, BASEURL,RECOVERY_API } from "./API";
import Spinner from "react-native-loading-spinner-overlay";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import AsyncStorage from "@react-native-community/async-storage";

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      loading: false,
      animating: false,
      loginInProcess: false,
      firstName: "",
      lastName: "",
      phonenumber: "",
      userName: "",
      confirmPassword: "",
      dateOfBirth: "",
      location: "",
      mobilevalidate: false,
      filePath: [],
      status: false,
      animating: false,
      newFileName: "",
      origionalNumber: "",
      countryCode:""
    };
  }
  mobilevalidate(text) {
    const reg = /^[0]?[789642351]\d{11}$/;
    if (reg.test(text) === false) {
      this.setState({
        mobilevalidate: false,
        phonenumber: text
      });
      return false;
    } else {
      this.setState({
        mobilevalidate: true,
        phonenumber: text
      });
      return true;
    }
  }

  storeData = async (Key, Value) => {
    try {
      await AsyncStorage.setItem(Key, Value);
    } catch (e) {
      // saving error
    }
  };

  handleCardNumber = text => {
    this.setState({ origionalNumber: text });
    var target = "";
    const input = text.replace(/\D/g, "").substring(0, 12);
    const zip = input.substring(0, 3);
    const middle = input.substring(3, 6);
    const last = input.substring(6, 12);
    console.log("Input Print", input);
    if (input.length > 6) {
      target = `(${zip}) - ${middle}  ${last}`;
    } else if (input.length > 3) {
      target = `(${zip}) - ${middle}`;
    } else if (input.length > 0) {
      target = `(${zip}`;
    }
    console.log("Target Print", target);
    this.setState({ phonenumber: target });
  };
  onTextChange(text) {
    this.mobilevalidate(text);
    var cleaned = ("" + text).replace(/\D/g, "");
    var match = cleaned.match(
      /^(1|2|3|4|5|6|7|8|9|0)?(\d{3})(\d{3})(\d{4})|(\d{2})$/
    );

    console.log("Match Array Print:", match);
    console.warn("Match Array Print:", match);

    if (match) {
      //  var number = [ "(", match[2], ") ", match[3], "-", match[4]].join(
      //     ""
      //   );

      var number = match[2] + "-" + match[3] + "-" + match[4];
      this.setState({
        origionalNumber: number,
        origionalNumber: match[0]
      });
      console.log("Phone Number Edit:", this.state.phonenumber);
      console.log("Phone Number Number:", number);
      console.warn("Phone Number Edit:", this.state.phonenumber);
      console.warn("Phone Number Number:", number);

      // console.warn("Match Array Print:", match);

      return;
    }

    this.setState({
      phonenumber: text
    });
  }
  updateProfileImage = imageID => {
    var options = {
      title: "Select Photo",
      cancelButtonTitle: "Cancel",
      takePhotoButtonTitle: "Take Photo",
      chooseFromLibraryButtonTitle: "Choose From Library",
      allowsEditing: true,
      returnBase64Image: false,
      returnIsVertical: false,
      quality: 0.8,
      noData: true,
      storageOptions: {
        skipBackup: true
      }
    };
    ImagePicker.showImagePicker(options, response => {
      // console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled image picker");
        return true;
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
        return true;
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
        alert(response.customButton);
      } else {
        var filname = response.uri.split("/");
        var index2 = filname.length;
        var filenameConcat = filname[index2 - 1].replace("'", "");

        const photo = {
          uri: response.uri,
          type: response.type,
          name: response.fileName
        };
        console.log("Photos URI", photo);
        this.setState({ filePath: response });
        this.setState({ newFileName: filenameConcat });
        const form = new FormData();
        form.append("profile_image", photo);
        form.append("user_id", this.state.userid);
        form.append("image_caption", this.state.editTxt);
        form.append("image_id", imageID);
        console.log("------------------------------" + form);
      }
    });
  };

  _onSignupSubmit = () => {
    console.log("SignUp Press");
    console.log(this.state.password);
    var myString = this.state.phonenumber.replace(/\D/g, "");
    console.log("Phone Number real", myString);

    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    let regCheck = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[\w~@#$%^&*+=`|{}:;!.?\"()\[\]-]{6,16}$/;

    if (this.state.firstName == "") {
      Alert.alert(
        "BBF",
        "First Name is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (this.state.lastName == "") {
      Alert.alert(
        "BBF",
        "Last Name is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (this.state.dateOfBirth == "") {
      Alert.alert(
        "BBF",
        "Date of Birth is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (myString == "") {
      Alert.alert(
        "BBF",
        "Phone Number is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (myString.length < 10) {
      Alert.alert(
        "BBF",
        "Please add valid mobile number.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (this.state.email == "") {
      Alert.alert(
        "BBF",
        "Email is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (reg.test(this.state.email) === false) {
      Alert.alert(
        "BBF",
        "Email Address is not valid.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (this.state.password == "") {
      Alert.alert(
        "BBF",
        "Password is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (regCheck.test(this.state.password) === false) {
      Alert.alert(
        "BBF",
        "Password should be at least one UpperCase, lowercase and Number with minimum 6 and Maximum 16 Characters.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (this.state.confirmPassword == "") {
      Alert.alert(
        "BBF",
        "Confirm Password is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (this.state.password != this.state.confirmPassword) {
      Alert.alert(
        "BBF",
        "Passwords do not match.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (this.state.filePath.uri == null) {
      //profileimageurl = this.state.filePath.uri;
      Alert.alert(
        "BBF",
        "Image is a required Field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else {
      // var filname = response.uri.split("/");
      // var index2 = filname.length;
      // var filename = filname[index2 - 1].replace("'", "");
      //let www_reg = /(www\.[\S]+(\b|$))/gim;

      const photo = {
        uri: this.state.filePath.uri,
        type: this.state.filePath.type,
        name: this.state.newFileName
      };

      console.log("FilePath", this.state.filePath.uri);
      const form = new FormData();
      // console.log("Form", form);
      // form.append("Photo", photo);
      form.append("FirstName", this.state.firstName);
      form.append("LastName", this.state.lastName);
      form.append("Email", this.state.email);
      form.append("PhoneNumber", myString);
      form.append("UserName", this.state.email);
      form.append("Password", this.state.password);
      form.append("dob", this.state.dateOfBirth);
      form.append("RoleId", 1);
      form.append("Location", this.state.location);
      // form.append("Lat", "17.4375");
      // form.append("Lan", "78.4482");
      form.append("ProfileImage", photo);
      console.log("FormData", form);
      // this.setState({
      //   animating: true
      // });
     let body = {
      "FirstName":this.state.firstName,
      "LastName": this.state.lastName,
      "Email":this.state.email,
      "PhoneNumber":myString,
      "UserName":this.state.email,
      "Password":this.state.password,
      "dob":this.state.dateOfBirth,
      "RoleId":1,
      "Location":this.state.location,
      "ProfileImage":photo
     }
      this.storeData("SignUpDataBody",JSON.stringify(body))
      this.storeData("PhoneNumberSignUp",this.state.phonenumber)
      
        // this.setState({
        //   loginInProcess: true,
        //   animating: true
        // });
//         let body1 = {};
//         console.log("Body Print:");
//         console.log(body1);
//           console.warn("API:-",RECOVERY_API)
//         APISingletonClass.getInstance().postMethodServiceHandler(
//           RECOVERY_API + "?email=" + this.state.email,
//           body1,
//           "",
//           responce => {
//             let responseJson = JSON.parse(responce);
//             console.log("responseJson forgot", responseJson);
//             //console.warn("_loginServiceHandler " + responseJson.status)
//             this.setState({
//               animating: false
//             });
//             if (responseJson.Status == true) {
//  setTimeout(() => {
//                 Alert.alert(
//                   "BBF",
//                   "User Already Exist Please try another email Id.",
//                   [
//                     {
//                       text: "OK",
//                       onPress: () =>
//                         console.log("Ok")
//                     }
//                   ],
//                   { cancelable: false }
//                 );
//               }, 200);
//             }else{
//            this.props.navigation.navigate("OtpScreen", {
//                 // BodyPass:body  
//                  })
             
//             } 
//           },
//           error => {
//             this.setState({
//               loading: false
//             });
//                           this.setState({
//           animating: false
//         });
//          setTimeout(() => {
//               Alert.alert(
//                 "BBF",
//                 "Something Went wrong ,Please try again.",
//                 [
//                   {
//                     text: "OK",
//                     onPress: () => console.log("Dislike.")
//                   }
//                 ],
//                 { cancelable: false }
//               );
//             }, 200);

//             console.warn("_loginServiceHandler error " + error);
//           }
//         );
      
// this.props.navigation.navigate("OtpScreen", {
//              // BodyPass:body  
//               })
this.setState({
  animating: true
});
      console.log("Body Added:", body);
      console.log("API:-", BASEURL + "/api/User/CreateUser");
      fetch(BASEURL + "/api/User/CreateUser", {
        method: "POST",
        // headers: {
        //   "Content-Type": "multipart/form-data;"
        // },
        body: form
      })
        .then(response => {
          return response.json();
        })
        .then(responseData => {
          this.setState({
            animating: false
          });
          var res = JSON.stringify(responseData);
          console.log("Response Data", responseData);
          if (responseData.Status == true) {
            // Alert.alert(
            //   "BBF",
            //   "Registration Successfully Completed.",
            //   [{ text: "OK", onPress: () => this.props.navigation.goBack() }],
            //   { cancelable: false }
            // );

            setTimeout(() => {
              Alert.alert(
                "BBF",
                "Registration Successfully Completed.",
                [
                  {
                    text: "OK",
                    onPress: () => this.props.navigation.goBack()
                  }
                ],
                { cancelable: false }
              );
            }, 200);
          } else {
            setTimeout(() => {
              Alert.alert(
                "BBF",
                responseData.ResponseException.ExceptionMessage,
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);
          }

          return responseData.state;
        })
        .catch(error => {
          console.error(error);
                        this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);

        });
    }
  };

  loginOptions() {
    this.props.navigation.navigate("eulaSetting", {
      DataHomeComment: ""
    });
  }
  render() {
    let deviceWidth = Dimensions.get("window").width;
    var now = new Date(),
      age = new Date(now.getFullYear() - 18, now.getMonth());
    var profileimageurl = "";
    if (this.state.filePath.uri != null) {
      profileimageurl = this.state.filePath.uri;
    }
    return (
      <View style={{ backgroundColor: "gray" }}>
        <StatusBar hidden={true} />
        <Spinner
          visible={this.state.animating}
          //textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <ImageBackground
          source={require("./Assets/main_background.png")}
          size={28}
          // borderRadius={15}
          style={{
            height: Dimensions.get("window").height,
            width: Dimensions.get("window").width,
            resizeMode: "contain"
          }}
        >
          <KeyboardAwareScrollView>
            <View style={{ marginTop: moderateScale(10) }}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between"
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    width: 230,
                    left: 15,
                    backgroundColor: "transparent"
                  }}
                >
                  <TouchableOpacity
                    onPress={() => this.props.navigation.goBack()}
                  >
                    <Image
                      source={require("./Assets/back.png")}
                      size={28}
                      borderRadius={15}
                      //onPress={this.props.navigation.goBack()}
                      style={{
                        height: 60,
                        width: 30,
                        alignSelf: "center",
                        resizeMode: "contain",
                        marginTop: 24,
                        backgroundColor: "transparent",
                        justifyContent: "flex-start",
                        backgroundColor: "transparent"
                      }}
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      fontSize: moderateScale(25),
                      color: "#FEFEFE",
                      textAlign: "center",
                      fontWeight: "200",
                      marginTop: 40,
                      marginLeft: 0,
                      marginRight: 20,
                      backgroundColor: "transparent",
                      fontFamily: "SegoeUI-Light"
                    }}
                  >
                    {" "}
                    S I G N U P
                  </Text>
                </View>
              </View>
            </View>

            <TouchableOpacity onPress={() => this.updateProfileImage()}>
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  marginTop: 20
                }}
              >
                <Image
                  style={styles.avatar}
                  marginTop={2}
                  //source={require("./Assets/BrianImage.jpeg")}
                  source={
                    profileimageurl == ""
                      ? require("./Assets/UserDefaultImg1.png")
                      : { uri: profileimageurl }
                  }
                  onPress={this.chooseFile}
                  resizeMode="cover"
                />
                <View
                  style={{
                    // flexDirection: "row",
                    backgroundColor: "green",
                    alignSelf: "center",
                    alignContent: "center",
                    alignItems: "center"
                  }}
                >
                  <Image
                    style={styles.avatar2}
                    source={require("./Assets/take_profile_picture.png")}
                    onPress={this.chooseFile}
                    resizeMode="cover"
                  />
                </View>
              </View>
            </TouchableOpacity>
            <View
              style={{
                flexDirection: "row",

                alignContent: "flex-start",
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
                backgroundColor: "transparent",
                borderColor: "#7098AA",
                borderWidth: 1,
                borderRadius: 10,
                height: moderateScale(40)
              }}
            >
              <TextInput
                placeholder="First Name"
                placeholderTextColor="#C0C0C0"
                onChangeText={firstName => {
                  this.setState({ firstName });
                }}
                value={this.state.firstName}
                returnKeyType={"next"}
                style={{
                  marginLeft: 10,
                  marginRight: 10,
                  textAlign: "left",
                  position: "absolute",
                  marginTop: 5,
                  color: "white",
                  fontSize: moderateScale(20),
                  width: Dimensions.get("window").width - scale(80),
                  fontFamily: "SegoeUI-Light"
                }}
              />
            </View>
            <View
              style={{
                flexDirection: "row",
                alignContent: "flex-start",
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
                backgroundColor: "transparent",
                borderColor: "#7098AA",
                borderWidth: 1,
                borderRadius: 10,
                height: moderateScale(40)
              }}
            >
              <TextInput
                placeholder="Last Name"
                placeholderTextColor="#C0C0C0"
                onChangeText={lastName => {
                  this.setState({ lastName });
                }}
                value={this.state.lastName}
                returnKeyType={"next"}
                style={{
                  marginLeft: 10,
                  marginRight: 10,
                  textAlign: "left",
                  position: "absolute",
                  marginTop: 5,
                  fontFamily: "SegoeUI-Light",
                  color: "white",
                  fontSize: moderateScale(20),
                  width: Dimensions.get("window").width - scale(80)
                }}
              />
            </View>
            <View
              style={{
                flexDirection: "row",

                alignContent: "flex-start",
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
                backgroundColor: "transparent",
                borderColor: "#7098AA",
                borderWidth: 1,
                borderRadius: 10,
                height: moderateScale(40)
                // backgroundColor: "yellow"
              }}
            >
              <DatePicker
                label="datepic"
                style={{
                  padding: 5,
                  width: deviceWidth - 50,
                  position: "absolute",
                  //backgroundColor: "red",
                  height: 40,
                  marginTop: -8
                }}
                date={this.state.dateOfBirth}
                placeholder="Date of Birth"
                //minDate={}
                //maxDate={age}
                mode="date"
                format="YYYY-MM-DD"
                showIcon={true}
                cancelBtnText="Cancel"
                confirmBtnText="Done"
                container={{
                  marginTop: 0
                }}
                customStyles={{
                  dateInput: {
                    alignItems: "flex-start",
                    borderWidth: 0,
                    borderBottomWidth: 0,
                    paddingLeft: 5,
                    width: "100%"
                    //padding: 5
                  },
                  dateText: {
                    width: "100%",
                    placeholderTextColor: "#C0C0C0",
                    color: "#FEFEFE",
                    fontSize: moderateScale(20),
                    fontFamily: "SegoeUI-Light"
                  },
                  placeholderText: {
                    color: "#C0C0C0",
                    fontSize: moderateScale(20),
                    fontFamily: "SegoeUI-Light"
                  },
                  color: "red"
                }}
                onDateChange={dateOfBirth => {
                  this.setState({ dateOfBirth: dateOfBirth });
                }}
                returnKeyType={"next"}
                //autoFocus={true}
                //onSubmitEditing={() => { this.focusTheField('gender'); }}
              />
            </View>
            {/* <View
              style={{
                flexDirection: "row",

                alignContent: "flex-start",
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
                backgroundColor: "transparent",
                borderColor: "#7098AA",
                borderWidth: 1,
                borderRadius: 10,
                height: moderateScale(40)
              }}
            >
              <TextInput
                placeholder="Country Code e.g.+1"
                placeholderTextColor="#C0C0C0"
                // onChangeText={phonenumber => {
                //   this.setState({ phonenumber }),
                //     this.mobilevalidate(phonenumber);
                // }}
                onChangeText={countryCode => {
                  this.setState({ countryCode });
                }}
                keyboardType="phone-pad"
                value={this.state.countryCode}
                returnKeyType={"next"}
                style={{
                  marginLeft: 10,
                  marginRight: 10,
                  textAlign: "left",
                  position: "absolute",
                  marginTop: 5,
                  fontFamily: "SegoeUI-Light",
                  color: "white",
                  fontSize: moderateScale(20),
                  width: Dimensions.get("window").width - scale(80)
                }}
              />
            </View> */}
            <View
              style={{
                flexDirection: "row",

                alignContent: "flex-start",
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
                backgroundColor: "transparent",
                borderColor: "#7098AA",
                borderWidth: 1,
                borderRadius: 10,
                height: moderateScale(40)
              }}
            >
              <TextInput
                placeholder="Phone Number"
                placeholderTextColor="#C0C0C0"
                // onChangeText={phonenumber => {
                //   this.setState({ phonenumber }),
                //     this.mobilevalidate(phonenumber);
                // }}
                onChangeText={text => this.handleCardNumber(text)}
                keyboardType="numeric"
                value={this.state.phonenumber}
                returnKeyType={"next"}
                style={{
                  marginLeft: 10,
                  marginRight: 10,
                  textAlign: "left",
                  position: "absolute",
                  marginTop: 5,
                  fontFamily: "SegoeUI-Light",
                  color: "white",
                  fontSize: moderateScale(20),
                  width: Dimensions.get("window").width - scale(80)
                }}
              />
            </View>
            <View
              style={{
                flexDirection: "row",

                alignContent: "flex-start",
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
                backgroundColor: "transparent",
                borderColor: "#7098AA",
                borderWidth: 1,
                borderRadius: 10,
                height: moderateScale(40)
              }}
            >
              <TextInput
                placeholder="Email"
                placeholderTextColor="#C0C0C0"
                onChangeText={email => {
                  this.setState({ email });
                  //this.emailCheck(email);
                }}
                value={this.state.email}
                returnKeyType={"next"}
                style={{
                  marginLeft: 10,
                  marginRight: 10,
                  textAlign: "left",
                  position: "absolute",
                  marginTop: 5,
                  color: "white",
                  fontSize: moderateScale(20),
                  width: Dimensions.get("window").width - scale(80),
                  fontFamily: "SegoeUI-Light"
                }}
              />
            </View>
            <View
              style={{
                flexDirection: "row",

                alignContent: "flex-start",
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
                backgroundColor: "transparent",
                borderColor: "#7098AA",
                borderWidth: 1,
                borderRadius: 10,
                height: moderateScale(40)
              }}
            >
              <TextInput
                placeholder="Location"
                placeholderTextColor="#C0C0C0"
                onChangeText={location => {
                  this.setState({ location });
                }}
                value={this.state.location}
                returnKeyType={"next"}
                style={{
                  marginLeft: 10,
                  marginRight: 10,
                  textAlign: "left",
                  position: "absolute",
                  marginTop: 5,
                  color: "white",
                  fontSize: moderateScale(20),
                  width: Dimensions.get("window").width - scale(80),
                  fontFamily: "SegoeUI-Light"
                }}
              />
            </View>
            {/* <GooglePlacesAutocomplete
              placeholder="Search"
              minLength={2} // minimum length of text to search
              autoFocus={false}
              returnKeyType={"search"}
              listViewDisplayed="false"
              fetchDetails={true}
              renderDescription={row =>
                row.description || row.formatted_address || row.name
              }
              onPress={(data, details = null) => {
                this.props.handler(data.description);
              }}
              getDefaultValue={() => {
                return ""; // text input default value
              }}
              query={{
                key: "AIzaSyC353JjU6e-eZ7rpM8Kap1H_2C6TvUOT1I",
                language: "en", // language of the results
                types: "(cities)" // default: 'geocode'
              }}
              styles={{
                description: {
                  fontWeight: "bold"
                },
                predefinedPlacesDescription: {
                  color: "#1faadb"
                },
                marginBottom: 100,
                marginLeft: 20,
                marginRight: 10
              }}
              enablePoweredByContainer={true}
              nearbyPlacesAPI="GoogleReverseGeocoding"
              GooglePlacesSearchQuery={{
                rankby: "distance",
                types: "food"
              }}
              filterReverseGeocodingByTypes={[
                "locality",
                "administrative_area_level_3"
              ]}
              debounce={200}
            /> */}
            <View
              style={{
                flexDirection: "row",
                alignContent: "flex-start",
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
                backgroundColor: "transparent",
                borderColor: "#7098AA",
                borderWidth: 1,
                borderRadius: 10,
                height: moderateScale(40)
              }}
            >
              <TextInput
                placeholder="Password"
                placeholderTextColor="#C0C0C0"
                onChangeText={password => {
                  this.setState({ password });
                }}
                secureTextEntry={true}
                value={this.state.password}
                returnKeyType={"next"}
                style={{
                  marginLeft: 10,
                  marginRight: 10,
                  textAlign: "left",
                  position: "absolute",
                  marginTop: 5,
                  color: "white",
                  fontSize: moderateScale(20),
                  width: Dimensions.get("window").width - scale(80),
                  fontFamily: "SegoeUI-Light"
                }}
              />
            </View>
            <View
              style={{
                flexDirection: "row",

                alignContent: "flex-start",
                marginLeft: 30,
                marginRight: 30,
                marginTop: 30,
                backgroundColor: "transparent",
                borderColor: "#7098AA",
                borderWidth: 1,
                borderRadius: 10,
                height: moderateScale(40)
              }}
            >
              <TextInput
                placeholder="Confirm Password"
                placeholderTextColor="#C0C0C0"
                onChangeText={confirmPassword => {
                  this.setState({ confirmPassword });
                }}
                secureTextEntry={true}
                value={this.state.confirmPassword}
                returnKeyType={"next"}
                style={{
                  marginLeft: 10,
                  marginRight: 10,
                  textAlign: "left",
                  position: "absolute",
                  marginTop: 5,
                  color: "white",
                  fontSize: moderateScale(20),
                  width: Dimensions.get("window").width - scale(80),
                  fontFamily: "SegoeUI-Light"
                }}
              />
            </View>

            <TouchableOpacity onPress={() => this._onSignupSubmit()}>
              <View
                style={{
                  height: 60,
                  fontSize: 20,
                  marginTop: 70,
                  marginBottom: 20,
                  textAlign: "center",
                  backgroundColor: "transparent",
                  borderWidth: 3,
                  borderColor: "#7098AA",
                  height: moderateScale(50),
                  borderRadius: moderateScale(35),
                  width: moderateScale(280),
                  alignItems: "center",
                  alignContent: "center",
                  alignSelf: "center"
                }}
              >
                <Text
                  style={{
                    height: moderateScale(60),
                    fontSize: moderateScale(27),
                    marginTop: moderateScale(0),
                    alignItems: "center",
                    width: scale(150),
                    textAlign: "center",
                    backgroundColor: "transparent",
                    height: moderateScale(50),
                    color: "white",
                    fontWeight: "200",
                    fontFamily: "SegoeUI-Light"
                  }}
                  //onPress={() => console.log("hello")}
                  color="black"
                  accessibilityLabel="Tap on Me"
                >
                  Sign Up !
                </Text>
              </View>
            </TouchableOpacity>
          </KeyboardAwareScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ecf0f1"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  avatar: {
    width: scale(170),
    height: scale(170),
    borderRadius: scale(85),
    borderWidth: scale(8),
    borderColor: "#7098AA",
    alignItems: "center"
  },
  avatar2: {
    width: 25,
    height: 22,
    marginTop: -30,
    marginLeft: 0,
    position: "absolute"
  },
  avatar3: {
    width: 25,
    height: 22,
    marginTop: -30,
    marginLeft: 20,
    position: "absolute"
  },
  spinnerTextStyle: {
    color: "#565656"
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 10
  },
  sharingtext: {
    color: "#AA2E2E",
    backgroundColor: "#fff",
    borderRadius: 14,
    borderColor: "#AA2E2E",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 18,
    width: 130,
    height: 30,
    alignItems: "center",
    alignContent: "center",
    textAlignVertical: "center"
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover" // or 'stretch'
  },
  footerWrapperlikeCommentShare: {
    flexWrap: "nowrap",

    flexDirection: "row",
    justifyContent: "space-evenly",
    marginLeft: 30,
    marginRight: 30
  },
  separatorlikeShare: {
    flex: 0,
    borderWidth: 1,
    borderColor: "#ECEDED"
  },
  imageSize2: {
    width: 60,
    height: 60,
    padding: 5,
    borderRadius: 10
  },
  headerTextComment: {
    marginTop: "03%",
    fontSize: 18,
    textAlign: "left",
    //margin: 20,
    fontWeight: "normal",
    marginRight: "0%",
    color: "#565656",

    marginLeft: "5%",
    borderWidth: 0.5,
    borderRadius: 7,
    borderColor: "#C0C0C0"
  }
});

export default withNavigation(SignUp);
