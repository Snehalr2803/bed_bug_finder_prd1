import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  ScrollView,
  NavigatorIOS,
  Alert,
  Image,
 
  ToastAndroid,
  ActivityIndicator,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  StatusBar,
  Switch,
  FlatList
} from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import APISingletonClass, { BASEURL, REPORT_INCIDENT } from "./API";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

import { withNavigation } from "react-navigation";
//import { Switch } from "react-native-switch";
import ToggleSwitch from "toggle-switch-react-native";
var ImagePicker = require("react-native-image-picker");
import Spinner from "react-native-loading-spinner-overlay";
import { ScaledSheet } from "react-native-size-matters";
import { scale, verticalScale, moderateScale } from "react-native-size-matters";
//import AutoSuggest from "autosuggest";
import AutoComplete from "react-native-autocomplete-select";
import { Dropdown } from "react-native-material-dropdown";
import AsyncStorage from "@react-native-community/async-storage";
class ReportImage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      loading: false,
      animating: false,
      loginInProcess: false,
      switchedBool: false,
      filePath1: [],
      filePath2: [],
      filePath3: [],
      filePath4: [],
      newFileName1: "",
      newFileName2: "",
      newFileName3: "",
      newFileName4: "",
      token: "",
      ProfilePic: "",
      mapRegion: null,
      lastLat: null,
      lastLong: null,
      locationKey: "",
      entertext: "",
      unitNameState: "",
      suggestions: [],
      idArray: [],
      selectedLocation: "",
      selectedIndex: "",
      idSaved: "",

      compayname: "",
      Country: "",
      locationname: "",
      statename: "",
      idCompny: "",
      zipCode: "",
      cityName: "",

      wtachList: [
        {
          name: "Brian J Virag Law Offices",
          avatar_url: "https://randomuser.me/api/portraits/med/men/77.jpg",
          subtitle: "16400 Ventura Blvd,Encino,CA"
        },
        {
          name: "ABCD Hotel Chain",
          avatar_url: "https://randomuser.me/api/portraits/med/men/77.jpg",
          subtitle: "1234 Ventura Blvd,Encino,CA"
        },
        {
          name: "EFGH Hotel Chain",
          avatar_url: "https://randomuser.me/api/portraits/med/men/77.jpg",
          subtitle: "5678 Ventura Blvd,Encino,CA"
        }
      ]
    };
    var watchID = null;
    // const onSelect = suggestion => {
    //   console.log(suggestion); // the pressed suggestion
    // };
    // const suggestions = [
    //   { text: "suggestion1", anotherProperty: "value" },
    //   { text: "suggestion2", anotherProperty: "value2" }
    // ];
  }
  onSelect = suggestion => {
    console.log(suggestion); // the pressed suggestion
  };
  updateProfileImage = imageID => {
    var options = {
      title: "Select Photo",
      cancelButtonTitle: "Cancel",
      takePhotoButtonTitle: "Take Photo",
      chooseFromLibraryButtonTitle: "Choose From Library",
      allowsEditing: true,
      returnBase64Image: false,
      returnIsVertical: false,
      quality: 0.8,
      noData: true,
      storageOptions: {
        skipBackup: true
      }
    };
    ImagePicker.showImagePicker(options, response => {
      // console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled image picker");
        return true;
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
        return true;
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
        alert(response.customButton);
      } else {
        var filname = response.uri.split("/");
        var index2 = filname.length;
        var filename12 = filname[index2 - 1].replace("'", "");
        const photo = {
          uri: response.uri,
          type: response.type,
          name: response.fileName
        };
        if (imageID == "1") {
          this.setState({ filePath1: response });
          this.setState({ newFileName1: filename12 });
        } else if (imageID == "2") {
          this.setState({ filePath2: response });
          this.setState({ newFileName2: filename12 });
        } else if (imageID == "3") {
          this.setState({ filePath3: response });
          this.setState({ newFileName3: filename12 });
        } else {
          this.setState({ filePath4: response });
          this.setState({ newFileName4: filename12 });
        }

        // const form = new FormData();
        // form.append("profile_image", photo);
        // form.append("user_id", this.state.userid);
        // form.append("image_caption", this.state.editTxt);
        // form.append("image_id", imageID);
        // console.log("------------------------------" + form);
        // fetch(
        //   "https://kuwboo.com/api/public/index.php/api/updateProfileImages",
        //   {
        //     body: form,
        //     method: "POST",
        //     headers: {
        //       "Content-Type": "multipart/form-data"
        //     }
        //   }
        // )
        //   .then(response => response.json())
        //   .then(responseJson => {
        //     if (responseJson.status == true) {
        //       Alert.alert(
        //         "Sucess",
        //         responseJson.result,
        //         [{ text: "OK", onPress: () => this._closeModal() }],
        //         { cancelable: false }
        //       );
        //     } else {
        //       Alert.alert("Error" + responseJson);
        //     }
        //   })

        // .catch(error => {
        //   console.error(error);
        //   Alert.alert(error);
        // });
      }
    });
  };
  componentWillMount() {
    this._subscribe = this.props.navigation.addListener("didFocus", () => {
      this.getData("profilePhoto");
      this.getData("Company");
      // this.getData("sLocation");
      // this.getData("Contry");
      // this.getData("IdSent");
      // this.getData("State");
      // this.getData("CitySave");
      this.getToken("token");
      this.getLatLongRegion("LattitudeSave");
      this.getLatLongRegion("LongitudedeSave");

      // this.getData("sLocation");
      // this.getData("Contry");
      // this.getData("IdSent");
      // this.getData("State");
      // this.getData("CitySave");
    });
  }
  loginOptions() {
    this.props.navigation.navigate("ProfileNav", {
      DataHomeComment: ""
    });
  }
  GetNearLocation() {
    //this.props.navigation.push('Friends')
    this.setState({
      loginInProcess: true,
      animating: true
    });
    let body = {};
    // console.log("Body Print:");
    // console.log(body);
    // console.log("EulaIdd", this.state.eulaId);

    // console.log(
    //   "Url Printing",
    //   "http://192.168.0.20:1011" +
    //     "/api/Incident/GetNearByIncidents" +
    //     "?lat=" +
    //     String(this.state.lastLat) +
    //     "&lan=" +
    //     String(this.state.lastLong)
    // );
    //var BASEURL1 = "http://192.168.0.12:1011";
     const BASEURL1 = "http://bbf.dimitrisnowden.com";
    //const BASEURL1 = "http://51.143.17.1:3310";
    APISingletonClass.getInstance().getMethodServiceHandler(
      BASEURL1 +
        "/api/Common/GetLocation" +
        "?lat=" +
        String(this.state.lastLat) +
        "&lan=" +
        String(this.state.lastLong),
      body,
      "",
      responce => {
        console.log("responseJson Eula", responce);
        let responseJson = JSON.parse(responce);
        this.setState({
          animating: false
        });
        console.log("responseJson Eula", responseJson);
        //console.warn("_loginServiceHandler " + responseJson.status)
        if (responseJson.Status == true) {
          this.setState({
            locationKey: responseJson.Result.display_name
          });
          // if (responseJson.Result.accepted) {
          //   this.props.navigation.navigate("homePageBbf", {
          //     DataHomeComment: ""
          //   });
          // }
        } else {
        }
      },
      error => {
        this.setState({
          loading: false
        });
                      this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);

        console.warn("_loginServiceHandler error " + error);
      }
    );
  }

  GetEnterPriseLocation() {
    //this.props.navigation.push('Friends')
    this.setState({
      loginInProcess: true,
      animating: true
    });
    let body = {};
    // console.log("Body Print:");
    // console.log(body);
    // console.log("EulaIdd", this.state.eulaId);

    console.log(
      "Url Printing",
      "http://192.168.0.20:1011" +
        "/api/EnterpriseApi/GetEnterpriseLocation" +
        "?lat=" +
        String(this.state.lastLat) +
        "&lan=" +
        String(this.state.lastLong)
    );
     const BASEURL1 = "http://bbf.dimitrisnowden.com";
  //  const BASEURL1 = "http://192.168.0.12:1011";
    // const BASEURL1 = "http://51.143.17.1:3310";
    APISingletonClass.getInstance().getMethodServiceHandler(
      BASEURL1 +
        "/api/EnterpriseApi/GetEnterpriseLocation" +
        "?lat=" +
        String(this.state.lastLat) +
        "&lan=" +
        String(this.state.lastLong),
      body,
      "",
      responce => {
        console.log("responseJson Eula", responce);
        let responseJson = JSON.parse(responce);
        this.setState({
          animating: false
        });
        console.log("responseJson Eula", responseJson);
        //console.warn("_loginServiceHandler " + responseJson.status)
        if (responseJson.Status == true) {
          var tempMarker = [];
          var tempIdMarker = [];
          for (var p in responseJson.Result) {
            tempMarker.push({
              value: responseJson.Result[p].location
            });
          }
          console.log("tempMarker", tempMarker);
          this.setState({ suggestions: tempMarker });

          for (var p in responseJson.Result) {
            tempIdMarker.push({
              value: responseJson.Result[p].id
            });
          }

          this.setState({
            idArray: tempIdMarker
          });
          console.log("Suggestion Given", this.state.suggestions);
          // if (responseJson.Result.accepted) {
          //   this.props.navigation.navigate("homePageBbf", {
          //     DataHomeComment: ""
          //   });
          // }
        } else {
        }
      },
      error => {
        this.setState({
          loading: false
        });
                      this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);

        console.warn("_loginServiceHandler error " + error);
      }
    );
  }
  storeData = async (Key, Value) => {
    try {
      await AsyncStorage.setItem(Key, Value);
    } catch (e) {
      // saving error
    }
  };
  getToken = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Pic Token", value);
        this.setState({
          token: value
        });

        // } else {
        //   this.setState({
        //     profileImage: value
        //   });
        // }
        //this.GetProfile();
      }
    } catch (e) {
      // error reading value
    }
  };
  uploadImages() {
    if (this.state.selectedLocation == "") {
      Alert.alert(
        "BBF",
        "Search your location is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (this.state.unitNameState == "") {
      Alert.alert(
        "BBF",
        "Room is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (this.state.filePath1.uri == null) {
      // profileimageurl1 = this.state.filePath1.uri;
      Alert.alert(
        "BBF",
        "HEADBOARD Image is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (this.state.filePath2.uri == null) {
      // profileimageurl1 = this.state.filePath1.uri;
      Alert.alert(
        "BBF",
        "PILLOW Image is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (this.state.filePath3.uri == null) {
      // profileimageurl1 = this.state.filePath1.uri;
      Alert.alert(
        "BBF",
        "MATTRESS Image is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (this.state.filePath4.uri == null) {
      // profileimageurl1 = this.state.filePath1.uri;
      Alert.alert(
        "BBF",
        "BED BASE Image is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else {
      const photo1 = {
        uri: this.state.filePath1.uri,
        type: this.state.filePath1.type,
        name: this.state.newFileName1
      };
      const photo2 = {
        uri: this.state.filePath2.uri,
        type: this.state.filePath2.type,
        name: this.state.newFileName2
      };
      const photo3 = {
        uri: this.state.filePath3.uri,
        type: this.state.filePath3.type,
        name: this.state.newFileName3
      };
      const photo4 = {
        uri: this.state.filePath4.uri,
        type: this.state.filePath4.type,
        name: this.state.newFileName4
      };
      var imageArray = [photo1, photo2, photo3, photo4];
      // console.log("FilePath", this.state.filePath.uri);

      const latSend = String(this.state.lastLat);
      const longSend = String(this.state.lastLong);
      const form = new FormData();

      // console.log("Form", form);
      // form.append("Photo", photo);
      form.append("Lat", this.state.lastLat);
      form.append("Lan", this.state.lastLong);
      //form.append("Location", this.state.locationKey);
      form.append("Images", photo1);
      form.append("Images", photo2);
      form.append("Images", photo3);
      form.append("Images", photo4);
      form.append("UnitNo", this.state.unitNameState);
      form.append(
        "Location",
        this.state.compayname
        //this.state.idArray[this.state.selectedIndex].value
      );
      console.log("Print Form Data", form);
      let body = {
        Lat: "17.09",
        Lan: "17.08",
        Location: "United Satates,California",
        Images: imageArray
      };
      this.setState({
        animating: true
      });
      console.log("Token", this.state.token);
      var BASEURL1 =  "http://bbf.dimitrisnowden.com"
      //var BASEURL = "http://192.168.0.12:1011";
      //var BASEURL = "http://51.143.17.1:3310";
      console.log(BASEURL1 + "/api/Incident/ReportIncident")
      fetch(BASEURL1 + "/api/Incident/ReportIncident", {
        method: "POST",
        headers: {
          Authorization: "Bearer " + this.state.token
        },
        body: form
      })
        .then(response => {
          return response.json();
        })
        .then(responseData => {
          this.setState({
            animating: false
          });
          var res = JSON.stringify(responseData);
          console.log("Response Data", responseData);
          if (responseData.Status == true) {
            this.storeData("Company", "");
            this.storeData("sLocation", "");
            this.storeData("pinCode", "");
            this.storeData("Contry", "");
            this.storeData("IdSent", "");
            this.storeData("State", "");
            //cityName
            this.storeData("CitySave", "");

            setTimeout(() => {
              Alert.alert(
                "BBF",
                responseData.Message,
                [
                  {
                    text: "OK",
                    onPress: () => this.props.navigation.goBack()
                  }
                ],
                { cancelable: false }
              );
            }, 200);
          } else {
            setTimeout(() => {
              Alert.alert(
                "BBF",
                responseData.ResponseException.ExceptionMessage,
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);
          }

          return responseData.state;
        })
        .catch(error => {
          console.error(error);
                        this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);

        });
    }
  }
  optionPress(option) {
    if (option == "0") {
    } else if (option == "1") {
    } else if (option == "2") {
    } else if (option == "3") {
    }
  }
  getData = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Pic", value);
        if (key == "profilePhoto") {
          this.setState({
            ProfilePic: value
          });
        } else if (key == "Company") {
          this.setState({
            compayname: value
          });
        }
        // else if (key == "sLocation") {
        //   this.setState({
        //     locationname: value
        //   });
        // } else if (key == "pinCode") {
        //   this.setState({
        //     zipCode: value
        //   });
        // } else if (key == "Contry") {
        //   this.setState({
        //     Country: value
        //   });
        // } else if (key == "IdSent") {
        //   this.setState({
        //     idCompny: value
        //   });
        // } else if (key == "State") {
        //   this.setState({
        //     statename: value
        //   });
        // } else if (key == "CitySave") {
        //   this.setState({
        //     cityName: value
        //   });
        // }

        //this.GetProfile();
      }
    } catch (e) {
      // error reading value
    }
  };
  onRegionChange(region, lastLat, lastLong) {
    console.log("Last Lattitude", lastLat);
    console.log("Last longitude", lastLong);
    this.setState({
      mapRegion: region,
      // If there are no new values set the current ones
      lastLat: lastLat || this.state.lastLat,
      lastLong: lastLong || this.state.lastLong
    });
    if (this.state.lastLat != null && this.state.lastLong != null) {
      this.GetNearLocation();
    }
  }
  getLatLongRegion = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        if (key == "LattitudeSave") {
          this.setState({ lastLat: value });
        } else if (key == "LongitudedeSave") {
          this.setState({ lastLong: value });
          let region = {
            latitude: this.state.lastLat,
            longitude: this.state.lastLong,
            latitudeDelta: 0.00922 * 1.5,
            longitudeDelta: 0.00421 * 1.5
          };
          this.setState({ mapRegion: region });
        } else {
          this.setState({ mapRegion: value });
        }
        // value previously stored
        console.log("Got Pic", value);
        if (this.state.lastLat != null && this.state.lastLong != null) {
          this.GetNearLocation();
          // this.GetEnterPriseLocation();
        }
        // this.setState({
        //   ProfilePic: value
        // });

        //this.GetProfile();
      }
    } catch (e) {
      // error reading value
    }
  };
  componentDidMount() {
    this.getData("profilePhoto");
    this.getData("Company");
    // this.getData("sLocation");
    // this.getData("Contry");
    // this.getData("IdSent");
    // this.getData("State");
    // this.getData("CitySave");
    this.getToken("token");
    this.getLatLongRegion("LattitudeSave");
    this.getLatLongRegion("LongitudedeSave");

    // this.storeData("Company", form[0].company.name);
    // this.storeData("sLocation", form[0].location);
    // this.storeData("pinCode", form[0].zip);
    // this.storeData("Contry", form[0].country);
    // this.storeData("IdSent", form[0].id);
    // this.storeData("State", form[0].state);

    // this.watchID = navigator.geolocation.watchPosition(
    //   position => {
    //     // Create the object to update this.state.mapRegion through the onRegionChange function
    //     let region = {
    //       latitude: position.coords.latitude,
    //       longitude: position.coords.longitude,
    //       latitudeDelta: 0.00922 * 1.5,
    //       longitudeDelta: 0.00421 * 1.5
    //     };
    //     this.onRegionChange(region, region.latitude, region.longitude);
    //   },
    //   error => console.log(error)
    // );
  }
  render() {
    let deviceWidth = Dimensions.get("window").width;
    var now = new Date(),
      age = new Date(now.getFullYear() - 18, now.getMonth());
    var profileimageurl1 = "";
    var profileimageurl2 = "";
    var profileimageurl3 = "";
    var profileimageurl4 = "";
    var key1 = null;
    if (this.state.filePath1.uri != null) {
      profileimageurl1 = this.state.filePath1.uri;
    }
    if (this.state.filePath2.uri != null) {
      profileimageurl2 = this.state.filePath2.uri;
    }
    if (this.state.filePath3.uri != null) {
      profileimageurl3 = this.state.filePath3.uri;
    }
    if (this.state.filePath4.uri != null) {
      profileimageurl4 = this.state.filePath4.uri;
    }
    // {item.location},{item.city},{item.state},{item.country},
    // {item.zip}
    this.state.selectedLocation = this.state.compayname;

    return (
      <View style={{ backgroundColor: "gray" }}>
        <StatusBar hidden={true} />
        <Spinner
          visible={this.state.animating}
          //textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <ImageBackground
          source={require("./Assets/main_background.png")}
          size={28}
          // borderRadius={15}
          style={{
            height: Dimensions.get("window").height,
            width: Dimensions.get("window").width,
            resizeMode: "contain"
          }}
        >
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <View
              style={{
                flexDirection: "row",
                width: scale(230),
                left: 15,
                backgroundColor: "transparent"
              }}
            >
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image
                  source={require("./Assets/back.png")}
                  size={28}
                  borderRadius={15}
                  //onPress={this.props.navigation.goBack()}
                  style={{
                    height: verticalScale(60),
                    width: scale(30),
                    alignSelf: "center",
                    resizeMode: "contain",
                    marginTop: moderateScale(26),
                    backgroundColor: "transparent",
                    justifyContent: "flex-start",
                    backgroundColor: "transparent"
                  }}
                />
              </TouchableOpacity>
              <Text
                style={{
                  fontSize: moderateScale(25),
                  color: "#FEFEFE",
                  textAlign: "center",
                  fontWeight: "200",
                  marginTop: moderateScale(42),
                  marginLeft: 0,
                  marginRight: 20,
                  backgroundColor: "transparent",
                  fontFamily: "SegoeUI-Light"
                }}
              >
                {" "}
                R E P O R T
              </Text>
            </View>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("ProfileNav1", {
                  DataHomeComment: ""
                })
              }
            >
              <View
                style={{
                  marginRight: 10,
                  marginTop: 20,
                  alignSelf: "flex-end",
                  width: moderateScale(75),
                  height: moderateScale(75),
                  borderRadius: moderateScale(45),
                  borderWidth: moderateScale(3),
                  borderColor: "#525353",
                  backgroundColor: "transparent"
                }}
              >
                <Image
                  style={styles.avatar}
                  //source={require("./Assets/BrianImage.jpeg")}
                  source={{
                    uri: BASEURL + "/" + this.state.ProfilePic
                  }}
                  onPress={this.chooseFile}
                  resizeMode="cover"
                />
              </View>
            </TouchableOpacity>
          </View>
          <KeyboardAwareScrollView>
            {/* <AutoComplete
              onSelect={this.onSelect()}
              suggestions={this.state.suggestions}
              suggestionObjectTextProperty="location"
              value={this.state.entertext}
              placeholder="Search Here for location"
              placeholderTextColor="#C0C0C0"
              onChangeText={entertext => {
                this.setState({ entertext });
              }}
              style={{
                fontSize: moderateScale(45),
                color: "#FEFEFE",
                textAlign: "center",
                fontWeight: "200",
                marginTop: moderateScale(20),
                marginLeft: 20,
                marginRight: 20,
                fontFamily: "SegoeUI-Light",
                borderWidth: 0.5,
                height: 35,
                borderRadius: 5,
                borderColor: "#7098AA",
                color: "white"
              }}
            /> */}

            <View
              style={{
                marginLeft: 20,
                marginRight: 20,
                width: Dimensions.get("window").width - 40,
                backgroundColor: "transparent",
                height: 80,
                marginTop: 10
              }}
            >
              <TextInput
                placeholder="Select your address"
                placeholderTextColor="#C0C0C0"
                onChangeText={selectedLocation => {
                  this.setState({ selectedLocation });
                }}
                onPress={() =>
                  this.props.navigation.navigate("LocationCall", {
                    DataHomeComment: ""
                  })
                }
                editable={false}
                multiline={true}
                value={this.state.selectedLocation}
                //   returnKeyType={"next"}
                style={{
                  marginLeft: 0,
                  marginRight: 0,
                  backgroundColor: "transparent",
                  marginTop: 0,
                  color: "white",
                  fontSize: moderateScale(20),
                  height: 35,
                  fontFamily: "SegoeUI-Light",
                  borderWidth: 0.5,
                  borderColor: "#7098AA",
                  borderRadius: 5,
                  height: 100,
                  position: "absolute",
                  width: Dimensions.get("window").width - 40
                }}
              />
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("LocationCall", {
                    DataHomeComment: ""
                  })
                }
              >
                <View
                  style={{
                    marginLeft: 20,
                    marginRight: 20,
                    backgroundColor: "red",
                    marginTop: 10,
                    height: 80,
                    width: Dimensions.get("window").width - 40,
                    backgroundColor: "transparent"
                  }}
                />
              </TouchableOpacity>
            </View>
            {/* <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("LocationCall", {
                  DataHomeComment: ""
                })
              }
            >
              <View
                style={{
                  marginLeft: 20,
                  marginRight: 20,
                  // backgroundColor: "red",
                  marginTop: 10,
                  height: 80,
                  width: Dimensions.get("window").width - 40,
                  backgroundColor: "transparent",
                  position: "absolute",
                  backgroundColor: "black"
                }}
              />
            </TouchableOpacity> */}
            {/* <Dropdown
              pickerStyle={{ borderColor: "gray", borderWidth: 1 }}
              style={{ color: "#565656" }}
              label="secquestion"
              //height={150}
              label="Search your location"
              placeholderTextColor="white"
              fontFamily="SegoeUI-Light"
              containerStyle={{ marginLeft: 20, marginRight: 20 }}
              data={this.state.suggestions}
              dropdownStyle={{ width: 170 }}
              textStyle={{
                fontWeight: "bold",
                textAlign: "right",
                color: "black",
                fontFamily: "SegoeUI-Light",
                placeholderTextColor: "white"
              }}
              textColor="white"
              selectedItemColor="black"
              style={{ flex: 1, paddingRight: 30, fontSize: 18 }}
              ref="securequestion"
              returnKeyType={"next"}
              // borderWidth={1}
              onChangeText={(selectedLocation, selectedIndex) =>
                this.setState(
                  { selectedLocation, selectedIndex },
                  console.log(
                    "QUestion: INdex: data: " +
                      selectedLocation +
                      ":" +
                      selectedIndex +
                      ":"
                  )
                )
              }
              value={this.state.suggestions.location}
              //onChangeText={this.onSecurityQuestionChange}
              //value={this.state.askAQuestion}
              returnKeyType={"next"}
              autoFocus={true}
              //textColor="#565656"

              // onPress={question_id => this.setState({ question_id })}
            /> */}
            <TextInput
              placeholder="Room"
              placeholderTextColor="#C0C0C0"
              onChangeText={unitNameState => {
                this.setState({ unitNameState });
              }}
              value={this.state.unitNameState}
              //   returnKeyType={"next"}
              style={{
                marginLeft: 20,
                marginRight: 20,
                // backgroundColor: "red",
                marginTop: 30,
                color: "white",
                fontSize: moderateScale(20),
                height: 35,
                fontFamily: "SegoeUI-Light",
                borderWidth: 0.5,
                borderColor: "#7098AA",
                borderRadius: 5,
                justifyContent: "center"
                //backgroundColor: "red"
              }}
            />
            <Text
              style={{
                fontSize: moderateScale(25),
                color: "#FEFEFE",
                textAlign: "center",
                fontWeight: "200",
                marginTop: moderateScale(20),
                marginLeft: 20,
                marginRight: 20,
                fontFamily: "SegoeUI-Light"
              }}
            >
              {" "}
              You'll take four pictures...
            </Text>
            <View
              style={{
                flexDirection: "row",
                width: Dimensions.get("window").width,
                height: moderateScale(160),
                backgroundColor: "transperant",
                marginTop: moderateScale(40),
                justifyContent: "space-between"
              }}
            >
              {/* <AutoSuggest
              onChangeText={text => console.log("input changing!")}
              terms={[
                "Apple",
                "Banana",
                "Orange",
                "Strawberry",
                "Lemon",
                "Cantaloupe",
                "Peach",
                "Mandarin",
                "Date",
                "Kiwi"
              ]}
            /> */}
              <TouchableOpacity onPress={() => this.updateProfileImage("1")}>
                <View
                  style={{
                    height: moderateScale(150),
                    width: moderateScale(150),
                    alignSelf: "flex-start",
                    resizeMode: "contain",

                    marginLeft: 20,
                    backgroundColor: "transparent"
                  }}
                >
                  <Image
                    source={
                      profileimageurl1 == ""
                        ? require("./Assets/take_picture.png")
                        : { uri: profileimageurl1 }
                    }
                    size={28}
                    borderRadius={15}
                    style={{
                      height: moderateScale(140),
                      width: moderateScale(140),
                      alignSelf: "center",
                      resizeMode: "contain",
                      marginTop: moderateScale(10),
                      backgroundColor: "transparent"
                    }}
                  />
                  <Text
                    style={{
                      fontSize: moderateScale(18),
                      color: "#FEFEFE",
                      alignSelf: "center",
                      fontWeight: "300",
                      marginTop: moderateScale(5),
                      textAlign: "center",
                      fontFamily: "SegoeUI-Light"
                    }}
                  >
                    HEADBOARD
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.updateProfileImage("2")}>
                <View
                  style={{
                    height: moderateScale(150),
                    width: moderateScale(150),
                    alignSelf: "flex-end",
                    resizeMode: "contain",

                    marginRight: 20,
                    backgroundColor: "transparent"
                  }}
                >
                  <Image
                    source={
                      profileimageurl2 == ""
                        ? require("./Assets/take_picture.png")
                        : { uri: profileimageurl2 }
                    }
                    size={28}
                    borderRadius={15}
                    style={{
                      height: moderateScale(140),
                      width: moderateScale(140),
                      alignSelf: "center",
                      resizeMode: "contain",
                      marginTop: moderateScale(10),
                      backgroundColor: "transparent"
                    }}
                  />
                  <Text
                    style={{
                      fontSize: moderateScale(18),
                      color: "#FEFEFE",
                      alignSelf: "center",
                      fontWeight: "300",
                      marginTop: moderateScale(5),
                      textAlign: "center",
                      fontFamily: "SegoeUI-Light"
                    }}
                  >
                    PILLOW
                  </Text>
                </View>
              </TouchableOpacity>
            </View>

            <View
              style={{
                flexDirection: "row",
                width: Dimensions.get("window").width,
                height: moderateScale(160),
                backgroundColor: "transparent",
                marginTop: moderateScale(40),
                justifyContent: "space-between"
              }}
            >
              <TouchableOpacity onPress={() => this.updateProfileImage("3")}>
                <View
                  style={{
                    height: moderateScale(150),
                    width: moderateScale(150),
                    alignSelf: "flex-start",
                    resizeMode: "contain",

                    marginLeft: 20,
                    backgroundColor: "transparent"
                  }}
                >
                  <Image
                    source={
                      profileimageurl3 == ""
                        ? require("./Assets/take_picture.png")
                        : { uri: profileimageurl3 }
                    }
                    size={28}
                    borderRadius={15}
                    style={{
                      height: moderateScale(140),
                      width: moderateScale(140),
                      alignSelf: "center",
                      resizeMode: "contain",
                      marginTop: moderateScale(10),
                      backgroundColor: "transparent"
                    }}
                  />
                  <Text
                    style={{
                      fontSize: moderateScale(18),
                      color: "#FEFEFE",
                      alignSelf: "center",
                      textAlign: "center",
                      fontWeight: "300",
                      marginTop: 5,
                      fontFamily: "SegoeUI-Light"
                    }}
                  >
                    MATTRESS
                  </Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.updateProfileImage("4")}>
                <View
                  style={{
                    height: moderateScale(150),
                    width: moderateScale(150),
                    alignSelf: "flex-end",
                    resizeMode: "contain",
                    marginRight: 20,
                    backgroundColor: "transparent"
                  }}
                >
                  {/* <View</View>
                style={{
                  
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              > */}

                  <Image
                    source={
                      profileimageurl4 == ""
                        ? require("./Assets/take_picture.png")
                        : { uri: profileimageurl4 }
                    }
                    size={28}
                    borderRadius={15}
                    style={{
                      height: moderateScale(140),
                      width: moderateScale(140),
                      alignSelf: "center",
                      resizeMode: "contain",
                      marginTop: moderateScale(10),
                      backgroundColor: "transparent"
                    }}
                  />
                  <Text
                    style={{
                      fontSize: moderateScale(18),
                      color: "#FEFEFE",
                      alignSelf: "center",
                      fontWeight: "300",
                      marginTop: moderateScale(5),
                      textAlign: "center",
                      fontFamily: "SegoeUI-Light"
                    }}
                  >
                    {" "}
                    BED BASE
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
            <TouchableOpacity onPress={() => this.uploadImages()}>
              <Image
                source={require("./Assets/done.png")}
                size={28}
                borderRadius={15}
                //onPress={this.props.navigation.goBack()}
                style={{
                  height: moderateScale(60),
                  width: moderateScale(30),
                  alignSelf: "center",
                  resizeMode: "contain",
                  marginTop:
                    Dimensions.get("window").height <= 568
                      ? moderateScale(-5)
                      : moderateScale(20),
                  marginRight: moderateScale(0),
                  backgroundColor: "transparent",
                  justifyContent: "flex-start",
                  backgroundColor: "rgba(52, 52, 52, 0.5)"
                }}
              />
            </TouchableOpacity>
          </KeyboardAwareScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ecf0f1"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 10
  },
  sharingtext: {
    color: "#AA2E2E",
    backgroundColor: "#fff",
    borderRadius: 14,
    borderColor: "#AA2E2E",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 18,
    width: 130,
    height: 30,
    alignItems: "center",
    alignContent: "center",
    textAlignVertical: "center"
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover" // or 'stretch'
  },
  footerWrapperlikeCommentShare: {
    flexWrap: "nowrap",
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginLeft: 30,
    marginRight: 30
  },
  separatorlikeShare: {
    flex: 0,
    borderWidth: 1,
    borderColor: "#ECEDED"
  },
  imageSize2: {
    width: 60,
    height: 60,
    padding: 5,
    borderRadius: 10
  },
  avatar: {
    width: moderateScale(70),
    height: moderateScale(70),
    borderRadius: moderateScale(35),
    borderWidth: moderateScale(3),
    borderColor: "#7098AA",
    alignItems: "center"
  }
});

export default withNavigation(ReportImage);
