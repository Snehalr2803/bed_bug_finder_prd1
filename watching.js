import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  ScrollView,
  NavigatorIOS,
  Alert,
  Image,
  ToastAndroid,
  ActivityIndicator,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  StatusBar,
  Switch,
  FlatList
} from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import APISingletonClass, { BASEURL, GET_WATCHLIST_API } from "./API";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import AsyncStorage from "@react-native-community/async-storage";
import { withNavigation } from "react-navigation";
//import { Switch } from "react-native-switch";
import ToggleSwitch from "toggle-switch-react-native";
import Spinner from "react-native-loading-spinner-overlay";
import { ScaledSheet } from "react-native-size-matters";
import { scale, verticalScale, moderateScale } from "react-native-size-matters";
const EmptyComponent = ({ title }) => (
  <View style={styles.emptyContainer}>
    <Text style={styles.emptyText}>{title}</Text>
  </View>
);
class Watching extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      ProfilePic: "",
      password: "",
      loading: false,
      animating: false,
      loginInProcess: false,
      switchedBool: false,
      token: "",
      wtachList: []
    };
  }
  getData = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Pic", value);
        this.setState({
          ProfilePic: value
        });

        //this.GetProfile();
      }
    } catch (e) {
      // error reading value
    }
  };
  getToken = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Pic Token", value);
        this.setState({
          token: value
        });
        this.GetWatchList();

        // } else {
        //   this.setState({
        //     profileImage: value
        //   });
        // }
        //this.GetProfile();
      }
    } catch (e) {
      // error reading value
    }
  };
  componentDidMount() {
    this.getData("profilePhoto");
    this.getToken("token");
  }
  loginOptions() {
    this.props.navigation.navigate("ProfileNav", {
      DataHomeComment: ""
    });
  }
  GetWatchList() {
    //this.props.navigation.push('Friends')

    this.setState({
      loginInProcess: true,
      animating: true
    });
    let body = {};

    console.log("Body Print Token:", this.state.token);
    console.log(body);
    APISingletonClass.getInstance().getMethodServiceHandler(
      GET_WATCHLIST_API,
      body,
      "Bearer " + this.state.token,
      responce => {
        this.setState({
          animating: false
        });
        console.log("responseJson Watchlist", responce);
        let responseJson = JSON.parse(responce);
        console.log("responseJson Eula", responseJson);
        //console.warn("_loginServiceHandler " + responseJson.status)
        if (responseJson.Status == true) {
          this.setState({
            wtachList: responseJson.Result
          });
        } else {
        }
      },
      error => {
        this.setState({
          loading: false
        });
                      this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);

        console.warn("_loginServiceHandler error " + error);
      }
    );
  }
  optionPress(option) {
    if (option == "0") {
    } else if (option == "1") {
    } else if (option == "2") {
    } else if (option == "3") {
    }
  }
  render() {
    return (
      <View style={{ backgroundColor: "gray" }}>
        <StatusBar hidden={true} />
        <Spinner
          visible={this.state.animating}
          //textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <ImageBackground
          source={require("./Assets/main_background.png")}
          size={28}
          // borderRadius={15}
          style={{
            height: Dimensions.get("window").height,
            width: Dimensions.get("window").width,
            resizeMode: "contain"
          }}
        >
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <View
              style={{
                flexDirection: "row",
                width: scale(230),
                left: moderateScale(15),
                backgroundColor: "transparent"
              }}
            >
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image
                  source={require("./Assets/back.png")}
                  size={28}
                  borderRadius={15}
                  //onPress={this.props.navigation.goBack()}
                  style={{
                    height: moderateScale(60),
                    width: moderateScale(30),
                    alignSelf: "center",
                    resizeMode: "contain",
                    marginTop: moderateScale(24),
                    backgroundColor: "transparent",
                    justifyContent: "flex-start",
                    backgroundColor: "transparent"
                  }}
                />
              </TouchableOpacity>
              <Text
                style={{
                  fontSize: moderateScale(22),
                  color: "#FEFEFE",
                  textAlign: "center",
                  fontWeight: "200",
                  marginTop: moderateScale(40),
                  marginLeft: 0,
                  marginRight: 20,
                  backgroundColor: "transparent",
                  fontFamily: "SegoeUI-Light"
                }}
              >
                {" "}
                W A T C H I N G
              </Text>
            </View>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("ProfileNav1", {
                  DataHomeComment: ""
                })
              }
            >
              <View
                style={{
                  marginRight: 10,
                  marginTop: moderateScale(20),
                  alignSelf: "flex-end",
                  width: moderateScale(75),
                  height: moderateScale(75),
                  borderRadius: moderateScale(40),
                  borderWidth: moderateScale(3),
                  borderColor: "#525353",
                  backgroundColor: "transparent"
                }}
              >
                <Image
                  style={styles.avatar}
                  // source={require("./Assets/BrianImage.jpeg")}
                  source={{
                    uri: BASEURL + "/" + this.state.ProfilePic
                  }}
                  onPress={this.chooseFile}
                  resizeMode="cover"
                />
              </View>
            </TouchableOpacity>
          </View>

          <FlatList
            margin={10}
            numColumns="1"
            data={this.state.wtachList}
            showsVerticalScrollIndicator={true}
            keyExtractor={item => item}
            ListEmptyComponent={
              <EmptyComponent title="You aren’t watching any locations." />
            }
            renderItem={({ item, index }) => (
              <View style={styles.footerWrapperNumberCL}>
                <Text
                  style={{
                    fontSize: moderateScale(24),
                    color: "#7098AA",
                    textAlign: "left",
                    fontWeight: "400",
                    marginTop: moderateScale(20),
                    marginLeft: 20,
                    marginRight: 20,
                    fontFamily: "SegoeUI-Light"
                  }}
                >
                  {item.incident.location != null
                    ? item.incident.location.split(",")[0]
                    : "null"}
                  {/* {item.incident.location.split(",")[0]} */}
                </Text>
                <Text
                  style={{
                    fontSize: moderateScale(20),
                    color: "#FEFEFE",
                    textAlign: "left",
                    fontWeight: "200",
                    marginTop: moderateScale(5),
                    marginLeft: 20,
                    marginRight: 20,
                    fontFamily: "SegoeUI-Light"
                  }}
                >
                  {item.incident.location}
                </Text>
              </View>
            )}
          />
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ecf0f1"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  footerWrapperNumberCL: {
    flexWrap: "wrap",
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 10
  },
  sharingtext: {
    color: "#AA2E2E",
    backgroundColor: "#fff",
    borderRadius: 14,
    borderColor: "#AA2E2E",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 18,
    width: 130,
    height: 30,
    alignItems: "center",
    alignContent: "center",
    textAlignVertical: "center"
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover" // or 'stretch'
  },
  footerWrapperlikeCommentShare: {
    flexWrap: "nowrap",
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginLeft: 30,
    marginRight: 30
  },
  separatorlikeShare: {
    flex: 0,
    borderWidth: 1,
    borderColor: "#ECEDED"
  },
  imageSize2: {
    width: 60,
    height: 60,
    padding: 5,
    borderRadius: 10
  },
  avatar: {
    width: moderateScale(70),
    height: moderateScale(70),
    borderRadius: moderateScale(35),
    borderWidth: moderateScale(3),
    borderColor: "#7098AA",
    alignItems: "center"
  },
  emptyContainer: {
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    alignContent: "center",
    marginTop: 30
  },
  emptyText: {
    fontSize: 20,

    color: "white",
    borderWidth: 1,
    borderColor: "#ECEDED",
    padding: 20
  }
});

export default withNavigation(Watching);
