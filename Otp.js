import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  ScrollView,
  NavigatorIOS,
  Alert,
  Image,
  ToastAndroid,
  ActivityIndicator,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  StatusBar,
  TouchableWithoutFeedback,
  Keyboard
} from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Spinner from "react-native-loading-spinner-overlay";
import { withNavigation } from "react-navigation";
import { ScaledSheet } from "react-native-size-matters";
import { scale, verticalScale, moderateScale } from "react-native-size-matters";
import APISingletonClass, { RECOVERY_API, BASEURL } from "./API";
import AsyncStorage from "@react-native-community/async-storage";
var reactnative_deviceinfo_DeviceInfo = require("react-native-device-info").default;
class OtpScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      OtpState: "",
      password: "",
      loading: false,
      animating: false,
      loginInProcess: false,
      phoneNumberSignUp:"",
      formDa:null,
      deviceId: "",
    };
    this.inputRefs = {};
    //this.validate = this.validate.bind(this);
  }

  loginOptions() {
    this.props.navigation.navigate("ProfileNav", {
      DataHomeComment: ""
    });
  }
  getdeviceId = () => {
    //Getting the Unique Id from here
    var id = reactnative_deviceinfo_DeviceInfo.getUniqueID();
    console.log("Device Id:",id)
    this.setState({ deviceId: id, });
  };
  validate = () => {
    let text = this.state.email;
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      console.log("Email is Not Correct");
      this.setState({ email: text });
      return false;
    } else {
      this.setState({ email: text });
      console.log("Email is Correct");
      return true;
    }
  };
  componentDidMount(){
    this.getPhonenumber("PhoneNumberSignUp")
    this.getdeviceId()
  }
  getFormData= async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got form Token", value);
        this.setState({
          formDa: JSON.parse(value)
        });
       // this.GetOtp()
        //this.GetProfile();
       // console.log("Social login status1",this.state.phoneNumberSignUp)

      }
    } catch (e) {
      // error reading value
    }
  };
  getPhonenumber= async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Token", value);
        this.setState({
          phoneNumberSignUp: value
        });
        this.GetOtp()
        //this.GetProfile();
       // console.log("Social login status1",this.state.phoneNumberSignUp)

      }
    } catch (e) {
      // error reading value
    }
  };
  GetOtp(){
    if (!this.state.loginInProcess) {
      this.setState({
        loginInProcess: true,
        animating: true
      });
      var myString = this.state.phoneNumberSignUp.replace(/\D/g, "");
      console.log("Phone Number",myString)
//       let body = {
//         mobileNumber:"510-894-5743",
//         OTPUsingFor: "MobileNumberVerification"
//  };/
let body = {
};
      console.log("Body Print:");
      console.log(body);
      APISingletonClass.getInstance().postMethodServiceHandler(
        BASEURL+"/api/OTP/getOTP?mobileNumber="+"510-894-5743"+"&OTPUsingFor=MobileNumberVerification" ,
        body,
        "",
        responce => {
          let responseJson = JSON.parse(responce);
          console.log("responseJson forgot", responseJson);
          //console.warn("_loginServiceHandler " + responseJson.status)
          this.setState({
            animating: false
          });
          if (responseJson.Status == true) {
            this.getFormData("SignUpDataBody")
          } else {
            setTimeout(() => {
              Alert.alert(
                "BBF",
                responseJson.ResponseException.ExceptionMessage,
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Ok")
                  }
                ],
                { cancelable: false }
              );
            }, 200);
          }
        },
        error => {
          this.setState({
            loading: false
          });
                        this.setState({
        animating: false
      });
       setTimeout(() => {
            Alert.alert(
              "BBF",
              "Something Went wrong ,Please try again.",
              [
                {
                  text: "OK",
                  onPress: () => console.log("Dislike.")
                }
              ],
              { cancelable: false }
            );
          }, 200);
          console.warn("_loginServiceHandler error " + error);
        }
      );
    }
  }
  ProceedToSignUp(){
    // var formDataFromSign = 
     const form = new FormData()
     form.append("FirstName", this.state.formDa.FirstName);
     form.append("LastName", this.state.formDa.LastName);
     form.append("Email", this.state.formDa.Email);
     form.append("PhoneNumber", this.state.formDa.PhoneNumber);
     form.append("UserName", this.state.formDa.UserName);
     form.append("Password", this.state.formDa.Password);
     form.append("dob", this.state.formDa.dob);
     form.append("RoleId", 1);
     form.append("Location", this.state.formDa.Location);
     // form.append("Lat", "17.4375");
     // form.append("Lan", "78.4482");
     form.append("ProfileImage", this.state.formDa.ProfileImage);
     console.log("FormData", form);
    // form = 
console.log("Printing form Data",this.state.formDa)
console.log("Form :",form)
  // console.log("Body Added:", body);
      console.log("API:-", BASEURL + "/api/User/CreateUser");
      fetch(BASEURL + "/api/User/CreateUser", {
        method: "POST",
        // headers: {
        //   "Content-Type": "multipart/form-data;"
        // },
        body: form
      })
        .then(response => {
          return response.json();
        })
        .then(responseData => {
          this.setState({
            animating: false
          });
          var res = JSON.stringify(responseData);
          console.log("Response Data signUp", responseData);
          if (responseData.Status == true) {
            // Alert.alert(
            //   "BBF",
            //   "Registration Successfully Completed.",
            //   [{ text: "OK", onPress: () => this.props.navigation.goBack() }],
            //   { cancelable: false }
            // );

            setTimeout(() => {
              Alert.alert(
                "BBF",
                "Registration Successfully Completed.",
                [
                  {
                    text: "OK",
                    onPress: () => this.props.navigation.navigate("Home", {})
                  }
                ],
                { cancelable: false }
              );
            }, 200);
          } else {
            setTimeout(() => {
              Alert.alert(
                "BBF",
                responseData.ResponseException.ExceptionMessage,
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);
          }
          return responseData.state;
        })
        .catch(error => {
          console.error(error);
                        this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);
        }); 
  }
  _loginBtnAction() {
    if (this.state.OtpState == "") {
      Alert.alert(
        "BBF",
        "Enter Otp here is required.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else {
      //this.props.navigation.push('Friends')
      
        this.setState({
          loginInProcess: true,
          animating: true
        });
        var myString = this.state.phoneNumberSignUp.replace(/\D/g, "");
        let body = {
          // checkingId:this.state.deviceId,
          // OTPReceived:this.state.OtpState,
        };
        console.log("Body Print:");
        console.log(body);
        console.log(BASEURL + "/api/OTP/verifyOTP?OTPReceived="+this.state.OtpState+"&checkingId="+this.state.deviceId)
        APISingletonClass.getInstance().postMethodServiceHandler(
          BASEURL + "/api/OTP/verifyOTP?OTPReceived="+this.state.OtpState+"&checkingId="+this.state.deviceId,
          body,
          "",
          responce => {
            let responseJson = JSON.parse(responce);
            console.log("responseJson forgot", responseJson);
            //console.warn("_loginServiceHandler " + responseJson.status)
            this.setState({
              animating: false
            });
            if (responseJson.Result.isOTPValid == true) {
            this.ProceedToSignUp()
            } else {
              setTimeout(() => {
               // this.ProceedToSignUp()
                Alert.alert(
                  "BBF",
                  responseJson.Result.reason,
                  [
                    {
                      text: "OK",
                      onPress: () => console.log("Ok")
                    }
                  ],
                  { cancelable: false }
                );
              }, 200);
            }
          },
          error => {
            this.setState({
              loading: false
            });
                          this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);
            console.warn("_loginServiceHandler error " + error);
          }
        );
      }   
  }
  render() {
    return (
      <View style={{ backgroundColor: "gray" }}>
        <StatusBar hidden={true} />
        {/* <Spinner
          visible={this.state.animating}
          //textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        /> */}
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <ImageBackground
            source={require("./Assets/main_background.png")}
            size={28}
            // borderRadius={15}
            style={{
              height: Dimensions.get("window").height,
              width: Dimensions.get("window").width,
              resizeMode: "contain"
            }}
          >
            <View style={{ marginTop: 10 }}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between"
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    width: Dimensions.get("window").width,
                    left: 15,
                    backgroundColor: "transparent"
                  }}
                >
                  <TouchableOpacity
                    onPress={() => this.props.navigation.goBack()}
                  >
                    <Image
                      source={require("./Assets/back.png")}
                      size={28}
                      borderRadius={15}
                      //onPress={this.props.navigation.goBack()}
                      style={{
                        height: 60,
                        width: 30,
                        alignSelf: "center",
                        resizeMode: "contain",
                        marginTop: 24,
                        backgroundColor: "transparent",
                        justifyContent: "flex-start",
                        backgroundColor: "transparent"
                      }}
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      fontSize: moderateScale(21),
                      color: "#FEFEFE",
                      textAlign: "center",
                      fontWeight: "200",
                      marginTop: 40,
                      marginLeft: 0,
                      marginRight: 20,
                      backgroundColor: "transparent"
                    }}
                  >
                    O T P {"  "}V E R I F I C A T I O N
                  </Text>
                </View>
              </View>
            </View>

            <Image
              source={require("./Assets/bbf_logo.png")}
              size={28}
              borderRadius={15}
              style={{
                height: verticalScale(150),
                width: scale(120),
                alignSelf: "center",
                justifyContent: "center",
                resizeMode: "contain",
                marginTop: 10
              }}
            />
            {/* <Text
              style={{
                fontSize: moderateScale(20),
                color: "#FEFEFE",
                textAlign: "center",
                fontWeight: "200",
                marginTop: moderateScale(20),
                alignContent: "center",
                alignSelf: "center",
                alignItems: "center",
                backgroundColor: "transparent"
              }}
            >
              {" "}
              Otp Sent to your Mobile Number.
            </Text> */}
            <View
              style={{
                flexDirection: "row",
                alignContent: "flex-start",
                marginLeft: 30,
                marginRight: 30,
                marginTop: 40,
                backgroundColor: "transparent",
                borderColor: "#7098AA",
                borderWidth: 1,
                borderRadius: 10,
                height: 40
              }}
            >
              <TextInput
                placeholder="Your Mobile Number"
                placeholderTextColor="#C0C0C0"
                
                value={this.state.phoneNumberSignUp}
                returnKeyType={"next"}
                editable={false}
                style={{
                  marginLeft: 10,
                  marginRight: 10,
                  textAlign: "left",
                  position: "absolute",
                  marginTop: 5,
                  color: "white",
                  fontSize: 20,
                  width: Dimensions.get("window").width - scale(80)
                }}
              />
            </View>
            <View
              style={{
                flexDirection: "row",
                alignContent: "flex-start",
                marginLeft: 30,
                marginRight: 30,
                marginTop: 40,
                backgroundColor: "transparent",
                borderColor: "#7098AA",
                borderWidth: 1,
                borderRadius: 10,
                height: 40
              }}
            >
              <TextInput
                placeholder="Enter Otp here"
                placeholderTextColor="#C0C0C0"
                onChangeText={OtpState => {
                  this.setState({ OtpState });
                }}
                value={this.state.OtpState}
                returnKeyType={"next"}
                style={{
                  marginLeft: 10,
                  marginRight: 10,
                  textAlign: "left",
                  position: "absolute",
                  marginTop: 5,
                  color: "white",
                  fontSize: 20,
                  width: Dimensions.get("window").width - scale(80)
                }}
              />
            </View>
            <View
              style={{
                height: 60,
                fontSize: 20,
                marginTop: 50,
                textAlign: "center",
                backgroundColor: "#484848",
                height: 40,
                borderRadius: 18,
                borderWidth: 0.5,
                borderColor: "#7098AA",
                width: 150,
                alignItems: "center",
                alignContent: "center",
                alignSelf: "center"
              }}
            >
              <Text
                style={{
                  height: 60,
                  fontSize: 20,
                  marginTop: 4,
                  width: 150,
                  textAlign: "center",
                  backgroundColor: "transparent",
                  height: 50,
                  color: "white"
                }}
                onPress={() => this._loginBtnAction()}
                title="Login"
                color="black"
                accessibilityLabel="Tap on Me"
              >
                Submit
              </Text>
            </View>
          </ImageBackground>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ecf0f1"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 10
  },
  sharingtext: {
    color: "#AA2E2E",
    backgroundColor: "#fff",
    borderRadius: 14,
    borderColor: "#AA2E2E",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 18,
    width: 130,
    height: 30,
    alignItems: "center",
    alignContent: "center",
    textAlignVertical: "center"
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover" // or 'stretch'
  },
  footerWrapperlikeCommentShare: {
    flexWrap: "nowrap",
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginLeft: 30,
    marginRight: 30
  },
  separatorlikeShare: {
    flex: 0,
    borderWidth: 1,
    borderColor: "#ECEDED"
  },
  imageSize2: {
    width: 45,
    height: 45,
    padding: 5,
    borderRadius: 10
  }
});

export default withNavigation(OtpScreen);
