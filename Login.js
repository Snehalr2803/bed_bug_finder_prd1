import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  ScrollView,
  NavigatorIOS,
  Alert,
  Image,
  ToastAndroid,
  ActivityIndicator,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  StatusBar,
  Keyboard,
  TouchableWithoutFeedback,
  UIManager,
  LayoutAnimation
} from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import { withNavigation } from "react-navigation";
import { authorize, refresh, revoke } from "react-native-app-auth";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { ScaledSheet } from "react-native-size-matters";
import { scale, verticalScale, moderateScale } from "react-native-size-matters";
import APISingletonClass, { LOGIN_API } from "./API";
import Storage from "react-native-storage";
import AsyncStorage from "@react-native-community/async-storage";
import Spinner from "react-native-loading-spinner-overlay";
import TokenClient from "@okta/okta-react-native";
import InstagramLogin from 'react-native-instagram-login'
import { GoogleSignin, GoogleSigninButton,statusCodes } from 'react-native-google-signin';

//import {LoginManager} from "react-native-fbsdk"
//import LinkedInModal from 'react-native-linkedin'
var {FBLogin, FBLoginManager} = require('react-native-facebook-login');
import CookieManager from 'react-native-cookies';
const tokenClient = new TokenClient({
  issuer: "https://dev-311674.okta.com/oauth2/default",
  client_id: "0oatq8sxzwPZ8ejdv356",
  scope: "openid profile",
  redirect_uri: __DEV__
    ? "com.okta.infinit:/callback"
    : "com.okta.infinit:/callback"
  // ? "com.okta.dev-311674:/callback"
  // : "com.okta.dev-311674:/callback"
});
UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);
const config = {
  issuer: "https://dev-311674.okta.com/oauth2/default",
  clientId: "0oatq8sxzwPZ8ejdv356",
  //clientSecret: "533790692053f008fa862ec94e65d2b8",
  redirectUrl: "com.okta.infinit:/callback",
  serviceConfiguration: {
    authorizationEndpoint: "https://dev-311674.okta.com/oauth2/v1/authorize",
    tokenEndpoint: "https://dev-311674.okta.com/oauth2/v1/token",
    registrationEndpoint: "https://dev-311674.okta.com/oauth2/v1/clients"
  },
  additionalParameters: {
    prompt: "login"
  },
  scopes: ["openid", "profile", "email", "offline_access"],
  usePKCE: true
};
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      loading: false,
      animating: false,
      loginInProcess: false,
      screenHeight: Dimensions.get("window").height,
      authenticated: false,
      hasLoggedInOnce: false,
      accessToken: "",
      accessTokenExpirationDate: "",
      refreshToken: ""
    };
    this.inputRefs = {};
    this.validate = this.validate.bind(this);
    // this.checkAuthentication = this.checkAuthentication.bind(this);
    // this.login = this.login.bind(this);
    //this.logout = this.logout.bind(this);
  }
  animateState(nextState, delay) {
    setTimeout(() => {
      this.setState(() => {
        LayoutAnimation.easeInEaseOut();
        return nextState;
      });
    }, delay);
  }
  // moveToAnother(){
  //   this.props.navigation.navigate("ProfileNav", {
  //     DataHomeComment: ""
  //   })
  // }
 async _loginFacebook(){
const thisSatate = this
try{
  FBLoginManager.setLoginBehavior(FBLoginManager.LoginBehaviors.Web); // defaults to Native
 
FBLoginManager.loginWithPermissions(["email","user_friends"], function(error, data){
  if (!error) {
    console.log("Login data: ", data);
    //this.storeData("SocialLoginType", "Facebook");

    thisSatate.setInstToken(data.credentials.token,"Facebook")
    // setTimeout(() => {
    //   Alert.alert(
    //     "BBF",
    //     "Login Successful.",
    //     [
    //       {
    //         text: "OK",
    //         onPress: () => thisSatate.props.navigation.navigate("ProfileNav", {
    //           DataHomeComment: ""
    //         })
    //         }
    //     ],
    //     { cancelable: false }
    //   );
    // }, 200);
  } else {
    console.log("Error: ", error);
  }
})
// let result = await LoginManager.logInWithPermissions(["public_profile"])
// if (result.isCancelled){
//   Alert.alert(
//     "BBF",
//     "login was cancelled.",
//     [{ text: "OK", onPress: () => console.log("OK Pressed") }],
//     { cancelable: false }
//   );
// }else{
//   Alert.alert(
//     "BBF",
//     "login successfull with permission."+result.grantedPermissions.toString(),
//     [{ text: "OK", onPress: () => console.log("OK Pressed") }],
//     { cancelable: false }
//   );
// }
}catch(error){
  Alert.alert(
    "BBF",
    "login Failed with."+error,
    [{ text: "OK", onPress: () => console.log("OK Pressed") }],
    { cancelable: false }
  );
}
}
  loginOptions() {
    // this.props.navigation.navigate("ProfileNav", {
    //   DataHomeComment: ""
    // });
  }
  storeData = async (Key, Value) => {
    try {
      await AsyncStorage.setItem(Key, Value);
    } catch (e) {
      // saving error
    }
  };
  authorize = async () => {

    Alert.alert(
      "BBF",
      "Comming Soon.",
      [{ text: "OK", onPress: () => console.log("OK Pressed") }],
      { cancelable: false }
    );
    return;
    try {
      const authState = await authorize(config);
      this.animateState(
        {
          hasLoggedInOnce: true,
          accessToken: authState.accessToken,
          accessTokenExpirationDate: authState.accessTokenExpirationDate,
          refreshToken: authState.refreshToken
        },
        500
      );
    } catch (error) {
      Alert.alert("Failed to log in", error.message);
    }
  };
  revoke = async () => {
    try {
      await revoke(config, {
        tokenToRevoke: this.state.accessToken,
        sendClientId: true
      });
      this.animateState({
        accessToken: "",
        accessTokenExpirationDate: "",
        refreshToken: ""
      });
    } catch (error) {
      Alert.alert("Failed to revoke token", error.message);
    }
  };
  getSignInToken(){
    GoogleSignin.getAccessToken()
.then((token) => {
  console.log(token);
})
.catch((err) => {
  console.log(err);
})
.done();
  }

  componentWillMount(){
    this._subscribe = this.props.navigation.addListener("didFocus", () => {
      //this.clearCache()
      GoogleSignin.hasPlayServices({ autoResolve: true }).then(() => {
        // play services are available. can now configure library
        GoogleSignin.configure({
        iosClientId:"719586895703-3gnse6rpvkdqnjk2e8fcmrqeo08fi4r0.apps.googleusercontent.com",
       // webClientId: Platform.OS === 'ios' ? '719586895703-3gnse6rpvkdqnjk2e8fcmrqeo08fi4r0.apps.googleusercontent.com"' : 'Client Id From Google services.json'
        // only for iOS
      })
      .then(() => {
        // you can now call currentUserAsync()
      });
    })
    .catch((err) => {
      console.log("Play services error", err.code, err.message);
    })
     
    });
  }
  componentDidMount() {
    GoogleSignin.hasPlayServices({ autoResolve: true }).then(() => {
      // play services are available. can now configure library
      GoogleSignin.configure({
      iosClientId:"719586895703-3gnse6rpvkdqnjk2e8fcmrqeo08fi4r0.apps.googleusercontent.com",
     // webClientId: Platform.OS === 'ios' ? '719586895703-3gnse6rpvkdqnjk2e8fcmrqeo08fi4r0.apps.googleusercontent.com"' : 'Client Id From Google services.json'
      // only for iOS
    })
    .then(() => {
      // you can now call currentUserAsync()
      // GoogleSignin.currentUserAsync().then((user) => {
      //   console.log('USER', user);
      //   this.setState({user: user});
      // }).done();
      //   this.getData("token");
    });
  })
  .catch((err) => {
    console.log("Play services error", err.code, err.message);
  })

  // CookieManager.clearAll()
  //     .then((res) => {
  //       console.log('CookieManager.clearAll =>', res);
  //       //this.setState({ token: '' })
  //     });
  // this.clearCache()

    // CookieManager.getAll()
    // .then((res) => {
    //   console.log('CookieManager.getAll =>', res);
    // });
   
    //  this.checkAuthentication();
  }
  // async checkAuthentication() {
  //   const authenticated = await tokenClient.isAuthenticated();
  //   if (authenticated !== this.state.authenticated) {
  //     this.setState({ authenticated: authenticated });
  //   }
  // }
  // async login() {
  //   await tokenClient.signInWithRedirect();
  //   this.checkAuthentication();
  // }
  // async logout() {
  //   await tokenClient.signOut();
  //   this.checkAuthentication();
  // }

  getData = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Token", value);
        // Alert.alert(
        //   "BBF",
        //   value,
        //   [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        //   { cancelable: false }
        // );
        this.setState({
          token: value
        });

        this.GetEula();
        //this.GetProfile();
      }
    } catch (e) {
      // error reading value
    }
  };
  validate = () => {
    let text = this.state.email;
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      console.log("Email is Not Correct");
      this.setState({ email: text });
      return false;
    } else {
      this.setState({ email: text });
      console.log("Email is Correct");
      return true;
    }
  };
  showInstagram(){
    this.refs['instagramLogin'].show();
    }
  clearCache(){
    CookieManager.clearAll()
      .then((res) => {
        console.log('CookieManager.clearAll =>', res);
        //this.setState({ token: '' })
      });
  }
  setInstToken(tokenSent,Ligintype){
    console.log("Social login token",tokenSent)
    this.storeData("SocialLoginType", Ligintype);
    if (Ligintype == "LinkedIn"){
      this.storeData("Socialtoken", tokenSent.access_token);
    }else{
      this.storeData("Socialtoken", tokenSent);
      // CookieManager.clearAll()
      // .then((res) => {
      //   console.log('CookieManager.clearAll =>', res);
      //   //this.setState({ token: '' })
      // });
    }
    
    this.storeData("isFromSocial", "true");
    setTimeout(() => {
      Alert.alert(
        "BBF",
        "Login Successful.",
        [
          {
            text: "OK",
            onPress: () =>
              this.props.navigation.navigate("ProfileNav", {
                fromSocialLogin: "true"
              })
          }
        ],
        { cancelable: false }
      );
    }, 200);
  }
  signIn = async () => {
    console.log("Inside Sign In")
    
    try {
      await GoogleSignin.hasPlayServices();
      console.log("After Google play srevice",GoogleSignin.hasPlayServices())
      const userInfo = await GoogleSignin.signIn();
      console.log("User Information",userInfo)
      this.setState({ userInfo });
      this.storeData("isFromSocial", "true");
      this.storeData("SocialLoginType", "Google");
      this.storeData("Socialtoken",userInfo.idToken)
      this.storeData("GoogleInfo",JSON.stringify(userInfo))
     // AsyncStorage.setItem('GoogleInfo', , () => {});
    setTimeout(() => {
      Alert.alert(
        "BBF",
        "Login Successful.",
        [
          {
            text: "OK",
            onPress: () =>
              this.props.navigation.navigate("ProfileNav", {
                fromSocialLogin: "true",
                userInformation:userInfo
              })
          }
        ],
        { cancelable: false }
      );
    }, 200);
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        Alert.alert(
          "BBF",
          "user cancelled the login flow.",
          [{ text: "OK", onPress: () => console.log("OK Pressed") }],
          { cancelable: false }
        );
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
        Alert.alert(
          "BBF",
          "operation (f.e. sign in) is in progress already.",
          [{ text: "OK", onPress: () => console.log("OK Pressed") }],
          { cancelable: false }
        );
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {

        // play services not available or outdated
        Alert.alert(
          "BBF",
          " play services not available or outdated.",
          [{ text: "OK", onPress: () => console.log("OK Pressed") }],
          { cancelable: false }
        );
      } else {
        // some other error happened
        Alert.alert(
          "BBF",
          " some other error happened.",
          [{ text: "OK", onPress: () => console.log("OK Pressed") }],
          { cancelable: false }
        );
      }
    }
  };
  _loginBtnAction() {
    if (this.state.email == "") {
      Alert.alert(
        "BBF",
        "Email Id is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
      //alert("Incorrect Email or Password");

      //Alert.alert("Email-ID Field is Mandatory.");
    } else if (this.validate() == false) {
      Alert.alert(
        "BBF",
        "Email Id Invalid.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
      //Alert.alert("Please Enter Valid Email-ID.");
    } else if (this.state.password == "") {
      Alert.alert(
        "BBF",
        "Password is required.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
      // Alert.alert("Please Enter the Password.");
    } else {
      //this.props.navigation.push('Friends')
      // if (!this.state.loginInProcess) {
      this.setState({
        loginInProcess: true,
        animating: true
      });

      let body = {
        userName: this.state.email,
        password: this.state.password
      };
      console.log("Body Print:");
      console.log(body);
      //http://51.143.17.1:3310/api/User/Login?userName=s4%40gmail.com&password=Snehal123
      console.log("Body Added:", body);
      console.warn("API:-",LOGIN_API)
      APISingletonClass.getInstance().postMethodServiceHandler(
        LOGIN_API,
        body,
        "",
responce => {
          let responseJson = JSON.parse(responce);
          console.log("responseJson", responseJson);
          //console.warn("_loginServiceHandler " + responseJson.status)
          this.setState({
            animating: false
          });
          if (responseJson.Status == true) {
            this.storeData("token", responseJson.Result.token);
            this.storeData("isFromSocial", "false");

            setTimeout(() => {
              Alert.alert(
                "BBF",
                "Login Successful.",
                [
                  {
                    text: "OK",
                    onPress: () =>
                      this.props.navigation.navigate("ProfileNav", {
                        fromSocialLogin: "false"
                      })
                  }
                ],
                { cancelable: false }
              );
            }, 200);
            // this.props.navigation.navigate("ProfileNav", {
            //   DataHomeComment: ""
            // });
          } else {
            setTimeout(() => {
              Alert.alert(
                "BBF",
                responseJson.ResponseException.ExceptionMessage,
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);
          }
        },
        error => {
          this.setState({
            loading: false
          });
                        this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);

          console.warn("_loginServiceHandler error " + error);
        }
      );
    }
  }
  render() {
    const { state } = this;
    var _this = this;
    var user = this.state.user;

    return (
      <View style={{ backgroundColor: "gray" }}>
        <StatusBar hidden={true} />
        <Spinner
          visible={this.state.animating}
          //textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <ImageBackground
            source={require("./Assets/main_background.png")}
            size={28}
            // borderRadius={15}
            style={{
              height: Dimensions.get("window").height,
              width: Dimensions.get("window").width,

              resizeMode: "contain"
            }}
          >
            <Image
              source={require("./Assets/bbf_logo.png")}
              size={28}
              borderRadius={15}
              style={{
                height: verticalScale(150),
                width: scale(120),
                alignSelf: "center",
                justifyContent: "center",
                resizeMode: "contain",
                marginTop:
                  Dimensions.get("window").height >= 812
                    ? moderateScale(30)
                    : moderateScale(10)
              }}
            />
{/* <FBLogin style={{ marginBottom: 10,width:50,height:10,backgroundColor:"red" }}
          permissions={["email","user_friends"]}
          onLogin={function(data){
            console.log("Logged in!");
            console.log(data);
            _this.setState({ user : data.credentials });
          }}
          onLogout={function(){
            console.log("Logged out.");
            _this.setState({ user : null });
          }}
          onLoginFound={function(data){
            console.log("Existing login found.");
            console.log(data);
            _this.setState({ user : data.credentials });
          }}
          onLoginNotFound={function(){
            console.log("No user logged in.");
            _this.setState({ user : null });
          }}
          onError={function(data){
            console.log("ERROR");
            console.log(data);
          }}
          onCancel={function(){
            console.log("User cancelled.");
          }}
          onPermissionsMissing={function(data){
            console.log("Check permissions!");
            console.log(data);
          }}
        /> */}
            <View
              style={{
                flexDirection: "row",
                padding: 10,
                alignContent: "flex-start",
                marginLeft: 30,
                marginRight: 30,
                marginTop: moderateScale(30),
                backgroundColor: "transparent",
                borderColor: "#7098AA",
                borderWidth: 1,
                borderRadius: 10,
                height: moderateScale(40)
              }}
            >
              <TextInput
                placeholder="Email"
                placeholderTextColor="#C0C0C0"
                onChangeText={email => {
                  this.setState({ email });
                }}
                value={this.state.email}
                returnKeyType={"next"}
                style={{
                  marginLeft: 10,
                  marginRight: 10,
                  textAlign: "left",
                  position: "absolute",
                  marginTop: moderateScale(5),
                  color: "white",
                  fontFamily: "SegoeUI-Light",
                  fontSize: moderateScale(20),
                  // backgroundColor: "red",
                  width: Dimensions.get("window").width - scale(80)
                }}
              />
            </View>
            {/* <GoogleSigninButton
    style={{ width: 192, height: 48 }}
    size={GoogleSigninButton.Size.Wide}
    color={GoogleSigninButton.Color.Dark}
    onPress={this.signIn}
    disabled={this.state.isSigninInProgress} 
    
    /> */}
            <InstagramLogin
        ref= {ref => this.instagramLogin= ref}
        clientId='e927e263c75d4189a86c0738e5257fb7'
 	      redirectUrl='http://bbf.dimitrisnowden.com/'
        scopes={['basic']}
        onLoginSuccess={(token) => this.setInstToken(token,"Instagram")
        }
        onLoginFailure={(data) => this.clearCache()}
        cacheEnabled={true}
        incognito={false}
        thirdPartyCookiesEnabled={false}
        sharedCookiesEnabled={false}
        domStorageEnabled={false}
    />
      {/* <InstagramLogin
        ref= {ref => this.instagramLogin= ref}
        clientId='ff6eebac370d4c1fbbc8fa2a3f0d5b9b'
 	      redirectUrl='http://bbf-dev.dimitrisnowden.com/'
        scopes={['basic']}
        onLoginSuccess={(token) => this.setInstToken(token,"Instagram")
        }
        onLoginFailure={(data) => console.log(data)}
        cacheEnabled={true}
        incognito={false}
        thirdPartyCookiesEnabled={false}
        sharedCookiesEnabled={false}
        domStorageEnabled={false}
    /> */}
            <View
              style={{
                flexDirection: "row",
                alignContent: "flex-start",
                marginLeft: 30,
                marginRight: 30,
                marginTop: moderateScale(30),
                backgroundColor: "transparent",
                borderColor: "#7098AA",
                borderWidth: 1,
                borderRadius: 10,
                height: moderateScale(40)
              }}
            >
              <TextInput
                placeholder="Password"
                placeholderTextColor="#C0C0C0"
                onChangeText={password => {
                  this.setState({ password });
                }}
                value={this.state.password}
                // returnKeyType={"next"}
                secureTextEntry={true}
                numberOfLines={1}
                style={{
                  marginLeft: 10,
                  marginRight: 10,
                  textAlign: "left",
                  position: "absolute",
                  marginTop: moderateScale(5),
                  color: "white",
                  fontFamily: "SegoeUI-Light",
                  fontSize: moderateScale(20),
                  width: Dimensions.get("window").width - scale(80)
                }}
              />
            </View>
            {/* <TouchableOpacity
            onPress={() => this.props.navigation.push("forGot")}
          > */}
            <View
              style={{
                height: 50,
                fontSize: 20,
                marginTop: moderateScale(20),
                textAlign: "center",
                //  backgroundColor: "red",
                width: 180,
                alignSelf: "center"
              }}
            >
              <Text
                style={{
                  height: 60,
                  fontSize: 18,
                  marginTop: moderateScale(10),
                  textAlign: "center",
                  fontFamily: "SegoeUI-Light",
                  color: "#FEFEFE"
                }}
                onPress={() => this.props.navigation.push("forGot")}
                title="Forgot Password?"
                // color="#AA2E2E"
                accessibilityLabel="Tap on Me"
              >
                Forgot Password?{state.refreshToken}
              </Text>
            </View>
            {/* </TouchableOpacity> */}
            <View
              style={{
                height: 60,
                fontSize: 20,
                marginTop: moderateScale(5),
                textAlign: "center",
                backgroundColor: "#484848",
                height: moderateScale(40),
                borderRadius: 18,
                borderWidth: 0.5,
                borderColor: "#7098AA",
                width: scale(150),
                alignItems: "center",
                alignContent: "center",
                alignSelf: "center"
              }}
            >
              <Text
                style={{
                  fontSize: moderateScale(20),
                  marginTop: moderateScale(4),
                  alignItems: "center",
                  width: scale(150),
                  textAlign: "center",
                  backgroundColor: "transparent",
                  height: moderateScale(50),
                  color: "white",
                  fontFamily: "SegoeUI-Light"
                }}
                onPress={this._loginBtnAction.bind(this)}
                //  onPress={}
                title="Login"
                color="black"
                accessibilityLabel="Tap on Me"
              >
                Login
              </Text>
            </View>
            <View
              style={{
                height: 60,
                fontSize: 20,
                marginTop: moderateScale(30),
                textAlign: "center",
                backgroundColor: "#484848",
                height: moderateScale(40),
                borderRadius: moderateScale(18),
                borderWidth: 0.5,
                borderColor: "#7098AA",
                width: scale(150),
                alignItems: "center",
                alignContent: "center",
                alignSelf: "center"
              }}
            >
              <Text
                style={{
                  fontSize: moderateScale(20),
                  marginTop: moderateScale(4),
                  width: scale(150),
                  textAlign: "center",
                  backgroundColor: "transparent",
                  height: moderateScale(50),
                  color: "white",
                  fontFamily: "SegoeUI-Light",
                  alignItems: "center"
                }}
                onPress={() =>
                  this.props.navigation.navigate("signUp", {
                    DataHomeComment: ""
                  })
                }
                title="Login"
                color="black"
                accessibilityLabel="Tap on Me"
              >
                Sign Up
              </Text>
            </View>
            <Text
              style={{
                fontSize: moderateScale(28),
                color: "#FEFEFE",
                textAlign: "center",
                fontWeight: "200",
                marginTop: moderateScale(20),
                fontFamily: "SegoeUI-Light"
              }}
            >
              {" "}
              sign in with...
            </Text>
            <View
              style={styles.footerWrapperlikeCommentShare}
              backgroundColor="clear"
            >
              <TouchableOpacity onPress={this._loginFacebook.bind(this)}>
                <Image
                  source={require("./Assets/facebook.png")}
                  //size={20}
                  //color="gray"
                  marginTop={5}
                  marginLeft={5}
                  resizeMode="contain"
                  style={styles.imageSize2}
                />
              </TouchableOpacity>
              <TouchableOpacity onPress={()=> this.instagramLogin.show()}>
                <Image
                  source={require("./Assets/instgram.png")}
                  //size={20}
                  //color="gray"
                  marginTop={5}
                  marginLeft={5}
                  resizeMode="contain"
                  style={styles.imageSize2}
                />
             
              </TouchableOpacity>
              <TouchableOpacity onPress={this.signIn}>
                <Image
                  source={require("./Assets/g.png")}
                  //size={20}
                  //color="gray"
                  marginTop={5}
                  marginLeft={5}
                  resizeMode="contain"
                  style={styles.imageSize2}
                />
              </TouchableOpacity>
             {/*  <TouchableOpacity onPress={this.authorize.bind(this)}>
                <View style={{backgroundColor:"transparent"}}>
             
                <Image
                  source={require("./Assets/linkedin.png")}
                  //size={20}
                  //color="gray"
                  marginTop={5}
                  marginLeft={5}
                  resizeMode="contain"
                  style={styles.imageSize2}
                />
                <View style={{marginTop:-50}}>
                 {/* <LinkedInModal
          clientID="81k46boqsux57a"
          clientSecret="YCVSxIZbn0mitf8b"
          permissions={['r_liteprofile', 'r_emailaddress']}
          redirectUri="http://51.143.17.1:3310/api/User/Login"
          onSuccess={token => this.setInstToken(token,"LinkedIn")}
          //renderButton={{width:100,height:100,backgroundColor:"red"}}
        /> */}
        {/* </View>
                </View>
              </TouchableOpacity> */} 
            </View>
          </ImageBackground>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ecf0f1"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 10
  },
  sharingtext: {
    color: "#AA2E2E",
    backgroundColor: "#fff",
    borderRadius: 14,
    borderColor: "#AA2E2E",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 18,
    width: 130,
    height: 30,
    alignItems: "center",
    alignContent: "center",
    textAlignVertical: "center"
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover" // or 'stretch'
  },
  footerWrapperlikeCommentShare: {
    flexWrap: "nowrap",
    marginTop:
      Dimensions.get("window").height == 568
        ? moderateScale(5)
        : moderateScale(25),
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginLeft: 30,
    marginRight: 30,
  },
  separatorlikeShare: {
    flex: 0,
    borderWidth: 1,
    borderColor: "#ECEDED"
  },
  imageSize2: {
    width: 45,
    height: 45,
    padding: 5,
    borderRadius: 10
  }
});

export default withNavigation(Login);
