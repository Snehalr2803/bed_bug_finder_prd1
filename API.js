//import console = require("console");

//---- API.js ----
//export const BASEURL = "http://192.168.0.12:1011";
//http://bbf-dev.dimitrisnowden.com
export const BASEURL = "http://bbf.dimitrisnowden.com";

//export const BASEURL = "https://sunny-bbe.azurewebsites.net";

//export const BASEURL = "http://51.143.17.1:3310";
export const SIGNUP_API = BASEURL + "/api/User/CreateUser";

export const LOGIN_API = BASEURL + "/api/User/Login";
export const CrossCheckkey = "Snehal";

//http://51.143.17.1:3310/api/User/RecoverPassword

export const RECOVERY_API = BASEURL + "/api/User/RecoverPassword";

export const RESET_API = BASEURL + "/api/User/ResetPassword";

export const EULA_API = BASEURL + "/api/Eula/GetEula";

export const EULA_CHECK_API = BASEURL + "/api​/Eula​/GetEulas";

export const EULAACCEPT_API = BASEURL + "/api/Eula/AcceptEula";

export const PROFILE_API = BASEURL + "/api/User/GetProfileInfo";

export const UPDATE_PROFILE_API = BASEURL + "/api/User/UpdateProfile";

export const GET_WATCHLIST_API = BASEURL + "/api/Incident/GetWatchList";

export const REPORT_INCIDENT = BASEURL + "​/api​/Incident​/ReportIncident";

export const SEARCH_REPORT_INCIDENT = BASEURL + "​/api​/Incident​/GetIncidents";

export const ADD_WATCHLIST = BASEURL + "/api​/Incident​/AddToWatchList";
export const LOGOUT = BASEURL + "/api​/User​/Logout";
export const GET_NEARBY_INCIDENTS =
  BASEURL + "/api/Incident/GetNearByIncidents";
export default class APIServiceHandlerSingletonClass {
  static myInstance = null;

  /**
   * @returns {APIServiceHandlerSingletonClass}
   */
  static getInstance() {
    if (APIServiceHandlerSingletonClass.myInstance == null) {
      APIServiceHandlerSingletonClass.myInstance = new APIServiceHandlerSingletonClass();
    }
    return this.myInstance;
  }
  postMethodServiceHandler = (
    urlStr,
    params,
    token,
    callbackResponce = jsonResponse => {},
    callbackError = errorResponse => {}
  ) => {
    let headers = { "Content-Type": "application/json" };
    if (token != "") {
      headers["Authorization"] = token;
    }
    console.log("passToken", token);
    console.log("passHeader", headers);
    fetch(urlStr, {
      method: "POST",
      headers: headers,

      body: JSON.stringify(params)
    })
      .then(response => response.json())
      .then(responseJson => {
        //console.warn("postMethodServiceHandler", responseJson);
        callbackResponce(JSON.stringify(responseJson));
      })
      .catch(error => callbackError(error));
  };

  // postMethodServiceHandler = (
  //   urlStr,
  //   params,
  //   token,
  //   farmAvailable,
  //   callbackResponce = jsonResponse => {},
  //   callbackError = errorResponse => {}
  // ) => {
  //   let headers = { "Content-Type": farmAvailable };
  //   // if (farmAvailable != "") {
  //   //   headers["Content-Type"] = farmAvailable;
  //   // }else{
  //   //   headers["Content-Type"] = farmAvailable;
  //   // }
  //   if (token != "") {
  //     headers["Authorization"] = token;
  //   }
  //   console.log("passToken", token);
  //   console.log("passHeader", headers);
  //   fetch(urlStr, {
  //     method: "POST",
  //      headers: headers,

  //     body: JSON.stringify(params)
  //   })
  //     .then(response => response.json())
  //     .then(responseJson => {
  //       //console.warn("postMethodServiceHandler", responseJson);
  //       callbackResponce(JSON.stringify(responseJson));
  //     })
  //     .catch(error => callbackError(error));
  // };
  postMethodServiceHandlerSignUp = (
    urlStr,
    params,
    token,
    callbackResponce = jsonResponse => {},
    callbackError = errorResponse => {}
  ) => {
    console.log("passToken", token);
    //console.log("passHeader", headers);
    fetch(urlStr, {
      method: "POST",
      body: params
    })
      .then(response => response.json())
      .then(responseJson => {
        //console.warn("postMethodServiceHandler", responseJson);
        callbackResponce(JSON.stringify(responseJson));
      })
      .catch(error => callbackError(error));
  };

  getMethodServiceHandler = (
    urlStr,
    param,
    token,
    callbackResponce = jsonResponse => {},
    callbackError = errorResponse => {}
  ) => {
    console.log("passToken", token);
    fetch(urlStr, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: token
      }
    })
      .then(response => response.json())
      .then(responseJson => {
        // console.warn("getMethodServiceHandler", responseJson);

        callbackResponce(JSON.stringify(responseJson));
      })
      .catch(error => callbackError(error));
  };
}
