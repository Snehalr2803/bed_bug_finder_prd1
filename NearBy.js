import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  ScrollView,
  NavigatorIOS,
  Alert,
  Image,
  ToastAndroid,
  ActivityIndicator,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  StatusBar,
  Switch,
  FlatList
} from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import SearchBar from "react-native-search-bar";
import { withNavigation } from "react-navigation";
//import { Switch } from "react-native-switch";
import ToggleSwitch from "toggle-switch-react-native";
import MapView from "react-native-maps";
import { Marker } from "react-native-maps";
import APISingletonClass, { GetNearByIncidents, BASEURL } from "./API";
import { ScaledSheet } from "react-native-size-matters";
import { scale, verticalScale, moderateScale } from "react-native-size-matters";
import AsyncStorage from "@react-native-community/async-storage";
import Geolocation from "@react-native-community/geolocation";
import Spinner from "react-native-loading-spinner-overlay";
class NearBy extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      token: "",
      loading: false,
      animating: false,
      loginInProcess: false,
      switchedBool: false,
      visible: false,
      ProfilePic: "",
      mapRegion: {},
      lastLat: null,
      lastLong: null,
      initialPosition: "unknown",
      lastPosition: "unknown",

      region: {
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
      },
      wtachList: [
        {
          name: "Brian J Virag Law Offices",
          avatar_url: "https://randomuser.me/api/portraits/med/men/77.jpg",
          subtitle: "16400 Ventura Blvd,Encino,CA"
        }
      ],
      markers: [],
      region: {
        latitude: 45.52220671242907,
        longitude: -122.6653281029795,
        latitudeDelta: 0.04864195044303443,
        longitudeDelta: 0.040142817690068
      }
    };
    var watchID = null;
    //   this.inputRefs = {};
    //   this.validate = this.validate.bind(this);
  }

  getData = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Pic", value);
        this.setState({
          ProfilePic: value
        });

        //this.GetProfile();
      }
    } catch (e) {
      // error reading value
    }
  };
  getLatLongRegion = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        if (key == "LattitudeSave") {
          this.setState({ lastLat: value });
        } else if (key == "LongitudedeSave") {
          this.setState({ lastLong: value });
          let region = {
            latitude: this.state.lastLat,
            longitude: this.state.lastLong,
            latitudeDelta: 0.00922 * 1.5,
            longitudeDelta: 0.00421 * 1.5
          };
          this.setState({ mapRegion: region });
        } else {
          this.setState({ mapRegion: value });
        }
        // value previously stored
        console.log("Got Pic", value);
        // this.setState({
        //   ProfilePic: value
        // });

        //this.GetProfile();
      }
    } catch (e) {
      // error reading value
    }
  };
  onRegionChange(region, lastLat, lastLong) {
    console.log("Last Lattitude", lastLat);
    console.log("Last longitude", lastLong);
    this.setState({
      mapRegion: region,
      // If there are no new values set the current ones
      lastLat: lastLat || this.state.lastLat,
      lastLong: lastLong || this.state.lastLong
    });
    if (this.state.lastLat != null && this.state.lastLong != null) {
      this.GetNearByIncidents();
    }
  }
  getToken = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Pic Token", value);
        this.setState({
          token: value
        });

        // } else {
        //   this.setState({
        //     profileImage: value
        //   });
        // }
        //this.GetProfile();
      }
    } catch (e) {
      // error reading value
    }
  };
  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchID);
  }
  componentWillMount() {
    this._subscribe = this.props.navigation.addListener("didFocus", () => {
      this.getData("profilePhoto");
      this.getToken("token");
      this.getLatLongRegion("LattitudeSave");
      this.getLatLongRegion("LongitudedeSave");
      //this.getLatLongRegion("RegionSave");
      this.GetNearByIncidents();
      //this.callLocation();
      // this.callLocation();
      //   navigator.geolocation.getCurrentPosition(
      //     position => {
      //       // Create the object to update this.state.mapRegion through the onRegionChange function
      //       let region = {
      //         latitude: position.coords.latitude,
      //         longitude: position.coords.longitude,
      //         latitudeDelta: 0.00922 * 1.5,
      //         longitudeDelta: 0.00421 * 1.5
      //       };
      //       this.setState({
      //         mapRegion: region,
      //         // If there are no new values set the current ones
      //         lastLat: position.coords.latitude,
      //         lastLong: position.coords.longitude
      //       });
      //       console.log("Region", region);
      //       console.log("Region", region);
      //       this.onRegionChange(region, region.latitude, region.longitude);
      //       if (this.state.lastLat != null && this.state.lastLong != null) {
      //         this.GetCount();
      //       }
      //       //this.onRegionChange(region, region.latitude, region.longitude);
      //       //     if (this.state.lastLat != null && this.state.lastLong != null) {
      //       //   this.GetNearByIncidents();
      //       // }
      //     },
      //     error => Alert.alert("Error", JSON.stringify(error))
      //     // { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
      //   );
      // });
    });
  }
  componentDidMount() {
    console.log("Component Did Mount Call.");
    this.getData("profilePhoto");
    this.getToken("token");
    this.getLatLongRegion("LattitudeSave");
    this.getLatLongRegion("LongitudedeSave");
    //this.getLatLongRegion("RegionSave");

    this.GetNearByIncidents();
    //this.callLocation();
    // Geolocation.getCurrentPosition(
    //   position => {
    //     const initialPosition = JSON.stringify(position);
    //     console.log("Initial Position", initialPosition);
    //     this.setState({ initialPosition });
    //   },
    //   error => Alert.alert("Error", JSON.stringify(error)),
    //   { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    // );
    // this.watchID = Geolocation.watchPosition(position => {
    //   const lastPosition = JSON.stringify(position);
    //   console.log("Last Position", lastPosition);
    //   this.setState({ lastPosition });
    // });

    // navigator.geolocation.getCurrentPosition(
    //   position => {
    //     // Create the object to update this.state.mapRegion through the onRegionChange function
    //     let region = {
    //       latitude: position.coords.latitude,
    //       longitude: position.coords.longitude,
    //       latitudeDelta: 0.00922 * 1.5,
    //       longitudeDelta: 0.00421 * 1.5
    //     };
    //     this.setState({
    //       mapRegion: region,
    //       // If there are no new values set the current ones
    //       lastLat: position.coords.latitude,
    //       lastLong: position.coords.longitude
    //     });
    //     console.log("Region", region);
    //     console.log("Region", region);
    //     this.onRegionChange(region, region.latitude, region.longitude);
    //     if (this.state.lastLat != null && this.state.lastLong != null) {
    //       this.GetNearByIncidents();
    //     }
    //     //this.onRegionChange(region, region.latitude, region.longitude);
    //     //     if (this.state.lastLat != null && this.state.lastLong != null) {
    //     //   this.GetNearByIncidents();
    //     // }
    //   },
    //   error => Alert.alert("Error", JSON.stringify(error))
    //   // { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    // );

    // this.watchID = navigator.geolocation.watchPosition(
    //   position => {
    //     // Create the object to update this.state.mapRegion through the onRegionChange function
    //     let region = {
    //       latitude: position.coords.latitude,
    //       longitude: position.coords.longitude,
    //       latitudeDelta: 0.00922 * 1.5,
    //       longitudeDelta: 0.00421 * 1.5
    //     };
    //     console.log("Region",region)
    //     this.onRegionChange(region, region.latitude, region.longitude);
    // //     if (this.state.lastLat != null && this.state.lastLong != null) {
    // //   this.GetNearByIncidents();
    // // }
    //   },
    //   error => console.log(error),
    //   {enableHighAccuracy: false, timeout: 10000, maximumAge: 3000}
    // );
  }
  GetNearByIncidents() {
    //this.props.navigation.push('Friends')
    this.setState({
      loginInProcess: true,
      animating: true
    });
    let body = {};
    // console.log("Body Print:");
    // console.log(body);
    // console.log("EulaIdd", this.state.eulaId);
   // const BASEURL1 = "http://51.143.17.1:3310";
  //var BASEURL1 =  "http://192.168.0.12:1011"
  var BASEURL1 =  "http://bbf.dimitrisnowden.com"
    console.log(
      "Url Printing",
      BASEURL1 +
        "/api/Incident/GetNearByIncidents?" +
        "lat=" +
        String(this.state.lastLat) +
        "&lan=" +
        String(this.state.lastLong)
    );
    //var BASEURL =  "http://192.168.0.20:1011"

    APISingletonClass.getInstance().getMethodServiceHandler(
      BASEURL1 +
        "/api/Incident/GetNearByIncidents?" +
        "lat=" +
        String(this.state.lastLat) +
        "&lan=" +
        String(this.state.lastLong),
      body,
      "",
      responce => {
        console.log("responseJson Eula", responce);
        let responseJson = JSON.parse(responce);
        this.setState({
          animating: false
        });
        console.log("responseJson Eula", responseJson);
        //console.warn("_loginServiceHandler " + responseJson.status)
        if (responseJson.Status == true) {
          this.setState({
            markers: responseJson.Result
          });
          // if (responseJson.Result.accepted) {
          //   this.props.navigation.navigate("homePageBbf", {
          //     DataHomeComment: ""
          //   });
          // }
        } else {
        }
      },
      error => {
        this.setState({
          loading: false
        });
                      this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);

        console.warn("_loginServiceHandler error " + error);
      }
    );
  }
  callLocation() {
    // alert("callLocation Called");
    navigator.geolocation.getCurrentPosition(
      // Will give you the current location
      position => {
        const currentLongitude = JSON.stringify(position.coords.longitude);
        // getting the Longitude from the location json
        const currentLatitude = JSON.stringify(position.coords.latitude);

        let region = {
          latitude: currentLatitude,
          longitude: currentLongitude,
          latitudeDelta: 0.00922 * 1.5,
          longitudeDelta: 0.00421 * 1.5
        };
        this.setState({
          mapRegion: region,
          // If there are no new values set the current ones
          lastLat: currentLatitude,
          lastLong: currentLongitude
        });
        console.log("Region", region);
        console.log("Region", region);
        //this.onRegionChange(region, region.latitude, region.longitude);
        if (this.state.lastLat != null && this.state.lastLong != null) {
          this.GetNearByIncidents();
        }
        // getting the Latitude from the location json
        // that.setState({ currentLongitude });
        // // Setting state Longitude to re re-render the Longitude Text
        // that.setState({ currentLatitude });
        // Setting state Latitude to re re-render the Longitude Text
      },
      error => alert(error.message),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
    this.watchID = navigator.geolocation.watchPosition(position => {
      // Will give you the location on location change
      console.log(position);
      const currentLongitude = JSON.stringify(position.coords.longitude);
      // getting the Longitude from the location json
      const currentLatitude = JSON.stringify(position.coords.latitude);
      // getting the Latitude from the location json
      // that.setState({ currentLongitude });
      // // Setting state Longitude to re re-render the Longitude Text
      // that.setState({ currentLatitude });
      // Setting state Latitude to re re-render the Longitude Text

      let region = {
        latitude: currentLatitude,
        longitude: currentLongitude,
        latitudeDelta: 0.00922 * 1.5,
        longitudeDelta: 0.00421 * 1.5
      };
      this.setState({
        mapRegion: region,
        // If there are no new values set the current ones
        lastLat: currentLatitude,
        lastLong: currentLongitude
      });
      console.log("Region", region);
      console.log("Region", region);
      //this.onRegionChange(region, region.latitude, region.longitude);
      if (this.state.lastLat != null && this.state.lastLong != null) {
        this.GetNearByIncidents();
      }
    });
  }
  loginOptions() {
    this.props.navigation.navigate("ProfileNav", {
      DataHomeComment: ""
    });
  }
  searchButtonPressed() {
    this.setState({ visible: !this.state.visible });
  }

  optionPress(option) {
    if (option == "0") {
    } else if (option == "1") {
    } else if (option == "2") {
    } else if (option == "3") {
    }
  }
  getInitialState() {
    return {
      region: {
        latitude: 37.78825,
        longitude: -122.4324,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
      }
    };
  }

  //   onRegionChange(region) {
  //     this.setState({ region: });
  //   }
  render() {
    return (
      <View style={{ backgroundColor: "gray" }}>
        <StatusBar hidden={true} />
        <Spinner
          visible={this.state.animating}
          //textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <MapView
          region={this.state.mapRegion}
          showsUserLocation={true}
          // onRegionChange={this.onRegionChange}
          style={styles.map}
        >
          {this.state.markers.map((marker, index12) => (
            <MapView.Marker
              coordinate={{ latitude: marker.lat, longitude: marker.lan }}
              title={
                marker.location != null ? marker.location.split(",")[0] : ""
              }
              description={marker.location}
              onPress={() =>
                this.props.navigation.navigate("incidentsVC", {
                  latitude: marker.lat,
                  longitude: marker.lan,
                  token: this.state.token,
                  marker: marker
                })
              }
            >
              <Image
                source={require("./Assets/warning2.png")}
                style={{ width: 40, height: 40 }}
              />
            
                <View
                  style={{
                    position: "absolute",
                    alignItems: "center",

                    right: 0,
                    top: -10,
                    bottom: 0,
                    backgroundColor: "#FF7200",
                    width: 25,
                    height: 25,
                    borderRadius: 20
                  }}
                >
                  <Text
                    style={{
                      fontSize: 16,
                      color: "#FEFEFE",
                      alignSelf: "center",
                      fontWeight: "500",

                      textAlign: "center"
                    }}
                  >
                  
                  {this.state.markers.filter(u=>u.lat===marker.lat&&u.lan===marker.lan).length}
                  </Text>
                </View>
              
            </MapView.Marker>
          ))}
        </MapView>

        <View style={styles.backgroundContainer}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between"
            }}
          >
            <View
              style={{
                flexDirection: "row",
                width: scale(230),
                left: 15,
                backgroundColor: "transparent"
              }}
            >
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image
                  source={require("./Assets/back.png")}
                  size={28}
                  borderRadius={15}
                  //onPress={this.props.navigation.goBack()}
                  style={{
                    height: verticalScale(60),
                    width: scale(30),
                    alignSelf: "center",
                    resizeMode: "contain",
                    marginTop: moderateScale(24),
                    backgroundColor: "transparent",
                    justifyContent: "flex-start",
                    backgroundColor: "transparent"
                  }}
                />
              </TouchableOpacity>
              <Text
                style={{
                  fontSize: moderateScale(25),
                  color: "#FEFEFE",
                  textAlign: "center",
                  fontWeight: "200",
                  marginTop:
                    Dimensions.get("window").height >= 812
                      ? moderateScale(42)
                      : moderateScale(32),
                  marginLeft: 0,
                  marginRight: 20,
                  backgroundColor: "transparent",
                  fontFamily: "SegoeUI-Light"
                }}
              >
                {" "}
                N E A R B Y
              </Text>
            </View>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("ProfileNav1", {
                  DataHomeComment: ""
                })
              }
            >
              <View
                style={{
                  marginRight: 10,
                  marginTop: moderateScale(10),
                  alignSelf: "flex-end",
                  width: moderateScale(75),
                  height: moderateScale(75),
                  borderRadius: moderateScale(40),
                  borderWidth: moderateScale(3),
                  borderColor: "#525353",
                  backgroundColor: "transparent"
                }}
              >
                <Image
                  style={styles.avatar}
                  // source={require("./Assets/BrianImage.jpeg")}
                  source={{
                    uri: BASEURL + "/" + this.state.ProfilePic
                  }}
                  onPress={this.chooseFile}
                  resizeMode="cover"
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ecf0f1"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 10
  },
  sharingtext: {
    color: "#AA2E2E",
    backgroundColor: "#fff",
    borderRadius: 14,
    borderColor: "#AA2E2E",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 18,
    width: 130,
    height: 30,
    alignItems: "center",
    alignContent: "center",
    textAlignVertical: "center"
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover" // or 'stretch'
  },
  footerWrapperlikeCommentShare: {
    flexWrap: "nowrap",
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginLeft: 30,
    marginRight: 30
  },
  separatorlikeShare: {
    flex: 0,
    borderWidth: 1,
    borderColor: "#ECEDED"
  },
  imageSize2: {
    width: 60,
    height: 60,
    padding: 5,
    borderRadius: 10
  },
  avatar: {
    width: moderateScale(70),
    height: moderateScale(70),
    borderRadius: moderateScale(35),
    borderWidth: moderateScale(3),
    borderColor: "#7098AA",
    alignItems: "center"
  },
  header_footer_style: {
    width: "100%",
    height: 44,
    backgroundColor: "#4CAF50",
    alignItems: "center",
    justifyContent: "center"
  },

  textStyle: {
    textAlign: "center",
    color: "#fff",
    fontSize: 21
  },
  map: {
    height: Dimensions.get("window").height,
    width: Dimensions.get("window").width,
    resizeMode: "contain"
  },
  backgroundContainer: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    height: 100,
    backgroundColor: "rgba(76,76,76,0.5)"
  }
});

export default withNavigation(NearBy);
