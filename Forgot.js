import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  ScrollView,
  NavigatorIOS,
  Alert,
  Image,
  AsyncStorage,
  ToastAndroid,
  ActivityIndicator,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  StatusBar,
  TouchableWithoutFeedback,
  Keyboard
} from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Spinner from "react-native-loading-spinner-overlay";
import { withNavigation } from "react-navigation";
import { ScaledSheet } from "react-native-size-matters";
import { scale, verticalScale, moderateScale } from "react-native-size-matters";
import APISingletonClass, { RECOVERY_API } from "./API";
class Forgot extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      loading: false,
      animating: false,
      loginInProcess: false,
      animating: false
    };
    this.inputRefs = {};
    this.validate = this.validate.bind(this);
  }

  loginOptions() {
    this.props.navigation.navigate("ProfileNav", {
      DataHomeComment: ""
    });
  }
  validate = () => {
    let text = this.state.email;
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      console.log("Email is Not Correct");
      this.setState({ email: text });
      return false;
    } else {
      this.setState({ email: text });
      console.log("Email is Correct");
      return true;
    }
  };
  _loginBtnAction() {
    if (this.state.email == "") {
      Alert.alert(
        "BBF",
        "Email is required.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (this.validate() == false) {
      Alert.alert(
        "BBF",
        "Email Invalid.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else {
      //this.props.navigation.push('Friends')
      if (!this.state.loginInProcess) {
        this.setState({
          loginInProcess: true,
          animating: true
        });
        let body = {};
        console.log("Body Print:");
        console.log(body);
          console.warn("API:-",RECOVERY_API)
        APISingletonClass.getInstance().postMethodServiceHandler(
          RECOVERY_API + "?email=" + this.state.email,
          body,
          "",
          responce => {
            let responseJson = JSON.parse(responce);
            console.log("responseJson forgot", responseJson);
            //console.warn("_loginServiceHandler " + responseJson.status)
            this.setState({
              animating: false
            });
            if (responseJson.Status == true) {
              console.log("codeSent", responseJson.Result.code);
              this.props.navigation.navigate("ChangePass", {
                CodeSent: responseJson.Result.code,
                emailSent: this.state.email
              });
              // setTimeout(() => {
              //   Alert.alert(
              //     "BBF",
              //     responseJson.Message,
              //     [
              //       {
              //         text: "OK",
              //         onPress: () =>
              //           this.props.navigation.navigate("ChangePass", {
              //             CodeSent: responseJson.Result.code,
              //             emailSent: this.state.email
              //           })
              //       }
              //     ],
              //     { cancelable: false }
              //   );
              // }, 200);
            } else {
              setTimeout(() => {
                Alert.alert(
                  "BBF",
                  responseJson.ResponseException.ExceptionMessage,
                  [
                    {
                      text: "OK",
                      onPress: () => this.props.navigation.goBack()
                    }
                  ],
                  { cancelable: false }
                );
              }, 200);
            }
          },
          error => {
            this.setState({
              loading: false
            });
                          this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);

            console.warn("_loginServiceHandler error " + error);
          }
        );
      }
    }
  }
  render() {
    return (
      <View style={{ backgroundColor: "gray" }}>
        <StatusBar hidden={true} />
        <Spinner
          visible={this.state.animating}
          //textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <ImageBackground
            source={require("./Assets/main_background.png")}
            size={28}
            // borderRadius={15}
            style={{
              height: Dimensions.get("window").height,
              width: Dimensions.get("window").width,

              resizeMode: "contain"
            }}
          >
            <View style={{ marginTop: 10 }}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between"
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    width: Dimensions.get("window").width,
                    left: 15,
                    backgroundColor: "transparent"
                  }}
                >
                  <TouchableOpacity
                    onPress={() => this.props.navigation.goBack()}
                  >
                    <Image
                      source={require("./Assets/back.png")}
                      size={28}
                      borderRadius={15}
                      //onPress={this.props.navigation.goBack()}
                      style={{
                        height: 60,
                        width: 30,
                        alignSelf: "center",
                        resizeMode: "contain",
                        marginTop: 24,
                        backgroundColor: "transparent",
                        justifyContent: "flex-start",
                        backgroundColor: "transparent"
                      }}
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      fontSize: moderateScale(21),
                      color: "#FEFEFE",
                      textAlign: "center",
                      fontWeight: "200",
                      marginTop: 40,
                      marginLeft: 0,
                      marginRight: 20,
                      backgroundColor: "transparent",
                      fontFamily: "SegoeUI-Light"
                    }}
                  >
                    F O R G O T {"  "}P A S S W O R D
                  </Text>
                </View>
              </View>
            </View>

            <Image
              source={require("./Assets/bbf_logo.png")}
              size={28}
              borderRadius={15}
              style={{
                height: verticalScale(150),
                width: scale(120),
                alignSelf: "center",
                justifyContent: "center",
                resizeMode: "contain",
                marginTop: 10
              }}
            />

            <View
              style={{
                flexDirection: "row",

                alignContent: "flex-start",
                marginLeft: 30,
                marginRight: 30,
                marginTop: 40,
                backgroundColor: "transparent",
                borderColor: "#7098AA",
                borderWidth: 1,
                borderRadius: 10,
                height: 40
              }}
            >
              <TextInput
                placeholder="Email"
                placeholderTextColor="#C0C0C0"
                onChangeText={email => {
                  this.setState({ email });
                }}
                value={this.state.email}
                returnKeyType={"next"}
                style={{
                  marginLeft: 10,
                  marginRight: 10,
                  textAlign: "left",
                  position: "absolute",
                  marginTop: moderateScale(5),
                  color: "white",
                  fontSize: 20,
                  width: Dimensions.get("window").width - scale(80),
                  fontFamily: "SegoeUI-Light"
                }}
              />
            </View>
            <View
              style={{
                height: 60,
                fontSize: 20,
                marginTop: 50,
                textAlign: "center",
                backgroundColor: "#484848",
                height: 40,
                borderRadius: 18,
                borderWidth: 0.5,
                borderColor: "#7098AA",
                width: 150,
                alignItems: "center",
                alignContent: "center",
                alignSelf: "center"
              }}
            >
              <Text
                style={{
                  height: 60,
                  fontSize: 20,
                  marginTop: moderateScale(4),
                  width: 150,
                  textAlign: "center",
                  backgroundColor: "transparent",
                  height: 50,
                  color: "white",
                  fontFamily: "SegoeUI-Light",
                  alignItems: "center"
                }}
                onPress={() => this._loginBtnAction()}
                title="Login"
                color="black"
                accessibilityLabel="Tap on Me"
              >
                Submit
              </Text>
            </View>
          </ImageBackground>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ecf0f1"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 10
  },
  sharingtext: {
    color: "#AA2E2E",
    backgroundColor: "#fff",
    borderRadius: 14,
    borderColor: "#AA2E2E",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 18,
    width: 130,
    height: 30,
    alignItems: "center",
    alignContent: "center",
    textAlignVertical: "center"
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover" // or 'stretch'
  },
  footerWrapperlikeCommentShare: {
    flexWrap: "nowrap",
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginLeft: 30,
    marginRight: 30
  },
  separatorlikeShare: {
    flex: 0,
    borderWidth: 1,
    borderColor: "#ECEDED"
  },
  imageSize2: {
    width: 45,
    height: 45,
    padding: 5,
    borderRadius: 10
  }
});

export default withNavigation(Forgot);
