import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  ScrollView,
  NavigatorIOS,
  Alert,
  Image,
  ToastAndroid,
  ActivityIndicator,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  StatusBar,
  Switch,
  Slider
} from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { withNavigation } from "react-navigation";
//import { Switch } from "react-native-switch";
import ToggleSwitch from "toggle-switch-react-native";
import { ScaledSheet } from "react-native-size-matters";
import { scale, verticalScale, moderateScale } from "react-native-size-matters";
import APISingletonClass, { EULA_API, EULAACCEPT_API } from "./API";
import AsyncStorage from "@react-native-community/async-storage";
import Spinner from "react-native-loading-spinner-overlay";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      loading: false,
      animating: false,
      loginInProcess: false,
      switchedBool: false,
      eulaDescription: "",
      eulaId: "",
      token: "",
      sliderValue: 0
    };
  }
  getData = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
       // console.log("Got Token", value);
        this.setState({
          token: value
        });
        this.GetEula();
        //this.GetProfile();
      }
    } catch (e) {
      // error reading value
    }
  };
  componentDidMount() {
    this.getData("token");
    this.storeData("isFromSocial", "false");

  }
  storeData = async (Key, Value) => {
    try {
      await AsyncStorage.setItem(Key, Value);
    } catch (e) {
      // saving error
    }
  };

  componentWillMount() {
    //BackHandler.addEventListener("hardwareBackPress", this.backPressed);
    this._subscribe = this.props.navigation.addListener("didFocus", () => {
      this.getData("token");
      this.setState({sliderValue:0})
      // this.getToken("token");
      // this.getData("profilePhoto");
      // this.getData("FullName");
    });
  }
  crossCheck(text) {
    if (text == 100) {
      //this.setState({sliderValue:0})
      this.slider.setNativeProps({sliderValue: 0});
      this.DoneEula();
    }
  }
  // componentWillUnmount() {
  //   //BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
  // }
  GetEula() {
    //this.props.navigation.push('Friends')

    this.setState({
      loginInProcess: true,
      animating: true
    });
    let body = {};
    console.log("Body Print:");
    console.log(body);
    APISingletonClass.getInstance().getMethodServiceHandler(
      EULA_API,
      body,
      "Bearer " + this.state.token,
      responce => {
        console.log("responseJson Eula", responce);
        let responseJson = JSON.parse(responce);
        this.setState({
          animating: false
        });
        console.log("responseJson Eula", responseJson);
        //console.warn("_loginServiceHandler " + responseJson.status)
        if (responseJson.Status == true) {
         this.setState({sliderValue:0})
          this.setState({
            eulaDescription: responseJson.Result.aggrement,
            eulaId: responseJson.Result.id,
            switchedBool: responseJson.Result.accepted
          });
          // if (responseJson.Result.accepted) {
          //   this.props.navigation.navigate("homePageBbf", {
          //     DataHomeComment: ""
          //   });
          // }

        } else {
        }
      },
      error => {
        this.setState({
          loading: false
        });
                      this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);

        console.warn("_loginServiceHandler error " + error);
      }
    );
  }
  DoneEula() {
    //this.props.navigation.push('Friends')

    this.setState({
      loginInProcess: true,
      animating: true
    });
    let body = {};
    console.log("Body Print:");
    console.log(body);
    console.log("EulaIdd", this.state.eulaId);
    APISingletonClass.getInstance().postMethodServiceHandler(
      EULAACCEPT_API + "?eulaId=" + this.state.eulaId,
      body,
      "Bearer " + this.state.token,

      responce => {
        console.log("responseJson Eula", responce);
        let responseJson = JSON.parse(responce);
        console.log("responseJson Eula", responseJson);
        this.setState({
          animating: false
        });
        //console.warn("_loginServiceHandler " + responseJson.status)
        if (responseJson.Status == true) {
          this.slider.setNativeProps({sliderValue: 0});
          this.setState({sliderValue:0})
          this.props.navigation.navigate("homePageBbf", {
            DataHomeComment: ""
          });
          // setTimeout(() => {
          //   Alert.alert(
          //     "BBF",
          //     responseJson.Result.message,
          //     [
          //       {
          //         text: "OK",
          //         onPress: () =>
          //           this.props.navigation.navigate("homePageBbf", {
          //             DataHomeComment: ""
          //           })
          //       }
          //     ],
          //     { cancelable: false }
          //   );
          // }, 200);
        } else {
        }
      },
      error => {
        this.setState({
          loading: false
        });
                      this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);

        console.warn("_loginServiceHandler error " + error);
      }
    );
  }
  loginOptions() {
    this.props.navigation.navigate("ProfileNav", {
      DataHomeComment: ""
    });
  }
  changeBool() {
    this.setState({ switchedBool: !this.state.switchedBool });
    if (this.state.switchedBool == false) {
      this.DoneEula();
    }
  }

  render() {
    // this.state.switchedBool = this.props.navigation.state.params.eulaBool;
    return (
      <View style={{ backgroundColor: "gray" }}>
        <StatusBar hidden={true} />
        <Spinner
          visible={this.state.animating}
          //textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <ImageBackground
          source={require("./Assets/main_background.png")}
          size={28}
          // borderRadius={15}
          style={{
            height: Dimensions.get("window").height,
            width: Dimensions.get("window").width,
            resizeMode: "contain"
          }}
        >
          <Image
            source={require("./Assets/bbf_logo.png")}
            size={28}
            borderRadius={15}
            style={{
              height: verticalScale(300),
              width: scale(170),

              alignSelf: "center",
              justifyContent: "center",
              resizeMode: "contain",
              marginTop: moderateScale(40)
            }}
          />
          <Text
            style={{
              fontSize: moderateScale(20),
              color: "#FEFEFE",
              textAlign: "center",
              fontWeight: "200",
              marginTop: moderateScale(50),
              marginLeft: 20,
              marginRight: 20,
              fontFamily: "SegoeUI-Light"
            }}
          >
            {this.state.eulaDescription}
          </Text>

          {/* <TouchableOpacity onPress={() => this.changeBool()}> */}
          {/* <TouchableOpacity> */}
          <View
            style={{
              marginLeft: 30,
              marginRight: 30,
              borderRadius: 24,
              borderColor: "#7DBDD8",
              borderWidth: 2,
              marginTop: moderateScale(52),
              padding: 0,
              flexDirection: "row",
              height: 48
              // backgroundColor: "black"
            }}
          >
            {/* <ToggleSwitch
                isOn={this.state.switchedBool}
                onColor="green"
                offColor="transparent"
                //label="Example label"
                labelStyle={{ color: "transparent", fontWeight: "900" }}
                // style={{width:}}
                size="large"
                onToggle={isOn => console.log("changed to : ", isOn)}
              /> */}
            <View
              style={{
                // position: "relative",
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                width: Dimensions.get("window").width - 60,
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "transparent"
              }}
            >
              <Text
                style={{
                  fontSize: moderateScale(22),
                  color: "#FEFEFE",
                  textAlign: "center",
                  fontWeight: "200",
                  marginTop: 0,
                  marginLeft: 0,
                  fontFamily: "SegoeUI-Light"
                }}
              >
                SLIDE TO ACCEPT
              </Text>
            </View>
            <Slider
              maximumValue={100}
              minimumValue={0}
              thumbImage={require("./Assets/slider.png")}
              minimumTrackTintColor="transparent"
              maximumTrackTintColor="transparent"
              backgroundColor="transparent"
              ref={r => this.slider = r}
              style={{
                borderRadius: 20,
                height: 50,
                marginLeft: -5,
                marginRight: -5,
                marginTop: -3,
                justifyContent: "center",
                alignItems: "center",
                position: "absolute",
                width: Dimensions.get("window").width - 55
              }}
              //step={1}
              value={this.state.sliderValue}
              onValueChange={sliderValue => {
                //   this.setState({ phonenumber }),
                this.crossCheck(sliderValue);
              }}
              // onSlidingComplete={sliderValue =>{
              //   this.setState({ sliderValue:0 });
              // }}
              // onChangeText={phonenumber => {
              //   this.setState({ phonenumber }),
              //     this.mobilevalidate(phonenumber);
              // }}
            />
          </View>
          {/* </TouchableOpacity> */}
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ecf0f1"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 10
  },
  sharingtext: {
    color: "#AA2E2E",
    backgroundColor: "#fff",
    borderRadius: 14,
    borderColor: "#AA2E2E",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 18,
    width: 130,
    height: 30,
    alignItems: "center",
    alignContent: "center",
    textAlignVertical: "center"
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover" // or 'stretch'
  },
  footerWrapperlikeCommentShare: {
    flexWrap: "nowrap",
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginLeft: 30,
    marginRight: 30
  },
  separatorlikeShare: {
    flex: 0,
    borderWidth: 1,
    borderColor: "#ECEDED"
  },
  imageSize2: {
    width: 60,
    height: 60,
    padding: 5,
    borderRadius: 10
  }
});

export default withNavigation(Login);
