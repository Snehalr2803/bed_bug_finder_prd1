import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  ScrollView,
  NavigatorIOS,
  Alert,
  Image,
 
  ToastAndroid,
  ActivityIndicator,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  StatusBar,
  Switch,
  FlatList,
  Keyboard,
  TouchableWithoutFeedback
} from "react-native";
import { CheckBox } from "react-native-elements";
import {
  NativeModules,
  requireNativeComponent,
  findNodeHandle
} from "react-native";

import { createStackNavigator, createAppContainer } from "react-navigation";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import SearchBar from "react-native-search-bar";
import { withNavigation } from "react-navigation";
//import { Switch } from "react-native-switch";
import ToggleSwitch from "toggle-switch-react-native";
import APISingletonClass, { BASEURL, ADD_WATCHLIST } from "./API";
import { ScaledSheet } from "react-native-size-matters";
import { scale, verticalScale, moderateScale } from "react-native-size-matters";
import AsyncStorage from "@react-native-community/async-storage";

import Spinner from "react-native-loading-spinner-overlay";
const EmptyComponent = ({ title }) => (
  <View style={styles.emptyContainer}>
    <Text style={styles.emptyText}>{title}</Text>
  </View>
);
class LocationClass extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      token: "",
      loading: false,
      animating: false,
      loginInProcess: false,
      switchedBool: false,
      visible: true,
      searchtext: "",
      ProfilePic: "",
      locationList: [],
      focus: true,
      checking: [],
      addedWatchList: [],
      lastLat: null,
      lastLong: null
    };
  }

  loginOptions() {
    this.props.navigation.navigate("ProfileNav", {
      DataHomeComment: ""
    });
  }
  getData = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Pic", value);
        this.setState({
          ProfilePic: value
        });

        //this.GetProfile();
      }
    } catch (e) {
      // error reading value
    }
  };
  getLatLongRegion = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        if (key == "LattitudeSave") {
          this.setState({ lastLat: value });
        } else if (key == "LongitudedeSave") {
          this.setState({ lastLong: value });
          let region = {
            latitude: this.state.lastLat,
            longitude: this.state.lastLong,
            latitudeDelta: 0.00922 * 1.5,
            longitudeDelta: 0.00421 * 1.5
          };
          this.setState({ mapRegion: region });
        } else {
          this.setState({ mapRegion: value });
        }
        // value previously stored
        console.log("Got Pic", value);
        if (this.state.lastLat != null && this.state.lastLong != null) {
          // this.GetNearLocation();
          this.GetEnterPriseLocation();
        }
        // this.setState({
        //   ProfilePic: value
        // });

        //this.GetProfile();
      }
    } catch (e) {
      // error reading value
    }
  };
  handleChange = index => {
    console.log("Selected index : " + index);
    let { checking } = this.state;
    checking[index] = !checking[index];
    this.setState({ checking });
    console.log("Checking State in  Handle:" + this.state.checking);
  };
  componentDidMount() {
    this.getData("profilePhoto");
    this.getToken("token");
    this.getLatLongRegion("LattitudeSave");
    this.getLatLongRegion("LongitudedeSave");
  }
  storeData = async (Key, Value) => {
    try {
      await AsyncStorage.setItem(Key, Value);
    } catch (e) {
      // saving error
    }
  };
  addIncident() {
    //this.props.navigation.push('Friends')
    // if (!this.state.loginInProcess) {
    this.setState({
      loginInProcess: true,
      animating: true
    });

    const form = [];
    for (i = 0; i < this.state.locationList.length; i++) {
      console.log("Invite Friends: " + this.state.locationList[i].id);
      if (this.state.checking[i] == true) {
        form.push(this.state.locationList[i]);
        console.log("Invite Friends: Checked" + this.state.locationList[i].id);
      } else {
      }
    }
    if (form.length == 0) {
      Alert.alert(
        "BBF",
        "Selected LocationList can not be empty.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
      return;
    } else if (form.length > 1) {
      Alert.alert(
        "BBF",
        "Please Selected  only one location.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
      return;
    }
    this.storeData("Company", form[0].display_name + ",");
    // this.storeData("sLocation", form[0].location+",");
    // this.storeData("pinCode", form[0].zip);
    // this.storeData("Contry", form[0].country+",");
    // this.storeData("IdSent", JSON.stringify(form[0].id));
    // this.storeData("State", form[0].state+",");
    // //cityName
    // this.storeData("CitySave", form[0].city+",");
    this.setState({
      animating: false
    });

    setTimeout(() => {
      Alert.alert(
        "BBF",
        "Location Added Successfully.",
        [
          {
            text: "OK",
            onPress: () => this.props.navigation.goBack()
          }
        ],
        { cancelable: false }
      );
    }, 200);
    // const formD = new FormData();

    // // console.log("Form", form);
    // // form.append("Photo", photo);
    // formD.append("incidentId", form);
    // let body = {
    //   incidentId: form
    // };
    // console.log("Body Print:");
    // console.log(body);
    // //http://51.143.17.1:3310/api/User/Login?userName=s4%40gmail.com&password=Snehal123
    // console.log("Body Added:", body);
    // //var BASEURL = "http://192.168.0.12:1011";
    // var BASEURL = "http://51.143.17.1:3310";
    // APISingletonClass.getInstance().postMethodServiceHandler(
    //   BASEURL + "/api/Incident/AddToWatchList",
    //   form,
    //   "Bearer " + this.state.token,
    //   responce => {
    //     let responseJson = JSON.parse(responce);
    //     console.log("responseJson", responseJson);
    //     //console.warn("_loginServiceHandler " + responseJson.status)
    //     this.setState({
    //       animating: false
    //     });
    //     if (responseJson.Status == true) {
    //       //  this.storeData("token", responseJson.Result.token);
    //       setTimeout(() => {
    //         Alert.alert(
    //           "BBF",
    //           responseJson.Result.message,
    //           [
    //             {
    //               text: "OK",
    //               onPress: () => console.log("Ok")
    //             }
    //           ],
    //           { cancelable: false }
    //         );
    //       }, 200);
    //     } else {
    //       setTimeout(() => {
    //         Alert.alert(
    //           "BBF",
    //           responseJson.ResponseException.ExceptionMessage,
    //           [
    //             {
    //               text: "OK",
    //               onPress: () => console.log("Dislike.")
    //             }
    //           ],
    //           { cancelable: false }
    //         );
    //       }, 200);
    //     }
    //   },
    //   error => {
    //     this.setState({
    //       loading: false
    //     });
    //     console.warn("_loginServiceHandler error " + error);
    //   }
    // );
  }
  getToken = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Pic Token", value);
        this.setState({
          token: value
        });
      }
    } catch (e) {}
  };
  GetEnterPriseLocation() {
    //this.props.navigation.push('Friends')
    this.setState({
      loginInProcess: true,
      animating: true
    });
    let body = {};
    // console.log("Body Print:");
    // console.log(body);
    // console.log("EulaIdd", this.state.eulaId);

    // console.warn(
    //   "Url Printing",
    //   "http://51.143.17.1:3310" +
    //     "/api/EnterpriseApi/GetNearbyHotels" +
    //     "?lat=" +
    //     this.state.lastLat +
    //     "&lan=" +
    //     this.state.lastLong
    // );
    // const BASEURL1 = "http://192.168.0.12:1011";
   // const BASEURL1 = "http://51.143.17.1:3310";
   var BASEURL1 =  "http://bbf.dimitrisnowden.com"
    APISingletonClass.getInstance().getMethodServiceHandler(
      "http://bbf.dimitrisnowden.com/api/EnterpriseApi/GetNearbyHotels?lat="+
        this.state.lastLat+
        "&lan=" +
        this.state.lastLong,
      body,
      "",
      responce => {
        console.log("responseJson Eula", responce);
        let responseJson = JSON.parse(responce);
        this.setState({
          animating: false
        });
        console.warn("responseJson Eula", responseJson);
        //console.warn("_loginServiceHandler " + responseJson.status)
        if (responseJson.Status == true) {
          //   var tempMarker = [];
          //   var tempIdMarker = [];
          //   for (var p in responseJson.Result) {
          //     tempMarker.push({
          //       value: responseJson.Result[p].location
          //     });
          //   }
          // console.log("tempMarker", tempMarker);
          this.setState({ locationList: responseJson.Result });

          //   for (var p in responseJson.Result) {
          //     tempIdMarker.push({
          //       value: responseJson.Result[p].id
          //     });
          //   }

          //   this.setState({
          //     idArray: tempIdMarker
          //   });
          // console.log("Suggestion Given", this.state.suggestions);
          // if (responseJson.Result.accepted) {
          //   this.props.navigation.navigate("homePageBbf", {
          //     DataHomeComment: ""
          //   });
          // }
        } else {
          this.setState({
            animating: false
          });
          // this.setState({ locationList: responseJson.Result });
        }
      },
      error => {
        this.setState({
          loading: false
        });
        console.warn("_loginServiceHandler error " + error);
      }
    );
  }
  searchButtonPressed() {
    this.setState({ visible: true });
    this.setState({ animating: true, focus: false });
    this.refs.searchBar.blur();
    console.log("Search text", this.state.searchtext);
    const body = {};
    // NativeModules.RNSearchBarManager.toggleCancelButton(
    //   findNodeHandle(this),
    //   false
    // );
    //const BASEURL1 = "http://192.168.0.12:1011";
//    var BASEURL1 =  "http://bbf-dev.dimitrisnowden.com"
var BASEURL1 =  "http://bbf.dimitrisnowden.com"

    //const BASEURL1 = "http://51.143.17.1:3310";
    // console.log(
    //   "Url Search",
    //   BASEURL1 +
    //     "/api/EnterpriseApi/GetNearbyHotels" +
    //     "?search=" +
    //     String(this.state.searchtext)
    // );
    APISingletonClass.getInstance().getMethodServiceHandler(
      BASEURL1 +
        "/api/EnterpriseApi/GetNearbyHotels?search=" +
        this.state.searchtext +
        "&lat=" +
        this.state.lastLat +
        "&lan=" +
        this.state.lastLong,
      body,
      "",
      responce => {
        console.log("responseJson Eula", responce);
        let responseJson = JSON.parse(responce);
        this.setState({
          animating: false
        });
        console.log("responseJson Eula", responseJson);
        //console.warn("_loginServiceHandler " + responseJson.status)
        if (responseJson.Status == true) {
          this.setState({ locationList: responseJson.Result });
          console.log("value set", this.state.locationList);
          //   var tempMarker = [];
          //   var tempIdMarker = [];
          //   for (var p in responseJson.Result) {
          //     tempMarker.push({
          //       value: responseJson.Result[p].location
          //     });
          //   }
          // console.log("tempMarker", tempMarker);
          // if (responseJson.Result == []) {
          //   this.setState({   locationList: [] });
          // } else {

          //  }

          //   for (var p in responseJson.Result) {
          //     tempIdMarker.push({
          //       value: responseJson.Result[p].id
          //     });
          //   }

          //   this.setState({
          //     idArray: tempIdMarker
          //   });
          // console.log("Suggestion Given", this.state.suggestions);
          // if (responseJson.Result.accepted) {
          //   this.props.navigation.navigate("homePageBbf", {
          //     DataHomeComment: ""
          //   });
          // }
        } else {
        }
      },
      error => {
        this.setState({
          loading: false
        });
                      this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);

        console.warn("_loginServiceHandler error " + error);
      }
    );
  }
  render_FlatList_footer = () => {
    var footer_View = (
      <View>
        {/* <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}> */}
        {/* <View
            style={{
              marginTop: moderateScale(20),
              marginLeft: 20,
              marginRight: 20,
              height: moderateScale(90),
              flexDirection: "row",
              justifyContent: "space-evenly"
            }}
          > */}
        {/* <View
              style={{
                backgroundColor: "transparent",
                width: moderateScale(50),
                height: moderateScale(50),
                borderRadius: moderateScale(25),
                borderWidth: moderateScale(3),
                borderColor: "#7098AA"
              }}
            >
              <Text
                style={{
                  fontSize: moderateScale(32),
                  // marginTop: moderateScale(4),
                  color: "#FEFEFE",
                  alignSelf: "center",
                  fontWeight: "300",
                  textAlign: "center",
                  fontFamily: "SegoeUI-Light"
                }}
              >
                {""}
                {this.state   locationList.length}
              </Text>
            </View> */}
        {/* <Text
              style={{
                fontSize: moderateScale(30),
                color: "#FEFEFE",
                textAlign: "left",
                fontWeight: "300",
                marginTop: moderateScale(12),
                marginLeft: 5,
                fontFamily: "SegoeUI-Light"
              }}
            >
              Incidents reported
            </Text> */}
        {/* </View> */}
        {/* </TouchableWithoutFeedback> */}
        <TouchableOpacity onPress={() => this.addIncident()}>
          <View
            style={{
              height: moderateScale(150),
              width: moderateScale(150),
              alignSelf: "center",
              resizeMode: "contain",
              marginLeft: 20,
              backgroundColor: "transparent",
              marginTop: moderateScale(20)
            }}
          >
            <Image
              source={require("./Assets/location.png")}
              size={28}
              borderRadius={15}
              style={{
                height: moderateScale(80),
                width: moderateScale(80),
                alignSelf: "center",
                resizeMode: "contain",
                marginTop: moderateScale(10),
                backgroundColor: "transparent"
              }}
            />
            <Text
              style={{
                fontSize: moderateScale(18),
                color: "#FEFEFE",
                alignSelf: "center",
                textAlign: "center",
                fontWeight: "300",
                marginTop: 0,
                fontFamily: "SegoeUI-Light"
              }}
            >
              ADD LOCATION
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );

    return footer_View;
  };
  optionPress(option) {
    if (option == "0") {
    } else if (option == "1") {
    } else if (option == "2") {
    } else if (option == "3") {
    }
  }
  render() {
    return (
      <View style={{ backgroundColor: "gray" }}>
        <StatusBar hidden={true} />
        <Spinner
          visible={this.state.animating}
          //textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <ImageBackground
            source={require("./Assets/main_background.png")}
            size={28}
            // borderRadius={15}
            style={{
              height: Dimensions.get("window").height,
              width: Dimensions.get("window").width,
              resizeMode: "contain"
            }}
          >
            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <View
                style={{
                  flexDirection: "row",
                  width: scale(230),
                  left: 15,
                  backgroundColor: "transparent"
                }}
              >
                <TouchableOpacity
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image
                    source={require("./Assets/back.png")}
                    size={28}
                    borderRadius={15}
                    //onPress={this.props.navigation.goBack()}
                    style={{
                      height: verticalScale(60),
                      width: scale(30),
                      alignSelf: "center",
                      resizeMode: "contain",
                      marginTop: moderateScale(34),
                      backgroundColor: "transparent",
                      justifyContent: "flex-start",
                      backgroundColor: "transparent"
                    }}
                  />
                </TouchableOpacity>
                <Text
                  style={{
                    fontSize: moderateScale(22),
                    color: "#FEFEFE",
                    textAlign: "center",
                    fontWeight: "200",
                    marginTop: moderateScale(46),
                    marginLeft: 0,
                    marginRight: 20,
                    backgroundColor: "transparent",
                    fontFamily: "SegoeUI-Light"
                  }}
                >
                  {" "}
                  L O C A T I O N S
                </Text>
              </View>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("ProfileNav1", {
                    DataHomeComment: ""
                  })
                }
              >
                <View
                  style={{
                    marginRight: 10,
                    marginTop: moderateScale(20),
                    alignSelf: "flex-end",
                    width: moderateScale(75),
                    height: moderateScale(75),
                    borderRadius: moderateScale(40),
                    borderWidth: moderateScale(3),
                    borderColor: "#525353",
                    backgroundColor: "transparent"
                  }}
                >
                  <Image
                    style={styles.avatar}
                    //source={require("./Assets/BrianImage.jpeg")}
                    source={{
                      uri: BASEURL + "/" + this.state.ProfilePic
                    }}
                    onPress={this.chooseFile}
                    resizeMode="cover"
                  />
                </View>
              </TouchableOpacity>
            </View>

            <SearchBar
              ref="searchBar"
              placeholder="Address,business name or city"
              barTintColor="#7098AA"
              hideBackground={true}
              tintColor="#7098AA"
              marginLeft={20}
              marginRight={20}
              marginTop={40}
              borderRadius={28}
              borderWidth={2}
              borderColor="#7098AA"
              backgroundColor="transparent"
              textFieldBackgroundColor="transparent"
              textColor="#FEFEFE"
              showsCancelButton={false}
              showsCancelButtonWhileEditing={false}
              enablesReturnKeyAutomatically={true}
              returnKeyType={"search"}
              onSearchButtonPress={this.searchButtonPressed.bind(this)}
              //   onChangeText={...}
              onChangeText={searchtext => {
                this.setState({ searchtext });
              }}
              //   onCancelButtonPress={...}
            />
            {/* {this.state.visible == true ? ( */}
            <FlatList
              margin={10}
              // numColumns="2"
              extraData={[]}
              // keyExtractor={hello => hello}
              showsVerticalScrollIndicator={true}
              ListEmptyComponent={
                <EmptyComponent title="No Location results Found." />
              }
              ListFooterComponent={this.render_FlatList_footer}
              data={this.state.locationList}
              renderItem={({ item, index }) => (
                <View style={styles.footerWrapperNumberCL}>
                  <View style={styles.footerWrapperCheck}>
                    <Text
                      style={{
                        fontSize: moderateScale(25),
                        color: "#7098AA",
                        textAlign: "left",
                        fontWeight: "400",
                        marginTop: moderateScale(20),
                        marginLeft: 20,
                        marginRight: 20,
                        fontFamily: "SegoeUI-Light",
                        width:Dimensions.get("window").width-100
                      }}
                    >
                      {item.display_name.split(",")[0]}
                      {/* {item.display_name} */}
                    </Text>
                    <CheckBox
                      // checkedIcon={<Image source={require("./assets/Checked.png")} />}
                      // uncheckedIcon={<Image source={require("./assets/unCheck.png")} />}
                      checkedIcon="check-square-o"
                      uncheckedIcon="square-o"
                      checkedColor="#7098AA"
                      //value={this.state.checked}
                      onPress={() => this.handleChange(index)}
                      checked={this.state.checking[index]}
                      // onPress=
                      containerStyle={{
                        marginTop: moderateScale(20),
                        marginRight: 0,
                        width: moderateScale(30),
                        height: moderateScale(30),
                        alignItems: "center",
                        backgroundColor: "transparent",
                        borderWidth: 0,
                        paddingTop: 0,
                        paddingRight: 0,
                        paddingBottom: 0
                      }}
                      //  ima={24}
                    />
                  </View>
                  <Text
                    style={{
                      fontSize: moderateScale(20),
                      color: "#FEFEFE",
                      textAlign: "left",
                      fontWeight: "200",
                      marginTop: moderateScale(5),
                      marginLeft: 20,
                      marginRight: 20,
                      fontFamily: "SegoeUI-Light"
                    }}
                  >
                    {item.display_name}
                  </Text>
                </View>
              )}
            />
            {/* ) : ( */}
            {/* <View /> */}
            {/* )} */}
          </ImageBackground>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ecf0f1"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 10
  },
  sharingtext: {
    color: "#AA2E2E",
    backgroundColor: "#fff",
    borderRadius: 14,
    borderColor: "#AA2E2E",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 18,
    width: 130,
    height: 30,
    alignItems: "center",
    alignContent: "center",
    textAlignVertical: "center"
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover" // or 'stretch'
  },
  footerWrapperlikeCommentShare: {
    flexWrap: "nowrap",
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginLeft: 30,
    marginRight: 30
  },
  separatorlikeShare: {
    flex: 0,
    borderWidth: 1,
    borderColor: "#ECEDED"
  },
  spinnerTextStyle: {
    color: "#565656"
  },
  imageSize2: {
    width: 60,
    height: 60,
    padding: 5,
    borderRadius: 10
  },
  avatar: {
    width: moderateScale(70),
    height: moderateScale(70),
    borderRadius: moderateScale(35),
    borderWidth: moderateScale(3),
    borderColor: "#7098AA",
    alignItems: "center"
  },
  header_footer_style: {
    width: "100%",
    height: 44,
    backgroundColor: "#4CAF50",
    alignItems: "center",
    justifyContent: "center"
  },
  textStyle: {
    textAlign: "center",
    color: "#fff",
    fontSize: 21
  },
  emptyContainer: {
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    alignContent: "center",
    marginTop: 30
  },
  emptyText: {
    fontSize: 20,

    color: "white",
    borderWidth: 1,
    borderColor: "#ECEDED",
    padding: 20
  },
  footerWrapperCheck: {
    //   flexWrap: "wrap",
    marginTop: 5,
    flexDirection: "row",
    justifyContent: "space-between"
  }
});

export default withNavigation(LocationClass);
