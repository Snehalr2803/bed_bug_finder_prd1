import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  ScrollView,
  NavigatorIOS,
  Alert,
  Image,
  ToastAndroid,
  ActivityIndicator,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  StatusBar,
  Switch
} from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import APISingletonClass, { PROFILE_API, EULAACCEPT_API, BASEURL } from "./API";
import { withNavigation } from "react-navigation";
//import { Switch } from "react-native-switch";
import ToggleSwitch from "toggle-switch-react-native";
import { ScaledSheet } from "react-native-size-matters";
import { scale, verticalScale, moderateScale } from "react-native-size-matters";
import AsyncStorage from "@react-native-community/async-storage";
import Geolocation from "@react-native-community/geolocation";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      loading: false,
      animating: false,
      loginInProcess: false,
      switchedBool: false,
      ProfilePic: "",
      fullName: "",
      token: "",
      watchListCount: 0,
      nearbyCount: 0
    };
    var watchID = null;
  }
  // onRegionChange(region, lastLat, lastLong) {
  //   console.log("Last Lattitude", lastLat);
  //   console.log("Last longitude", lastLong);
  //   this.setState({
  //     mapRegion: region,
  //     // If there are no new values set the current ones
  //     lastLat: lastLat || this.state.lastLat,
  //     lastLong: lastLong || this.state.lastLong
  //   });
  //   if (this.state.lastLat != null && this.state.lastLong != null) {
  //     this.GetCount();
  //   }
  // }
  GetCount() {
    //this.props.navigation.push('Friends')
    this.setState({
      loginInProcess: true,
      animating: true
    });
    let body = {};
    // console.log("Body Print:");
    // console.log(body);
    // console.log("EulaIdd", this.state.eulaId);

    // console.log(
    //   "Url Printing",
    //   "http://192.168.0.20:1011" +
    //     "/api​/Incident​/GetIncidentCount" +
    //     "?lat=" +
    //     String(this.state.lastLat) +
    //     "&lan=" +
    //     String(this.state.lastLong)
    // );
    //var BASEURL1 =  "http://192.168.0.12:1011"
    var BASEURL1 =  "http://bbf.dimitrisnowden.com"
    //const BASEURL1 = "http://51.143.17.1:3310";
    const url =
      BASEURL1 +
      "/api/Incident/GetIncidentCount?" +
      "lat=" +
      String(this.state.lastLat) +
      "&lan=" +
      String(this.state.lastLong);
    // console.log("Token Value Sent", this.state.token);
    // console.log("Print Url", url);
    APISingletonClass.getInstance().getMethodServiceHandler(
      url,
      //"http://51.143.17.1:3310/api/Incident/GetIncidentCount?lat=17.414384291744803&lan=78.42322344566789",
      body,
      "Bearer " + this.state.token,
      responce => {
        console.log("responseJson Eula", responce);
        let responseJson = JSON.parse(responce);
        this.setState({
          animating: false
        });
        // Alert.alert(
        //   "BBF",
        //   "Sevice Successfull Location Position",
        //   [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        //   { cancelable: false }
        // );
        console.log("responseJson Eula", responseJson);
        //console.warn("_loginServiceHandler " + responseJson.status)
        if (responseJson.Status == true) {
          this.setState({
            watchListCount: responseJson.Result.watchListCount,
            nearbyCount: responseJson.Result.nearbyIncidentCount
          });
          // if (responseJson.Result.accepted) {
          //   this.props.navigation.navigate("homePageBbf", {
          //     DataHomeComment: ""
          //   });
          // }
        } else {
        }
      },
      error => {
        this.setState({
          loading: false
        });
                      this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);

        console.warn("_loginServiceHandler error " + error);
      }
    );
  }
  loginOptions() {
    this.props.navigation.navigate("ProfileNav", {
      DataHomeComment: ""
    });
  }
  getData = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Pic", value);

        if (key == "FullName") {
          this.setState({
            fullName: value
          });
        } else {
          this.setState({
            ProfilePic: value
          });
        }

        //this.GetProfile();
      }
    } catch (e) {
      // error reading value
    }
  };
  storeData = async (Key, Value) => {
    try {
      await AsyncStorage.setItem(Key, Value);
    } catch (e) {
      // saving error
    }
  };

  componentWillMount() {
    //BackHandler.addEventListener("hardwareBackPress", this.backPressed);
    this._subscribe = this.props.navigation.addListener("didFocus", () => {
      this.getToken("token");
      this.getData("profilePhoto");
      this.getData("FullName");

      this.storeData("ScreenShow", "Home");
      //this.callLocation();
      // Alert.alert(
      //   "BBF",
      //   "Start geo willmount Location Position",
      //   [{ text: "OK", onPress: () => console.log("OK Pressed") }],
      //   { cancelable: false }
      // );
      navigator.geolocation.getCurrentPosition(
        position => {
          // Alert.alert(
          //   "BBF",
          //   "Inside geo willmount Location Position",
          //   [{ text: "OK", onPress: () => console.log("OK Pressed") }],
          //   { cancelable: false }
          // );
          // Create the object to update this.state.mapRegion through the onRegionChange function
          // let region = {
          //   latitude: position.coords.latitude,
          //   longitude: position.coords.longitude,
          //   latitudeDelta: 0.00922 * 1.5,
          //   longitudeDelta: 0.00421 * 1.5
          // };
          this.setState({
            //  mapRegion: region,
            // If there are no new values set the current ones
            lastLat: position.coords.latitude,
            lastLong: position.coords.longitude
          });
          this.storeData(
            "LattitudeSave",
            JSON.stringify(position.coords.latitude)
          );
          this.storeData(
            "LongitudedeSave",
            JSON.stringify(position.coords.longitude)
          );
          // this.storeData("RegionSave", region);

          // console.log("Region", region);
          // console.log("Region", region);
          //  this.onRegionChange(region, region.latitude, region.longitude);
          // Alert.alert(
          //   "BBF",
          //   "before count service called Willmount",
          //   [{ text: "OK", onPress: () => console.log("OK Pressed") }],
          //   { cancelable: false }
          // );
          if (this.state.lastLat != null && this.state.lastLong != null) {
            this.GetCount();
          }
          //this.onRegionChange(region, region.latitude, region.longitude);
          //     if (this.state.lastLat != null && this.state.lastLong != null) {
          //   this.GetNearByIncidents();
          // }
        },
        error => Alert.alert("Error", JSON.stringify(error)),
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
      );

      //   this.watchID = navigator.geolocation.watchPosition(position => {
      //     // Will give you the location on location change
      //     console.log(position);
      //     Alert.alert(
      //       "BBF",
      //       "Inside geo willmount Location Position",
      //       [{ text: "OK", onPress: () => console.log("OK Pressed") }],
      //       { cancelable: false }
      //     );
      //     const currentLongitude = JSON.stringify(position.coords.longitude);
      //     // getting the Longitude from the location json
      //     const currentLatitude = JSON.stringify(position.coords.latitude);
      //     // getting the Latitude from the location json
      //     // that.setState({ currentLongitude });
      //     // // Setting state Longitude to re re-render the Longitude Text
      //     // that.setState({ currentLatitude });
      //     // Setting state Latitude to re re-render the Longitude Text

      //     let region = {
      //       latitude: currentLatitude,
      //       longitude: currentLongitude,
      //       latitudeDelta: 0.00922 * 1.5,
      //       longitudeDelta: 0.00421 * 1.5
      //     };
      //     this.setState({
      //       mapRegion: region,
      //       // If there are no new values set the current ones
      //       lastLat: currentLatitude,
      //       lastLong: currentLongitude
      //     });
      //     console.log("Region", region);
      //     console.log("Region", region);
      //     //this.onRegionChange(region, region.latitude, region.longitude);
      //     if (this.state.lastLat != null && this.state.lastLong != null) {
      //       this.GetCount();
      //     }
      //     // });
      //   });
      //   Alert.alert(
      //     "BBF",
      //     "After geo  Location Position",
      //     [{ text: "OK", onPress: () => console.log("OK Pressed") }],
      //     { cancelable: false }
      //   );
    });
  }

  componentWillUnmount() {
    //BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
    navigator.geolocation.clearWatch(this.watchID);
  }

  // callLocation() {
  //   // alert("callLocation Called");
  //   navigator.geolocation.getCurrentPosition(
  //     // Will give you the current location
  //     position => {
  //       const currentLongitude = JSON.stringify(position.coords.longitude);
  //       // getting the Longitude from the location json
  //       const currentLatitude = JSON.stringify(position.coords.latitude);

  //       let region = {
  //         latitude: currentLatitude,
  //         longitude: currentLongitude,
  //         latitudeDelta: 0.00922 * 1.5,
  //         longitudeDelta: 0.00421 * 1.5
  //       };
  //       this.setState({
  //         mapRegion: region,
  //         // If there are no new values set the current ones
  //         lastLat: currentLatitude,
  //         lastLong: currentLongitude
  //       });
  //       this.storeData("LattitudeSave", currentLatitude);
  //       this.storeData("LongitudedeSave", currentLongitude);
  //       //  this.storeData("RegionSave", region);
  //       console.log("Region", region);
  //       console.log("Region", region);
  //       //this.onRegionChange(region, region.latitude, region.longitude);
  //       if (this.state.lastLat != null && this.state.lastLong != null) {
  //         this.GetCount();
  //       }
  //       // getting the Latitude from the location json
  //       // that.setState({ currentLongitude });
  //       // // Setting state Longitude to re re-render the Longitude Text
  //       // that.setState({ currentLatitude });
  //       // Setting state Latitude to re re-render the Longitude Text
  //     },
  //     error => alert(error.message)
  //     // { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
  //   );
  //   this.watchID = navigator.geolocation.watchPosition(position => {
  //     // Will give you the location on location change
  //     console.log(position);
  //     const currentLongitude = JSON.stringify(position.coords.longitude);
  //     // getting the Longitude from the location json
  //     const currentLatitude = JSON.stringify(position.coords.latitude);
  //     // getting the Latitude from the location json
  //     // that.setState({ currentLongitude });
  //     // // Setting state Longitude to re re-render the Longitude Text
  //     // that.setState({ currentLatitude });
  //     // Setting state Latitude to re re-render the Longitude Text

  //     let region = {
  //       latitude: currentLatitude,
  //       longitude: currentLongitude,
  //       latitudeDelta: 0.00922 * 1.5,
  //       longitudeDelta: 0.00421 * 1.5
  //     };
  //     this.setState({
  //       mapRegion: region,
  //       // If there are no new values set the current ones
  //       lastLat: currentLatitude,
  //       lastLong: currentLongitude
  //     });
  //     console.log("Region", region);
  //     console.log("Region", region);
  //     //this.onRegionChange(region, region.latitude, region.longitude);
  //     if (this.state.lastLat != null && this.state.lastLong != null) {
  //       this.GetCount();
  //     }
  //   });
  // }

  componentDidMount() {
    this.getToken("token");
    this.getData("profilePhoto");
    this.getData("FullName");
    this.storeData("isFromSocial", "false");

    //this.callLocation(this);
    // Alert.alert(
    //   "BBF",
    //   "Start geo Location Position",
    //   [{ text: "OK", onPress: () => console.log("OK Pressed") }],
    //   { cancelable: false }
    // );
    navigator.geolocation.getCurrentPosition(
      position => {
        // Alert.alert(
        //   "BBF",
        //   "Inside geo Location Position",
        //   [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        //   { cancelable: false }
        // );
        // Create the object to update this.state.mapRegion through the onRegionChange function
        // let region = {
        //   latitude: position.coords.latitude,
        //   longitude: position.coords.longitude,
        //   latitudeDelta: 0.00922 * 1.5,
        //   longitudeDelta: 0.00421 * 1.5
        // };
        this.setState({
          //  mapRegion: region,
          // If there are no new values set the current ones
          lastLat: position.coords.latitude,
          lastLong: position.coords.longitude
        });
        // console.log("Region", region);
        // console.log("Region", region);
        this.storeData(
          "LattitudeSave",
          JSON.stringify(position.coords.latitude)
        );
        this.storeData(
          "LongitudedeSave",
          JSON.stringify(position.coords.longitude)
        );
        // Alert.alert(
        //   "BBF",
        //   "Saved geo Location Position",
        //   [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        //   { cancelable: false }
        // );
        //  this.onRegionChange(region, region.latitude, region.longitude);
        if (this.state.lastLat != null && this.state.lastLong != null) {
          // Alert.alert(
          //   "BBF",
          //   "Service Count call Location Position",
          //   [{ text: "OK", onPress: () => console.log("OK Pressed") }],
          //   { cancelable: false }
          // );
          this.GetCount();
        }
        //this.onRegionChange(region, region.latitude, region.longitude);
        //     if (this.state.lastLat != null && this.state.lastLong != null) {
        //   this.GetNearByIncidents();
        // }
      },
      error => Alert.alert("Error", JSON.stringify(error)),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
    // this.watchID = navigator.geolocation.watchPosition(
    //   position => {
    //     Alert.alert(
    //       "BBF",
    //       "Inside geo Location Position",
    //       [{ text: "OK", onPress: () => console.log("OK Pressed") }],
    //       { cancelable: false }
    //     );
    //     // Create the object to update this.state.mapRegion through the onRegionChange function
    //     let region = {
    //       latitude: position.coords.latitude,
    //       longitude: position.coords.longitude,
    //       latitudeDelta: 0.00922 * 1.5,
    //       longitudeDelta: 0.00421 * 1.5
    //     };

    //     this.storeData("LattitudeSave", position.coords.latitude);
    //     this.storeData("LongitudedeSave", position.coords.longitude);
    //     // this.storeData("RegionSave", region);
    //     // Alert.alert(
    //     //   "BBF",
    //     //   "Saved geo Location Position",
    //     //   [{ text: "OK", onPress: () => console.log("OK Pressed") }],
    //     //   { cancelable: false }
    //     // );
    //     // console.log("Region", region);
    //     // console.log("Region", region);
    //     //  this.onRegionChange(region, region.latitude, region.longitude);
    //     if (this.state.lastLat != null && this.state.lastLong != null) {
    //       // Alert.alert(
    //       //   "BBF",
    //       //   "Service Count call Location Position",
    //       //   [{ text: "OK", onPress: () => console.log("OK Pressed") }],
    //       //   { cancelable: false }
    //       // );
    //       this.GetCount();
    //     }
    //     //  this.onRegionChange(region, region.latitude, region.longitude);
    //   },
    //   error =>
    //     Alert.alert(
    //       "BBF",
    //       error,
    //       [{ text: "OK", onPress: () => console.log("OK Pressed") }],
    //       { cancelable: false }
    //     ),
    //   { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    // );
    // Alert.alert(
    //   "BBF",
    //   " geo Location Position Not exucuted.",
    //   [{ text: "OK", onPress: () => console.log("OK Pressed") }],
    //   { cancelable: false }
    // );
  }
  getToken = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Pic Token", value);
        this.setState({
          token: value
        });

        // } else {
        //   this.setState({
        //     profileImage: value
        //   });
        // }
        // this.GetCount();
      }
    } catch (e) {
      // error reading value
    }
  };
  optionPress(option) {
    if (option == "0") {
      this.props.navigation.navigate("watchingVC1", {
        DataHomeComment: ""
      });
    } else if (option == "1") {
    } else if (option == "2") {
    } else if (option == "3") {
    }
  }
  render() {
    return (
      <View style={{ backgroundColor: "gray" }}>
        <StatusBar hidden={true} />
        <ImageBackground
          source={require("./Assets/main_background.png")}
          size={28}
          // borderRadius={15}
          style={{
            height: Dimensions.get("window").height,
            width: Dimensions.get("window").width,
            resizeMode: "contain"
          }}
        >
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate("ProfileNav1", {
                DataHomeComment: ""
              })
            }
          >
            <View
              style={{
                marginRight: 10,
                marginTop: moderateScale(20),
                alignSelf: "flex-end",
                width: moderateScale(75),
                height: moderateScale(75),
                borderRadius: moderateScale(40),
                borderWidth: moderateScale(3),
                borderColor: "#525353"
              }}
            >
              <Image
                style={styles.avatar}
                source={{
                  uri: BASEURL + "/" + this.state.ProfilePic
                }}
                onPress={this.chooseFile}
                resizeMode="cover"
              />
            </View>
          </TouchableOpacity>
          <Text
            style={{
              fontSize: moderateScale(40),
              color: "#FEFEFE",
              textAlign: "center",
              fontWeight: "200",
              marginTop: moderateScale(20),
              marginLeft: 20,
              marginRight: 20,
              fontFamily: "SegoeUI-Light"
            }}
          >
            {" "}
            W E L C O M E
          </Text>
          <Text
            style={{
              fontSize: moderateScale(28),
              color: "#7098AA",
              textAlign: "center",
              fontWeight: "300",
              marginTop: 5,
              marginLeft: 20,
              marginRight: 20,
              fontFamily: "SegoeUI-Light"
            }}
          >
            {" "}
            {this.state.fullName}
          </Text>

          <View
            style={{
              flexDirection: "row",
              width: Dimensions.get("window").width,
              height: 160,
              backgroundColor: "transperant",
              marginTop: 40,
              justifyContent: "space-between"
            }}
          >
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("watchingVC", {
                  DataHomeComment: ""
                })
              }
            >
              <View
                style={{
                  height: moderateScale(150),
                  width: moderateScale(150),
                  alignSelf: "flex-start",
                  resizeMode: "contain",
                  marginLeft: 20,
                  backgroundColor: "transparent"
                }}
              >
                <View
                  style={{
                    position: "absolute",
                    alignItems: "center",
                    right: 20,
                    top: 0,
                    bottom: 0,
                    backgroundColor: "#FF7200",
                    width: moderateScale(30),
                    height: moderateScale(30),
                    borderRadius: moderateScale(20)
                  }}
                >
                  <Text
                    style={{
                      fontSize: 17,
                      color: "#FEFEFE",

                      fontWeight: "500",
                      textAlign: "center",
                      marginTop: moderateScale(5)
                    }}
                  >
                    {""}
                    {this.state.watchListCount}
                  </Text>
                </View>
                <Image
                  source={require("./Assets/watch_list.png")}
                  size={28}
                  borderRadius={15}
                  style={{
                    height: moderateScale(80),
                    width: moderateScale(80),
                    alignSelf: "center",
                    resizeMode: "contain",
                    marginTop: moderateScale(10),
                    backgroundColor: "transparent"
                  }}
                />
                <Text
                  style={{
                    fontSize: moderateScale(18),
                    color: "#FEFEFE",
                    alignSelf: "center",
                    fontWeight: "300",
                    marginTop: moderateScale(5),
                    textAlign: "center",
                    fontFamily: "SegoeUI-Light"
                  }}
                >
                  {" "}
                  WATCH LIST
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("reportVC", {
                  DataHomeComment: ""
                })
              }
            >
              <View
                style={{
                  height: moderateScale(150),
                  width: moderateScale(150),
                  alignSelf: "flex-end",
                  resizeMode: "contain",

                  marginRight: 20,
                  backgroundColor: "transparent"
                }}
              >
                <Image
                  source={require("./Assets/report_incident.png")}
                  size={28}
                  borderRadius={15}
                  style={{
                    height: moderateScale(80),
                    width: moderateScale(80),
                    alignSelf: "center",
                    resizeMode: "contain",
                    marginTop: moderateScale(10),
                    backgroundColor: "transparent"
                  }}
                />
                <Text
                  style={{
                    fontSize: moderateScale(18),
                    color: "#FEFEFE",
                    alignSelf: "center",
                    fontWeight: "300",
                    marginTop: moderateScale(5),
                    textAlign: "center",
                    fontFamily: "SegoeUI-Light"
                  }}
                >
                  {" "}
                  REPORT BED BUG INCIDENT
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <View
            style={{
              flexDirection: "row",
              width: Dimensions.get("window").width,
              height: moderateScale(160),
              backgroundColor: "transparent",
              marginTop: moderateScale(30),
              justifyContent: "space-between"
            }}
          >
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("searchVC", {
                  DataHomeComment: ""
                })
              }
            >
              <View
                style={{
                  height: moderateScale(150),
                  width: moderateScale(150),
                  alignSelf: "flex-start",
                  resizeMode: "contain",
                  marginLeft: 20,
                  backgroundColor: "transparent"
                }}
              >
                <Image
                  source={require("./Assets/search_for_incident.png")}
                  size={28}
                  borderRadius={15}
                  style={{
                    height: moderateScale(80),
                    width: moderateScale(80),
                    alignSelf: "center",
                    resizeMode: "contain",
                    marginTop: moderateScale(10),
                    backgroundColor: "transparent"
                  }}
                />
                <Text
                  style={{
                    fontSize: moderateScale(18),
                    color: "#FEFEFE",
                    alignSelf: "center",
                    textAlign: "center",
                    fontWeight: "300",
                    marginTop: moderateScale(5),
                    fontFamily: "SegoeUI-Light"
                  }}
                >
                  {" "}
                  SEARCH FOR INCIDENTS
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("nearByVC", {
                  DataHomeComment: ""
                })
              }
            >
              <View
                style={{
                  height: moderateScale(150),
                  width: moderateScale(150),
                  alignSelf: "flex-end",
                  resizeMode: "contain",
                  marginRight: 20,
                  backgroundColor: "transparent"
                }}
              >
                <Image
                  source={require("./Assets/nearby_incidents.png")}
                  size={28}
                  borderRadius={15}
                  style={{
                    height: moderateScale(80),
                    width: moderateScale(80),
                    alignSelf: "center",
                    resizeMode: "contain",
                    marginTop: moderateScale(10),
                    backgroundColor: "transparent"
                  }}
                />
                <Text
                  style={{
                    fontSize: moderateScale(18),
                    color: "#FEFEFE",
                    alignSelf: "center",
                    fontWeight: "300",
                    marginTop: moderateScale(5),
                    textAlign: "center",
                    fontFamily: "SegoeUI-Light"
                  }}
                >
                  {" "}
                  NEAR BY INCIDENTS
                </Text>
                <View
                  style={{
                    position: "absolute",
                    alignItems: "center",

                    right: 20,
                    top: -10,
                    bottom: 0,
                    backgroundColor: "#FF7200",
                    width: moderateScale(30),
                    height: moderateScale(30),
                    borderRadius: moderateScale(20)
                  }}
                >
                  <Text
                    style={{
                      fontSize: moderateScale(17),
                      color: "white",
                      alignSelf: "center",
                      fontWeight: "500",
                      textAlign: "center",
                      marginTop: moderateScale(5)
                    }}
                  >
                    {""}
                    {this.state.nearbyCount}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ecf0f1"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 10
  },
  sharingtext: {
    color: "#AA2E2E",
    backgroundColor: "#fff",
    borderRadius: 14,
    borderColor: "#AA2E2E",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 18,
    width: 130,
    height: 30,
    alignItems: "center",
    alignContent: "center",
    textAlignVertical: "center"
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover" // or 'stretch'
  },
  footerWrapperlikeCommentShare: {
    flexWrap: "nowrap",
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginLeft: 30,
    marginRight: 30
  },
  separatorlikeShare: {
    flex: 0,
    borderWidth: 1,
    borderColor: "#ECEDED"
  },
  imageSize2: {
    width: 60,
    height: 60,
    padding: 5,
    borderRadius: 10
  },
  avatar: {
    width: moderateScale(70),
    height: moderateScale(70),
    borderRadius: moderateScale(35),
    borderWidth: moderateScale(3),
    borderColor: "#7098AA",
    alignItems: "center"
  }
});

export default withNavigation(Login);
