import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  ScrollView,
  NavigatorIOS,
  Alert,
  Image,
 
  ToastAndroid,
  ActivityIndicator,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  StatusBar,
  Switch,
  FlatList,
  Keyboard,
  TouchableWithoutFeedback
} from "react-native";
import { CheckBox } from "react-native-elements";
import {
  NativeModules,
  requireNativeComponent,
  findNodeHandle
} from "react-native";

import { createStackNavigator, createAppContainer } from "react-navigation";
import AsyncStorage from "@react-native-community/async-storage";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import SearchBar from "react-native-search-bar";
import { withNavigation } from "react-navigation";
//import { Switch } from "react-native-switch";
import ToggleSwitch from "toggle-switch-react-native";
import APISingletonClass, { BASEURL, ADD_WATCHLIST } from "./API";
import { ScaledSheet } from "react-native-size-matters";
import { scale, verticalScale, moderateScale } from "react-native-size-matters";

import Spinner from "react-native-loading-spinner-overlay";
const EmptyComponent = ({ title }) => (
  <View style={styles.emptyContainer}>
    <Text style={styles.emptyText}>{title}</Text>
  </View>
);
class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      token: "",
      loading: false,
      animating: false,
      loginInProcess: false,
      switchedBool: false,
      visible: false,
      searchtext: "",
      ProfilePic: "",
      wtachList: [],
      focus: true,
      checking: [],
      addedWatchList: []
    };
  }

  loginOptions() {
    this.props.navigation.navigate("ProfileNav", {
      DataHomeComment: ""
    });
  }
  getData = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Pic", value);
        this.setState({
          ProfilePic: value
        });

        //this.GetProfile();
      }
    } catch (e) {
      // error reading value
    }
  };
  handleChange = index => {
    console.log("Selected index : " + index);
    let { checking } = this.state;
    checking[index] = !checking[index];
    this.setState({ checking });
    console.log("Checking State in  Handle:" + this.state.checking);
  };
  componentDidMount() {
    this.getData("profilePhoto");
    this.getToken("token");
  }
  addIncident() {
    //this.props.navigation.push('Friends')
    // if (!this.state.loginInProcess) {
    this.setState({
      loginInProcess: true,
      animating: true
    });

    const form = [];
    for (i = 0; i < this.state.wtachList.length; i++) {
      console.log("Invite Friends: " + this.state.wtachList[i].id);
      if (this.state.checking[i] == true) {
        form.push(this.state.wtachList[i].id);
        console.log("Invite Friends: Checked" + this.state.wtachList[i].id);
      } else {
      }
    }
    if (form.length == 0) {
      Alert.alert(
        "BBF",
        "Selected Watchlist can not be empty.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
      return;
    }
    const formD = new FormData();

    // console.log("Form", form);
    // form.append("Photo", photo);
    formD.append("incidentId", form);
    let body = {
      incidentId: form
    };
    console.log("Body Print:");
    console.log(body);
    //http://51.143.17.1:3310/api/User/Login?userName=s4%40gmail.com&password=Snehal123
    console.log("Body Added:", body);
    //var BASEURL = "http://192.168.0.12:1011";
    var BASEURL =  "http://bbf.dimitrisnowden.com"
    //var BASEURL = "http://51.143.17.1:3310";
    APISingletonClass.getInstance().postMethodServiceHandler(
      BASEURL + "/api/Incident/AddToWatchList",
      form,
      "Bearer " + this.state.token,
      responce => {
        let responseJson = JSON.parse(responce);
        console.log("responseJson", responseJson);
        //console.warn("_loginServiceHandler " + responseJson.status)
        this.setState({
          animating: false
        });
        if (responseJson.Status == true) {
          //  this.storeData("token", responseJson.Result.token);
          setTimeout(() => {
            Alert.alert(
              "BBF",
              responseJson.Result.message,
              [
                {
                  text: "OK",
                  onPress: () => console.log("Ok")
                }
              ],
              { cancelable: false }
            );
          }, 200);
        } else {
          setTimeout(() => {
            Alert.alert(
              "BBF",
              responseJson.ResponseException.ExceptionMessage,
              [
                {
                  text: "OK",
                  onPress: () => console.log("Dislike.")
                }
              ],
              { cancelable: false }
            );
          }, 200);
        }
      },
      error => {
        this.setState({
          loading: false
        });
                      this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);

        console.warn("_loginServiceHandler error " + error);
      }
    );
  }
  getToken = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Pic Token", value);
        this.setState({
          token: value
        });
      }
    } catch (e) {}
  };
  searchButtonPressed() {
    this.setState({ visible: true });
    this.setState({ animating: true, focus: false });
    this.refs.searchBar.blur();
    console.log("Search text", this.state.searchtext);
    const body = {};
    // NativeModules.RNSearchBarManager.toggleCancelButton(
    //   findNodeHandle(this),
    //   false
    // );
    console.log("URL:-","http://bbf.dimitrisnowden.com/api/Incident/GetIncidents?isApproved=true&search=" +
    this.state.searchtext)
    APISingletonClass.getInstance().getMethodServiceHandler(
      "http://bbf.dimitrisnowden.com/api/Incident/GetIncidents?isApproved=true&search=" +
        this.state.searchtext,
      body,
      "Bearer " + this.state.token,
      responce => {
        console.log("responseJson Search", responce);
        let responseJson = JSON.parse(responce);
        this.setState({
          animating: false,
          focus: true
        });
        console.log("responseJson Eula", responseJson);
        //console.warn("_loginServiceHandler " + responseJson.status)
        if (responseJson.Status == true) {
          //  var nameArr = responseJson.Result.location.split(',');
          this.setState({
            wtachList: responseJson.Result
          });
          // if (responseJson.Result.accepted) {
          //   this.props.navigation.navigate("homePageBbf", {
          //     DataHomeComment: ""
          //   });
          // }
        } else {
        }
      },
      error => {
        this.setState({
          loading: false
        });
                      this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);

        console.warn("_loginServiceHandler error " + error);
      }
    );
  }
  render_FlatList_footer = () => {
    var footer_View = (
      <View>
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <View
            style={{
              marginTop: moderateScale(20),
              marginLeft: 20,
              marginRight: 20,
              height: moderateScale(90),
              flexDirection: "row",
              justifyContent: "space-evenly"
            }}
          >
            <View
              style={{
                backgroundColor: "transparent",
                width: moderateScale(50),
                height: moderateScale(50),
                borderRadius: moderateScale(25),
                borderWidth: moderateScale(3),
                borderColor: "#7098AA"
              }}
            >
              <Text
                style={{
                  fontSize: moderateScale(32),
                  // marginTop: moderateScale(4),
                  color: "#FEFEFE",
                  alignSelf: "center",
                  fontWeight: "300",
                  textAlign: "center",
                  fontFamily: "SegoeUI-Light"
                }}
              >
                {""}
                {this.state.wtachList.length}
              </Text>
            </View>
            <Text
              style={{
                fontSize: moderateScale(30),
                color: "#FEFEFE",
                textAlign: "left",
                fontWeight: "300",
                marginTop: moderateScale(12),
                marginLeft: 5,
                fontFamily: "SegoeUI-Light"
              }}
            >
              Incidents reported
            </Text>
          </View>
        </TouchableWithoutFeedback>
        <TouchableOpacity onPress={() => this.addIncident()}>
          <View
            style={{
              height: moderateScale(150),
              width: moderateScale(150),
              alignSelf: "center",
              resizeMode: "contain",
              marginLeft: 20,
              backgroundColor: "transparent"
            }}
          >
            <Image
              source={require("./Assets/watch_list.png")}
              size={28}
              borderRadius={15}
              style={{
                height: moderateScale(80),
                width: moderateScale(80),
                alignSelf: "center",
                resizeMode: "contain",
                marginTop: moderateScale(10),
                backgroundColor: "transparent"
              }}
            />
            <Text
              style={{
                fontSize: moderateScale(18),
                color: "#FEFEFE",
                alignSelf: "center",
                textAlign: "center",
                fontWeight: "300",
                marginTop: 0,
                fontFamily: "SegoeUI-Light"
              }}
            >
              ADD WATCH LIST
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );

    return footer_View;
  };
  optionPress(option) {
    if (option == "0") {
    } else if (option == "1") {
    } else if (option == "2") {
    } else if (option == "3") {
    }
  }
  render() {
    return (
      <View style={{ backgroundColor: "gray" }}>
        <StatusBar hidden={true} />
        <Spinner
          visible={this.state.animating}
          //textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <ImageBackground
            source={require("./Assets/main_background.png")}
            size={28}
            // borderRadius={15}
            style={{
              height: Dimensions.get("window").height,
              width: Dimensions.get("window").width,
              resizeMode: "contain"
            }}
          >
            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <View
                style={{
                  flexDirection: "row",
                  width: scale(230),
                  left: 15,
                  backgroundColor: "transparent"
                }}
              >
                <TouchableOpacity
                  onPress={() => this.props.navigation.goBack()}
                >
                  <Image
                    source={require("./Assets/back.png")}
                    size={28}
                    borderRadius={15}
                    //onPress={this.props.navigation.goBack()}
                    style={{
                      height: verticalScale(60),
                      width: scale(30),
                      alignSelf: "center",
                      resizeMode: "contain",
                      marginTop: moderateScale(34),
                      backgroundColor: "transparent",
                      justifyContent: "flex-start",
                      backgroundColor: "transparent"
                    }}
                  />
                </TouchableOpacity>
                <Text
                  style={{
                    fontSize: moderateScale(25),
                    color: "#FEFEFE",
                    textAlign: "center",
                    fontWeight: "200",
                    marginTop: moderateScale(46),
                    marginLeft: 0,
                    marginRight: 20,
                    backgroundColor: "transparent",
                    fontFamily: "SegoeUI-Light"
                  }}
                >
                  {" "}
                  S E A R C H
                </Text>
              </View>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("ProfileNav1", {
                    DataHomeComment: ""
                  })
                }
              >
                <View
                  style={{
                    marginRight: 10,
                    marginTop: moderateScale(20),
                    alignSelf: "flex-end",
                    width: moderateScale(75),
                    height: moderateScale(75),
                    borderRadius: moderateScale(40),
                    borderWidth: moderateScale(3),
                    borderColor: "#525353",
                    backgroundColor: "transparent"
                  }}
                >
                  <Image
                    style={styles.avatar}
                    //source={require("./Assets/BrianImage.jpeg")}
                    source={{
                      uri: BASEURL + "/" + this.state.ProfilePic
                    }}
                    onPress={this.chooseFile}
                    resizeMode="cover"
                  />
                </View>
              </TouchableOpacity>
            </View>

            <SearchBar
              ref="searchBar"
              placeholder="Address,business name or city"
              barTintColor="#7098AA"
              hideBackground={true}
              tintColor="#7098AA"
              marginLeft={20}
              marginRight={20}
              marginTop={40}
              borderRadius={28}
              borderWidth={2}
              borderColor="#7098AA"
              backgroundColor="transparent"
              textFieldBackgroundColor="transparent"
              textColor="#FEFEFE"
              showsCancelButton={false}
              showsCancelButtonWhileEditing={false}
              enablesReturnKeyAutomatically={true}
              returnKeyType={"search"}
              onSearchButtonPress={this.searchButtonPressed.bind(this)}
              //   onChangeText={...}
              onChangeText={searchtext => {
                this.setState({ searchtext });
              }}
              //   onCancelButtonPress={...}
            />
            {this.state.visible == true ? (
              <FlatList
                margin={10}
                numColumns="1"
                data={this.state.wtachList}
                showsVerticalScrollIndicator={true}
                ListEmptyComponent={
                  <EmptyComponent title="No Search results Found." />
                }
                ListFooterComponent={this.render_FlatList_footer}
                //   keyExtractor={item => item}
                renderItem={({ item, index }) => (
                  <View style={styles.footerWrapperNumberCL}>
                    <View style={styles.footerWrapperCheck}>
                      <Text
                        style={{
                          fontSize: moderateScale(22),
                          color: "#7098AA",
                          textAlign: "left",
                          fontWeight: "400",
                          marginTop: moderateScale(20),
                          marginLeft: 20,
                          marginRight: 20,
                          fontFamily: "SegoeUI-Light"
                        }}
                      >
                        {item.location.split(",")[0]}
                      </Text>
                      <CheckBox
                        // checkedIcon={<Image source={require("./assets/Checked.png")} />}
                        // uncheckedIcon={<Image source={require("./assets/unCheck.png")} />}
                        checkedIcon="check-square-o"
                        uncheckedIcon="square-o"
                        checkedColor="#7098AA"
                        //value={this.state.checked}
                        onPress={() => this.handleChange(index)}
                        checked={this.state.checking[index]}
                        // onPress=
                        containerStyle={{
                          marginTop: moderateScale(20),
                          marginRight: 0,
                          width: moderateScale(30),
                          height: moderateScale(30),
                          alignItems: "center",
                          backgroundColor: "transparent",
                          borderWidth: 0,
                          paddingTop: 0,
                          paddingRight: 0,
                          paddingBottom: 0
                        }}
                        //  ima={24}
                      />
                    </View>
                    <Text
                      style={{
                        fontSize: moderateScale(20),
                        color: "#FEFEFE",
                        textAlign: "left",
                        fontWeight: "200",
                        marginTop: moderateScale(5),
                        marginLeft: 20,
                        marginRight: 20,
                        fontFamily: "SegoeUI-Light"
                      }}
                    >
                      {item.location}
                    </Text>
                  </View>
                )}
              />
            ) : (
              <View />
            )}
          </ImageBackground>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ecf0f1"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 10
  },
  sharingtext: {
    color: "#AA2E2E",
    backgroundColor: "#fff",
    borderRadius: 14,
    borderColor: "#AA2E2E",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 18,
    width: 130,
    height: 30,
    alignItems: "center",
    alignContent: "center",
    textAlignVertical: "center"
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover" // or 'stretch'
  },
  footerWrapperlikeCommentShare: {
    flexWrap: "nowrap",
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginLeft: 30,
    marginRight: 30
  },
  separatorlikeShare: {
    flex: 0,
    borderWidth: 1,
    borderColor: "#ECEDED"
  },
  spinnerTextStyle: {
    color: "#565656"
  },
  imageSize2: {
    width: 60,
    height: 60,
    padding: 5,
    borderRadius: 10
  },
  avatar: {
    width: moderateScale(70),
    height: moderateScale(70),
    borderRadius: moderateScale(35),
    borderWidth: moderateScale(3),
    borderColor: "#7098AA",
    alignItems: "center"
  },
  header_footer_style: {
    width: "100%",
    height: 44,
    backgroundColor: "#4CAF50",
    alignItems: "center",
    justifyContent: "center"
  },
  textStyle: {
    textAlign: "center",
    color: "#fff",
    fontSize: 21
  },
  emptyContainer: {
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    alignContent: "center",
    marginTop: 30
  },
  emptyText: {
    fontSize: 20,

    color: "white",
    borderWidth: 1,
    borderColor: "#ECEDED",
    padding: 20
  },
  footerWrapperCheck: {
    //   flexWrap: "wrap",
    marginTop: 5,
    flexDirection: "row",
    justifyContent: "space-between"
  }
});

export default withNavigation(Search);
