import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  ScrollView,
  NavigatorIOS,
  Alert,
  Image,
  AsyncStorage,
  ToastAndroid,
  ActivityIndicator,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  StatusBar,
  Switch,
  FlatList
} from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import SearchBar from "react-native-search-bar";
import { withNavigation } from "react-navigation";
//import { Switch } from "react-native-switch";
import ToggleSwitch from "toggle-switch-react-native";
import APISingletonClass, { BASEURL } from "./API";
import { ScaledSheet } from "react-native-size-matters";
import Spinner from "react-native-loading-spinner-overlay";
import { scale, verticalScale, moderateScale } from "react-native-size-matters";
import { CheckBox } from "react-native-elements";
const EmptyComponent = ({ title }) => (
  <View style={styles.emptyContainer}>
    <Text style={styles.emptyText}>{title}</Text>
  </View>
);
class Incidents extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      loading: false,
      animating: false,
      loginInProcess: false,
      switchedBool: false,
      ProfilePic: "",
      visible: false,
      latitude: "",
      longitude: "",
      incidentsList: [],
      checking: [],
      addedWatchList: [],
      token: "",
      marker: {}
    };
    //   this.inputRefs = {};
    //   this.validate = this.validate.bind(this);
  }
  handleChange = index => {
    console.log("Selected index : " + index);
    let { checking } = this.state;
    checking[index] = !checking[index];
    this.setState({ checking });
    console.log("Checking State in  Handle:" + this.state.checking);
  };
  addIncident() {
    //this.props.navigation.push('Friends')
    // if (!this.state.loginInProcess) {
    this.setState({
      loginInProcess: true,
      animating: true
    });

    const form = [];
    for (i = 0; i < this.state.incidentsList.length; i++) {
      console.log("Invite Friends: " + this.state.incidentsList[i].id);
      if (this.state.checking[i] == true) {
        form.push(this.state.incidentsList[i].id);
        console.log("Invite Friends: Checked" + this.state.incidentsList[i].id);
      } else {
      }
    }
    if (form.length == 0) {
      Alert.alert(
        "BBF",
        "Selected Watchlist can not be empty.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
      return;
    }
    const formD = new FormData();

    // console.log("Form", form);
    // form.append("Photo", photo);
    formD.append("incidentId", form);
    let body = {
      incidentId: form
    };
    console.log("Body Print:");
    console.log(body);
    //http://51.143.17.1:3310/api/User/Login?userName=s4%40gmail.com&password=Snehal123
    console.log("Body Added:", body);
    // var BASEURL =  "http://192.168.0.12:1011"
    var BASEURL = "http://bbf.dimitrisnowden.com";
   // var BASEURL = "http://51.143.17.1:3310";
    APISingletonClass.getInstance().postMethodServiceHandler(
      BASEURL + "/api/Incident/AddToWatchList",
      form,
      "Bearer " + this.state.token,
      responce => {
        let responseJson = JSON.parse(responce);
        console.log("responseJson", responseJson);
        //console.warn("_loginServiceHandler " + responseJson.status)
        this.setState({
          animating: false
        });
        if (responseJson.Status == true) {
          //  this.storeData("token", responseJson.Result.token);
          setTimeout(() => {
            Alert.alert(
              "BBF",
              responseJson.Result.message,
              [
                {
                  text: "OK",
                  onPress: () => console.log("Ok")
                }
              ],
              { cancelable: false }
            );
          }, 200);
        } else {
          setTimeout(() => {
            Alert.alert(
              "BBF",
              responseJson.ResponseException.ExceptionMessage,
              [
                {
                  text: "OK",
                  onPress: () => console.log("Dislike.")
                }
              ],
              { cancelable: false }
            );
          }, 200);
        }
      },
      error => {
        this.setState({
          loading: false
        });
                      this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);

        console.warn("_loginServiceHandler error " + error);
      }
    );
  }
  loginOptions() {
    this.props.navigation.navigate("ProfileNav", {
      DataHomeComment: ""
    });
  }
  getData = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Pic", value);
        this.setState({
          ProfilePic: value
        });

        //this.GetProfile();
      }
    } catch (e) {
      // error reading value
    }
  };
  componentDidMount() {
    this.state.latitude = this.props.navigation.state.params.latitude;
    this.state.longitude = this.props.navigation.state.params.longitude;
    this.state.token = this.props.navigation.state.params.token;
    this.GetIncidents();
    this.getData("profilePhoto");
  }
  GetIncidents() {
    //this.props.navigation.push('Friends')
    this.setState({
      loginInProcess: true,
      animating: true
    });
    let body = {};
    // console.log("Body Print:");
    // console.log(body);
    // console.log("EulaIdd", this.state.eulaId);
    // Alert.alert(
    //   "BBF",
    //   "Inside Incident",
    //   [{ text: "OK", onPress: () => console.log("OK Pressed") }],
    //   { cancelable: false }
    // );
    console.log(
      "Url Printing",
      "http://192.168.0.20:1011" +
        "/api/Incident/GetNearByIncidents" +
        "?lat=" +
        String(this.state.lastLat) +
        "&lan=" +
        String(this.state.lastLong)
    );
    // var BASEURL1 =  "http://192.168.0.12:1011"
    var BASEURL1 =  "http://bbf.dimitrisnowden.com"
   // const BASEURL1 = "http://51.143.17.1:3310";
    APISingletonClass.getInstance().getMethodServiceHandler(
      BASEURL1 +
        "/api/Incident/GetIncidentByGeo" +
        "?lat=" +
        String(this.state.latitude) +
        "&lan=" +
        String(this.state.longitude),
      body,
      "",
      responce => {
        console.log("responseJson Eula", responce);
        let responseJson = JSON.parse(responce);
        this.setState({
          animating: false
        });
        console.log("responseJson Eula", responseJson);
        //console.warn("_loginServiceHandler " + responseJson.status)
        if (responseJson.Status == true) {
          this.setState({
            incidentsList: responseJson.Result
          });
          // if (responseJson.Result.accepted) {
          //   this.props.navigation.navigate("homePageBbf", {
          //     DataHomeComment: ""
          //   });
          // }
        } else {
        }
      },
      error => {
        this.setState({
          loading: false
        });
                      this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);

        console.warn("_loginServiceHandler error " + error);
      }
    );
  }

  searchButtonPressed() {
    this.setState({ visible: !this.state.visible });
  }
  render_FlatList_footer = () => {
    var footer_View = (
      <View>
        <TouchableOpacity onPress={() => this.addIncident()}>
          <View
            style={{
              height: moderateScale(150),
              width: moderateScale(150),
              alignSelf: "center",
              resizeMode: "contain",
              marginLeft: 20,
              backgroundColor: "transparent"
            }}
          >
            <Image
              source={require("./Assets/watch_list.png")}
              size={28}
              borderRadius={15}
              style={{
                height: moderateScale(80),
                width: moderateScale(80),
                alignSelf: "center",
                resizeMode: "contain",
                marginTop: moderateScale(10),
                backgroundColor: "transparent"
              }}
            />
            <Text
              style={{
                fontSize: moderateScale(18),
                color: "#FEFEFE",
                alignSelf: "center",
                textAlign: "center",
                fontWeight: "300",
                marginTop: 0,
                fontFamily: "SegoeUI-Light"
              }}
            >
              ADD WATCH LIST
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );

    return footer_View;
  };
  render_FlatList_header = item => {
    var header_View = (
      <View>
        <View
          style={{
            marginTop: moderateScale(10),
            marginLeft: 20,
            marginRight: 20,
            height: moderateScale(90),
            flexDirection: "row",
            justifyContent: "space-evenly"
          }}
        >
          <View
            style={{
              backgroundColor: "transparent",
              width: moderateScale(50),
              height: moderateScale(50),
              borderRadius: moderateScale(40),
              borderWidth: moderateScale(3),
              borderColor: "#7098AA"
            }}
          >
            <Text
              style={{
                fontSize: moderateScale(30),
                marginTop: moderateScale(0),
                color: "#FEFEFE",
                alignSelf: "center",
                fontWeight: "300",
                textAlign: "center",
                fontFamily: "SegoeUI-Light"
              }}
            >
              {this.state.incidentsList.length}
            </Text>
          </View>
          <Text
            style={{
              fontSize: moderateScale(30),
              color: "#FEFEFE",
              textAlign: "left",
              fontWeight: "300",
              marginTop: moderateScale(10),
              marginLeft: 5,
              fontFamily: "SegoeUI-Light"
            }}
          >
            Incidents reported
          </Text>
        </View>
        <View style={styles.footerWrapperNumberCL}>
          <Text
            style={{
              fontSize: moderateScale(25),
              color: "#7098AA",
              textAlign: "left",
              fontWeight: "400",
              marginTop: 0,
              marginLeft: 20,
              marginRight: 20,
              fontFamily: "SegoeUI-Light"
            }}
          >
            {this.state.incidentsList.length != 0
              ? this.state.incidentsList[0].location.split(",")[0]
              : "null"}
          </Text>
          <Text
            style={{
              fontSize: moderateScale(21),
              color: "#FEFEFE",
              textAlign: "left",
              fontWeight: "200",
              marginTop: moderateScale(5),
              marginLeft: 20,
              marginRight: 20,
              fontFamily: "SegoeUI-Light"
            }}
          >
            {this.state.incidentsList.length != 0
              ? this.state.incidentsList[0].location
              : "null"}
          </Text>
        </View>
        <View
          style={{
            height: 1,
            backgroundColor: "#C0C0C0",
            marginLeft: 20,
            marginRight: 20,
            marginTop: 15
          }}
        />
      </View>
    );

    return header_View;
  };
  optionPress(option) {
    if (option == "0") {
    } else if (option == "1") {
    } else if (option == "2") {
    } else if (option == "3") {
    }
  }
  render() {
    this.state.latitude = this.props.navigation.state.params.latitude;
    this.state.longitude = this.props.navigation.state.params.longitude;
    this.state.token = this.props.navigation.state.params.token;
    this.state.marker = this.props.navigation.state.params.marker;
    return (
      <View style={{ backgroundColor: "gray" }}>
        <StatusBar hidden={true} />
        <Spinner
          visible={this.state.animating}
          //textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <ImageBackground
          source={require("./Assets/main_background.png")}
          size={28}
          // borderRadius={15}
          style={{
            height: Dimensions.get("window").height,
            width: Dimensions.get("window").width,
            resizeMode: "contain"
          }}
        >
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <View
              style={{
                flexDirection: "row",
                width: scale(230),
                left: 15,
                backgroundColor: "transparent"
              }}
            >
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image
                  source={require("./Assets/back.png")}
                  size={28}
                  borderRadius={15}
                  //onPress={this.props.navigation.goBack()}
                  style={{
                    height: verticalScale(60),
                    width: scale(30),
                    alignSelf: "center",
                    resizeMode: "contain",
                    marginTop: moderateScale(24),
                    backgroundColor: "transparent",
                    justifyContent: "flex-start",
                    backgroundColor: "transparent"
                  }}
                />
              </TouchableOpacity>
              <Text
                style={{
                  fontSize: moderateScale(25),
                  color: "#FEFEFE",
                  textAlign: "center",
                  fontWeight: "200",
                  marginTop: moderateScale(40),
                  marginLeft: 0,
                  marginRight: 20,
                  backgroundColor: "transparent",
                  fontFamily: "SegoeUI-Light"
                }}
              >
                {" "}
                I N C I D E N T S
              </Text>
            </View>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("ProfileNav1", {
                  DataHomeComment: ""
                })
              }
            >
              <View
                style={{
                  marginRight: 10,
                  marginTop: moderateScale(20),
                  alignSelf: "flex-end",
                  width: moderateScale(75),
                  height: moderateScale(75),
                  borderRadius: moderateScale(40),
                  borderWidth: moderateScale(3),
                  borderColor: "#525353",
                  backgroundColor: "transparent"
                }}
              >
                <Image
                  style={styles.avatar}
                  //source={require("./Assets/BrianImage.jpeg")}
                  source={{
                    uri: BASEURL + "/" + this.state.ProfilePic
                  }}
                  onPress={this.chooseFile}
                  resizeMode="cover"
                />
              </View>
            </TouchableOpacity>
          </View>

          <FlatList
            margin={10}
            numColumns="1"
            data={this.state.incidentsList}
            showsVerticalScrollIndicator={true}
            ListFooterComponent={this.render_FlatList_footer.bind()}
            ListHeaderComponent={this.render_FlatList_header.bind(this)}
            ListEmptyComponent={<EmptyComponent title="No Incidents Found." />}
            // keyExtractor={item => item}
            renderItem={({ item, index }) => (
              <View style={styles.footerWrapperNumberCL}>
                <View style={styles.footerWrapperCheck}>
                  <Text
                    style={{
                      fontSize: moderateScale(20),
                      color: "#FEFEFE",
                      textAlign: "left",
                      fontWeight: "200",
                      marginTop: moderateScale(15),
                      marginLeft: 20,
                      marginRight: 20,
                      width: Dimensions.get("window").width - 100,
                      fontFamily: "SegoeUI-Light"
                    }}
                  >
                    {item.location}
                  </Text>
                  <CheckBox
                    // checkedIcon={<Image source={require("./assets/Checked.png")} />}
                    // uncheckedIcon={<Image source={require("./assets/unCheck.png")} />}
                    checkedIcon="check-square-o"
                    uncheckedIcon="square-o"
                    checkedColor="#7098AA"
                    //value={this.state.checked}
                    onPress={() => this.handleChange(index)}
                    checked={this.state.checking[index]}
                    // onPress=
                    containerStyle={{
                      marginTop: moderateScale(15),
                      marginRight: 0,
                      width: moderateScale(30),
                      height: moderateScale(30),
                      alignItems: "center",
                      backgroundColor: "transparent",
                      borderWidth: 0,
                      paddingTop: 0,
                      paddingRight: 0,
                      paddingBottom: 0
                    }}
                    //  ima={24}
                  />
                </View>
              </View>
            )}
          />
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ecf0f1"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 10
  },
  sharingtext: {
    color: "#AA2E2E",
    backgroundColor: "#fff",
    borderRadius: 14,
    borderColor: "#AA2E2E",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 18,
    width: 130,
    height: 30,
    alignItems: "center",
    alignContent: "center",
    textAlignVertical: "center"
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover" // or 'stretch'
  },
  footerWrapperlikeCommentShare: {
    flexWrap: "nowrap",
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginLeft: 30,
    marginRight: 30
  },
  separatorlikeShare: {
    flex: 0,
    borderWidth: 1,
    borderColor: "#ECEDED"
  },
  imageSize2: {
    width: 60,
    height: 60,
    padding: 5,
    borderRadius: 10
  },
  avatar: {
    width: moderateScale(70),
    height: moderateScale(70),
    borderRadius: moderateScale(35),
    borderWidth: moderateScale(3),
    borderColor: "#7098AA",
    alignItems: "center"
  },
  header_footer_style: {
    width: "100%",
    height: 44,
    backgroundColor: "#4CAF50",
    alignItems: "center",
    justifyContent: "center"
  },
  footerWrapperCheck: {
    //   flexWrap: "wrap",
    marginTop: 5,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  textStyle: {
    textAlign: "center",
    color: "#fff",
    fontSize: 21
  },

  emptyContainer: {
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "center",
    alignContent: "center",
    marginTop: 30
  },
  emptyText: {
    fontSize: 20,

    color: "white",
    borderWidth: 1,
    borderColor: "#ECEDED",
    padding: 20
  }
});

export default withNavigation(Incidents);
