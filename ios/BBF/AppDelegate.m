/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
#import "AppDelegate.h"
#import <React/RCTBridge.h>
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <RNGoogleSignin/RNGoogleSignin.h>
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
 // NSURL *jsCodeLocation;
  for (NSString* family in [UIFont familyNames])
  {
    NSLog(@"font name%@", family);
    for (NSString* name in [UIFont fontNamesForFamilyName: family])
    {
      NSLog(@"font family %@", name);
    }
  }
  RCTBridge *bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:launchOptions];
  RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:bridge
                                                   moduleName:@"BBF"
                                            initialProperties:nil];

  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];

  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  [[FBSDKApplicationDelegate sharedInstance]application:application didFinishLaunchingWithOptions:launchOptions ];
  return YES;
}

- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge
{
#if DEBUG
  return [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
#else
  return [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
#endif
}
//- (BOOL)application:(UIApplication *)application
//            openURL:(NSURL *)url
//            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
//
////  BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
////                                                                openURL:url
////                                                      sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
////                                                             annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
////                  ];
//  if ([[GIDSignIn sharedInstance] handleURL:url
//                          sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
//                                 annotation:options[UIApplicationOpenURLOptionsAnnotationKey]]) {
//    return YES;
//    // For Facebook SDK
//  }else if ( [[FBSDKApplicationDelegate sharedInstance] application:application
//                                                            openURL:url
//                                                  sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
//                                                         annotation:options[UIApplicationOpenURLOptionsAnnotationKey]]){
//    return YES;
//    //For Google plus SDK
//  }
//
//  return NO;
//
////  return [RNGoogleSignin application:application
////                             openURL:url
////                   sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
////                          annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
////          ];
//  // Add any custom logic here.
////  return handled;
//}
  - (BOOL)application:(UIApplication *)application openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options
  {
    return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey] annotation:options[UIApplicationOpenURLOptionsAnnotationKey]]
    || [RNGoogleSignin application:application openURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey] annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
  }
//- (BOOL)application:(UIApplication *)app
//            openURL:(NSURL *)url
//            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options
//{
//  if ([[FBSDKApplicationDelegate sharedInstance] application:app openURL:url options:options]) {
//    return YES;
//  }
//
//
//
//  return NO;
//}
- (void)applicationDidBecomeActive:(UIApplication *)application{
  [FBSDKAppEvents activateApp];
}
@end
