import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  ScrollView,
  NavigatorIOS,
  Alert,
  Image,
  ToastAndroid,
 
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  StatusBar,
  SafeAreaView
} from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import DatePicker from "react-native-datepicker";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
//import Icon from "react-native-vector-icons/FontAwesome";
import { withNavigation } from "react-navigation";
import { ScaledSheet } from "react-native-size-matters";
import { scale, verticalScale, moderateScale } from "react-native-size-matters";
var ImagePicker = require("react-native-image-picker");
import AsyncStorage from "@react-native-community/async-storage";
import Spinner from "react-native-loading-spinner-overlay";
import Moment from "moment";
var {FBLogin, FBLoginManager} = require('react-native-facebook-login');
import CookieManager from 'react-native-cookies';
import { GoogleSignin, statusCodes } from 'react-native-google-signin';
import APISingletonClass, {
  PROFILE_API,
  UPDATE_PROFILE_API,
  BASEURL,
  EULA_API,
  LOGOUT
} from "./API";
class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      loading: false,
      animating: false,
      loginInProcess: false,
      nameSatate: "",
      phoneNumber: "",
      location: "",
      dob: "",
      filePath: [],
      token: "",
      profileImage: "",
      imageEditClicked: false,
      filePath: [],
      newFileName: "",
      editable: true,
      eulaState: false,
      isFromSocila:"",
      socialFirstName:"",
      socialLastName:"",
      SocialId:"",
      SocialEmail:"",
      SocialProfileImage:"",
      socialToken:"",
      profileUpdated:false,
      userInfo:null,
      googleInfo:null
    };
    //   this.inputRefs = {};
    //   this.validate = this.validate.bind(this);
  }
  componentWillMount(){
 
   // this.state.isFromSocila = this.props.navigation.state.params.fromSocialLogin;
   GoogleSignin.hasPlayServices({ autoResolve: true }).then(() => {
    // play services are available. can now configure library
    GoogleSignin.configure({
    iosClientId:"719586895703-3gnse6rpvkdqnjk2e8fcmrqeo08fi4r0.apps.googleusercontent.com",
   // webClientId: Platform.OS === 'ios' ? '719586895703-3gnse6rpvkdqnjk2e8fcmrqeo08fi4r0.apps.googleusercontent.com"' : 'Client Id From Google services.json'
    // only for iOS
  })
  .then(() => {
    // you can now call currentUserAsync()
    // GoogleSignin.currentUserAsync().then((user) => {
    //   console.log('USER', user);
    //   this.setState({user: user});
    // }).done();
    //   this.getData("token");
  });
})
.catch((err) => {
  console.log("Play services error", err.code, err.message);
})
     console.log("Social login status0",this.state.isFromSocila)
     this._subscribe = this.props.navigation.addListener("didFocus", () => {
      // this.getData("token");
      // this.setState({sliderValue:0})
      // this.getToken("token");
      // this.getData("profilePhoto");
      // this.getData("FullName");
      this.setState({profileImage:""})
      CookieManager.getAll()
      .then((res) => {
        console.log('CookieManager.getAll =>', res);
      });
      console.log("Profile called")
     // this.storeData("profilePhoto", "");

      this.getStatusLogin("isFromSocial")
  
    });
  }
  logoutInstagram() {
   
  }
  componentWillUnmount(){
   // this._subscribe.remove();
  }
  componentDidMount() {
    // this.GetProfile();
    // setTimeout(() => {
    //   Alert.alert(
    //     "BBF",
    //     "Hi",
    //     [
    //       {
    //         text: "OK",
    //         onPress: () => console.log("Dislike.")
    //       }
    //     ],
    //     { cancelable: false }
    //   );
    // }, 200);
  //   GoogleSignin.hasPlayServices({ autoResolve: true }).then(() => {
  //     // play services are available. can now configure library
  //     GoogleSignin.configure({
  //     iosClientId:"719586895703-3gnse6rpvkdqnjk2e8fcmrqeo08fi4r0.apps.googleusercontent.com",
  //    // webClientId: Platform.OS === 'ios' ? '719586895703-3gnse6rpvkdqnjk2e8fcmrqeo08fi4r0.apps.googleusercontent.com"' : 'Client Id From Google services.json'
  //     // only for iOS
  //   })
  //   .then(() => {
  //     // you can now call currentUserAsync()
  //     // GoogleSignin.currentUserAsync().then((user) => {
  //     //   console.log('USER', user);
  //     //   this.setState({user: user});
  //     // }).done();
  //     //   this.getData("token");
  //   });
  // })
  // .catch((err) => {
  //   console.log("Play services error", err.code, err.message);
  // })
  //   CookieManager.getAll()
  //   .then((res) => {
  //     console.log('CookieManager.getAll =>', res);
  //   });
  //   console.log("Profile called")
    
  //   this.getStatusLogin("isFromSocial")
    // console.log("Print Token", );
  }
  mobilevalidate(text) {
    const reg = /^[0]?[789642351]\d{11}$/;
    if (reg.test(text) === false) {
      this.setState({
        mobilevalidate: false
        //phonenumber: text
      });
      return false;
    } else {
      this.setState({
        mobilevalidate: true
        //  phonenumber: text
      });
      return true;
    }
  }
  storeData = async (Key, Value) => {
    try {
      await AsyncStorage.setItem(Key, Value);
    } catch (e) {
      // saving error
    }
  };

  GetEulas() {
    //this.props.navigation.push('Friends')
    console.log("Get EULA called")
    
    // this.setState({
    //   loginInProcess: true,
    //   animating: true
    // });
    let body = {};
    console.log("Body Print:");
    console.log(body);
    console.log("TOKEN",this.state.token)
    APISingletonClass.getInstance().getMethodServiceHandler(
      EULA_API,
      body,
      "Bearer " + this.state.token,
      responce => {
        console.log("Get EULA SUCCESS")
        console.log("responseJson Eula", responce);
        let responseJson = JSON.parse(responce);
        // this.setState({
        //   animating: false
        // });
       
        console.log("responseJson Eula", responseJson);
        //console.warn("_loginServiceHandler " + responseJson.status)
        if (responseJson.Status == true) {
          this.setState({
            eulaState: responseJson.Result.accepted
            // switchedBool: responseJson.Result.accepted
          });
          // if (responseJson.Result.accepted) {
          //   this.props.navigation.navigate("homePageBbf", {
          //     DataHomeComment: ""
          //   });
          // }
        } else {
        }
      },
      error => {
        this.setState({
          loading: false
        });
                     this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);

        console.warn("_loginServiceHandler error " + error);
      }
    );
  }
  editProfile = () => {
    this.setState({ animating: true });
    // this.setState({ editable: true });
    this.setState({ animating: false });
  };
  clearData = () => {
    this.setState({
      animating: false
    });
      this.storeData("isFromSocial", "");
  this.storeData("profilePhoto", "");
  this.storeData("FullName", "");
  this.storeData("ScreenShow", "");
  this.storeData("token", "");
  this.storeData("Socialtoken", "");
  this.setState({socialToken:null})
  this.signOutGoogle()
  //this.logoutInstagram()
  CookieManager.clearAll()
  .then((res) => {
    console.log('CookieManager.clearAll =>', res);
    //this.setState({ token: '' })
  });
  FBLoginManager.logout((data) => {
    //do something
 })
 this.props.navigation.goBack();
  };

  signOutGoogle = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      this.setState({ user: null }); // Remember to remove the user from your app's state as well
    } catch (error) {
      console.error(error);
    }
  };
  logout = () => {
    this.setState({
      loginInProcess: true,
      animating: true
    });
    let body = {};
    console.log("Body Print:");
    console.log(body);
    //console.log("EulaIdd", this.state.eulaId);
    console.warn("API:", "http://bbf.dimitrisnowden.com/api/User/Logout");
    APISingletonClass.getInstance().postMethodServiceHandler(
      "http://bbf.dimitrisnowden.com/api/User/Logout",
      body,
      "Bearer " + this.state.token,

      responce => {
        console.log("responseJson Eula", responce);
        let responseJson = JSON.parse(responce);
        console.log("responseJson Eula", responseJson);
        this.setState({
          animating: false
        });
        //console.warn("_loginServiceHandler " + responseJson.status)
        if (responseJson.Status == true) {
          this.storeData("isFromSocial", "");
          this.storeData("profilePhoto", "");
          this.storeData("FullName", "");
          this.storeData("ScreenShow", "");
          this.storeData("token", "");
          this.storeData("Socialtoken", "");
          this.setState({socialToken:null})
          this.signOutGoogle()
          //this.logoutInstagram()
          CookieManager.clearAll()
          .then((res) => {
            console.log('CookieManager.clearAll =>', res);
            //this.setState({ token: '' })
          });
          FBLoginManager.logout((data) => {
            //do something
         })
          setTimeout(() => {
            Alert.alert(
              "BBF",
              responseJson.Result.message,
              [
                {
                  text: "OK",
                  onPress: () =>
                    this.props.navigation.navigate("Home", {
                      DataHomeComment: ""
                    })
                }
              ],
              { cancelable: false }
            );
          }, 200);
        } else {
        }
      },
      error => {
        this.setState({
          loading: false
        });
        this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);
        console.warn("_loginServiceHandler error " + error);
      }
    );
  };
  updateProfile = () => {
    console.log("Update Press");
    var myString = ""
    //console.log(this.state.password);
    if (this.state.phoneNumber != null){
      myString = this.state.phoneNumber.replace(/\D/g, "");
    }    console.log("Phone Number real", myString);
    // this.mobilevalidate(myString);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    let regCheck = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[\w~@#$%^&*+=`|{}:;!.?\"()\[\]-]{6,16}$/;
    if (this.state.nameSatate == "") {
      Alert.alert(
        "BBF",
        "First Name is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (this.state.dob == "") {
      Alert.alert(
        "BBF",
        "Date of Birth is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (myString == "") {
      Alert.alert(
        "BBF",
        "Phone Number is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (myString.length < 10) {
      Alert.alert(
        "BBF",
        "Please add valid mobile number.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (this.state.location == "") {
      Alert.alert(
        "BBF",
        "Location is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else {
      const photo = {
        uri: this.state.filePath.uri,
        type: this.state.filePath.type,
        name: this.state.newFileName
      };
      var lastNameDfirstNameD = "";
      var lastNameD = "";
      var name = this.state.nameSatate;
      console.log("name", name.split(" ")[0]);
      console.log("name1", name.split(" ")[1]);
      var Split = name.split(" ");
      if (Split[0] == undefined) {
        firstNameD = "";
      } else {
        firstNameD = Split[0];
      }
      if (Split[1] == undefined) {
        lastNameD = "";
      } else {
        lastNameD = Split[0];
      }

      console.log("FilePath", this.state.filePath.uri);
      const form = new FormData();
      // console.log("Form", form);
      // form.append("Photo", photo);
      form.append("FirstName", this.state.nameSatate);
      form.append("LastName", "");
      form.append("PhoneNumber", myString);
      form.append("Location", this.state.location);
      form.append("dob", this.state.dob);
      form.append("ProfileImage", photo);
      console.log("Form Data:", form);
      this.setState({
        animating: true
      });

      fetch(BASEURL + "/api/User/UpdateProfile", {
        method: "POST",
        headers: {
          Authorization: "Bearer " + this.state.token
        },
        body: form
      })
        .then(response => {
          return response.json();
        })
        .then(responseData => {
          this.setState({
            animating: false
          });
          var res = JSON.stringify(responseData);
          console.log("Response Data", responseData);
          if (responseData.Status == true) {
            // this.setState({
            //   editable: false
            // });
            this.setState({
              profileUpdated: true
            });
            setTimeout(() => {
              Alert.alert(
                "BBF",
                "Profile Updated Successfully.",
                [
                  {
                    text: "OK",
                    onPress: () => this.GetProfile()
                  }
                ],
                { cancelable: false }
              );
            }, 200);
          } else {
            setTimeout(() => {
              Alert.alert(
                "BBF",
                responseData.ResponseException.ExceptionMessage,
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);
          }

          return responseData.state;
        })
        .catch(error => {
          this.setState({
          animating: false
        });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);
          console.error(error);
        });
    }
  };
  updateProfileImage = imageID => {
    var options = {
      title: "Select Photo",
      cancelButtonTitle: "Cancel",
      takePhotoButtonTitle: "Take Photo",
      chooseFromLibraryButtonTitle: "Choose From Library",
      allowsEditing: true,
      returnBase64Image: false,
      returnIsVertical: false,
      quality: 0.8,
      noData: true,
      storageOptions: {
        skipBackup: true
      }
    };
    ImagePicker.showImagePicker(options, response => {
      // console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled image picker");
        return true;
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
        return true;
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
        alert(response.customButton);
      } else {
        var filname = response.uri.split("/");
        var index2 = filname.length;
        var filename = filname[index2 - 1].replace("'", "");
        const photo = {
          uri: response.uri,
          type: response.type,
          name: response.fileName
        };
        this.setState({
          imageEditClicked: true
        });
        this.setState({ filePath: response });
        this.setState({ newFileName: filename });
        //newFileName
        // const form = new FormData();
        // form.append("profile_image", photo);
        // form.append("user_id", this.state.userid);
        // form.append("image_caption", this.state.editTxt);
        // form.append("image_id", imageID);
        // console.log("------------------------------" + form);
      }
    });
  };

  chooseFile = () => {};
  postingSocialData(typeOfLogin){
    const form = new FormData();
    // console.log("Form", form);
    // form.append("Photo", photo);
    
    form.append("RoleId", 1);
    form.append("Email", this.state.SocialEmail);
    form.append("FirstName", this.state.socialFirstName);
    form.append("LastName", this.state.socialLastName);
    form.append("UserName", this.state.SocialId);
    form.append("ExternalAccountId", this.state.SocialId);
    form.append("ExternalAccessToken", this.state.socialToken);
    form.append("ExternalNetworkType", typeOfLogin);
    // form.append("Lat", "17.4375");
    // form.append("Lan", "78.4482");
    console.log("FormData", form);
    this.setState({
      animating: true
    });
    //console.log("Body Added:", body);
    console.log("API:-", BASEURL + "/api/User/CreateExternalUser");
    fetch(BASEURL + "/api/User/CreateExternalUser", {
      method: "POST",
      // headers: {
      //   "Content-Type": "multipart/form-data;"
      // },
      body: form
    })
      .then(response => {
        return response.json();
      })
      .then(responseData => {
        this.setState({
          animating: false
        });
        var res = JSON.stringify(responseData);
        console.log("Social Response Data", responseData);
        if (responseData.Status == true) {
          this.storeData("token", responseData.Result.token);
          this.getData("token")
          // Alert.alert(
          //   "BBF",
          //   "Registration Successfully Completed.",
          //   [{ text: "OK", onPress: () => this.props.navigation.goBack() }],
          //   { cancelable: false }
          // );

          // setTimeout(() => {
          //   Alert.alert(
          //     "BBF",
          //     "Registration Successfully Completed.",
          //     [
          //       {
          //         text: "OK",
          //         onPress: () => this.props.navigation.goBack()
          //       }
          //     ],
          //     { cancelable: false }
          //   );
          // }, 200);
        } else {
          setTimeout(() => {
            Alert.alert(
              "BBF",
              responseData.ResponseException.ExceptionMessage,
              [
                {
                  text: "OK",
                  onPress: () => this.clearData()
                }
              ],
              { cancelable: false }
            );
          }, 200);
        }

        return responseData.state;
      })
      .catch(error => {
        console.error(error);
                      this.setState({
        animating: false
      });
       setTimeout(() => {
            Alert.alert(
              "BBF",
              "Something Went wrong ,Please try again.",
              [
                {
                  text: "OK",
                  onPress: () => console.log("Dislike.")
                }
              ],
              { cancelable: false }
            );
          }, 200);

      });

  }
  loginOptions() {
    var myString = ""
    if (this.state.phoneNumber != null){
      myString = this.state.phoneNumber.replace(/\D/g, "");
    }
  
    console.log("Phone Number real", myString);
    // this.mobilevalidate(myString);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    let regCheck = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[\w~@#$%^&*+=`|{}:;!.?\"()\[\]-]{6,16}$/;
    if (this.state.nameSatate == "") {
      Alert.alert(
        "BBF",
        "First Name is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );

    } else if ((this.state.dob == "") || (this.state.dob == null)) {
      Alert.alert(
        "BBF",
        "Date of Birth is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
      // if (this.state.profileUpdated == false){
      //   Alert.alert(
      //     "BBF",
      //     "Please update profile.",
      //     [{ text: "OK", onPress: () => console.log("OK Pressed") }],
      //     { cancelable: false }
      //   )
      // }
    } else if (myString == "") {
      Alert.alert(
        "BBF",
        "Phone Number is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if (myString.length < 10) {
      Alert.alert(
        "BBF",
        "Please add valid mobile number.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
    } else if ((this.state.location == "")|| (this.state.location == null)){
      Alert.alert(
        "BBF",
        "Location is a required field.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }],
        { cancelable: false }
      );
      }else if ((this.state.isFromSocila == "true")&&(this.state.profileUpdated==false)){
        Alert.alert(
          "BBF",
          "Please update your profile.",
          [{ text: "OK", onPress: () => console.log("OK Pressed") }],
          { cancelable: false }
        ); 
      }
   else {
    if (this.state.eulaState == true) {
      this.props.navigation.navigate("homePageBbf", {
        DataHomeComment: ""
      });
    } else {
      this.props.navigation.navigate("eulaSetting", {
        eulaBool: false
      });
    }
  }
  }
  getData = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Token", value);
        this.setState({
          token: value
        });
        this.GetProfile();
      }
    } catch (e) {
      // error reading value
    }
  };
async getLinkedInProfile(){
  console.log("Inside Linked in:",this.state.socialToken)
const baseApi = 'https://api.linkedin.com/v2/people/'
const params = [
  'first-name',
  'last-name',
  'email-address',
// add more fields here
]

const response = await fetch(
  `${baseApi}~:(${params.join(',')})?format=json`,
  {
    method: 'GET',
    headers: {
      Authorization: 'Bearer ' + this.state.socialToken
    }
  }
)
const payload = await response.json()
console.log("Linked in response",payload)
  }
  getSocialProfile(typeOfSocial){
   console.log("Social Profile Facebook Called")
    this.setState({
      animating: true
    });
   
  fetch("https://graph.facebook.com/v2.5/me?fields=email,name,friends,picture.height(480)&access_token=" + this.state.socialToken)
  .then((response) => response.json())
  .then((json) => {
    // Some user object has been set up somewhere, build that user here
    console.log("Facebook Data",json)
    this.setState({
      userInfo:json
    })
    this.setState({
      animating: false
    });
    this.setState({
      socialFirstName: json.name
    });
    this.setState({
      SocialId: json.id
    });
    if (json.email == undefined){
      this.setState({
        SocialEmail: "Snehal.r28@gmail.com"
      });
    }else{
      this.setState({
        SocialEmail: json.email
      });
    }
    
    this.setState({
      SocialProfileImage: json.id
    });
    this.postingSocialData("Facebook")
    //SocialProfileImage
    //user.name = json.name
    //user.id = json.id
   // user.user_friends = json.friends
   // user.email = json.email
   // user.username = json.name
   // user.loading = false
   // user.loggedIn = true
    //user.avatar = setAvatar(json.id)      
  })
  .catch(() => {
    reject('ERROR GETTING DATA FROM FACEBOOK')
  })
  }
  getInstaSocialProfile(typeOfSocial){
console.log("Inside Instagram",this.state.socialToken)
this.setState({
  animating: true
});
  fetch("https://api.instagram.com/v1/users/self/?access_token=" + this.state.socialToken)
  .then((response) => response.json())
  .then((json) => {
    // Some user object has been set up somewhere, build that user here
    console.log("Facebook Data",json)
    this.setState({
      animating: false
    });
    this.setState({
      socialFirstName: json.data.full_name
    });
    this.setState({
      SocialId: json.data.id
    });
    this.setState({
      SocialEmail: "snehal.r28454@gmail.com"
    });
    console.log("Insta Social login saved results",this.state.socialFirstName)
    // this.setState({
    //   SocialProfileImage: json.id
    // });
    //SocialProfileImage
    //user.name = json.name
    //user.id = json.id
   // user.user_friends = json.friends
   // user.email = json.email
   // user.username = json.name
   // user.loading = false
   // user.loggedIn = true
    //user.avatar = setAvatar(json.id) 
    this.postingSocialData("Instragram")     
  })
  .catch(() => {
    reject('ERROR GETTING DATA FROM FACEBOOK')
  })
  }
  renderImage(){

    console.log(this.state.userInfo);
    this.render(
      <View>
        <Image source={{url: this.state.userInfo.photoURL}} />
      </View>
    )
}
  getSocialToken = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Token", value);
        this.setState({
          socialToken: value
        });

        this.getTypeOfSocial("SocialLoginType");
      }
    } catch (e) {
      // error reading value
    }
  };

  getSocialGoogle= async key => {
    console.log("Inside google values")
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
       
        var obj = JSON.parse(value);
     
        console.log("Parse values",value)
        this.setState({
          socialFirstName: obj.user.name
        });
        this.setState({
          SocialId:  obj.user.id
        });
        this.setState({
          SocialEmail:obj.user.email
        });
        this.postingSocialData("Facebook")

        //this.getTypeOfSocial("SocialLoginType");
      }
    } catch (e) {
      // error reading value
    }
  };
  getTypeOfSocial = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Token", value);
        if (value == "Facebook"){
         this.getSocialProfile("Facebook")
        }else if (value == "Instagram"){
          this.getInstaSocialProfile("Instagram")
       }else if (value == "Google"){
     //  this.getInstaSocialProfile("Instagram")
     console.log("Inside Google")
     this.getSocialGoogle("GoogleInfo")
     //GoogleInfo
     //console.log("Parameter name",this.props.navigation.state.params.userInformation.name)
    //  AsyncStorage.getItem('GoogleInfo', (err, result) => {
      
    //  })  
     }else{
          this.getLinkedInProfile()
        }
        // this.setState({
        //   socialToken: value
        // });
        
       // this.getSocialProfile(value);
      }
    } catch (e) {
      // error reading value
    }
  };
  getStatusLogin = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Token", value);
        this.setState({
          isFromSocila: value
        });
        if (value == "true"){
         this.getSocialToken("Socialtoken")
        }else{
          this.getData("token");
        }
        //this.GetProfile();
        console.log("Social login status1",this.state.isFromSocila)
      }
    } catch (e) {
      // error reading value
    }
  };

  handleCardNumber = text => {
    // this.setState({ origionalNumber: text });
    var target = "";
    const input = text.replace(/\D/g, "").substring(0, 12);
    const zip = input.substring(0, 3);
    const middle = input.substring(3, 6);
    const last = input.substring(6, 12);
    console.log("Input Print", input);
    if (input.length > 6) {
      target = `(${zip}) - ${middle}  ${last}`;
    } else if (input.length > 3) {
      target = `(${zip}) - ${middle}`;
    } else if (input.length > 0) {
      target = `(${zip}`;
    }
    console.log("Target Print", target);
    this.setState({ phoneNumber: target });
  };
   GetProfile() {
    //this.props.navigation.push('Friends')
    console.log("Get Profile called")
    
    this.setState({
     animating: true
    });
    let body = {};
    // this.setState({
    //   animating: false
    // });
    console.log("Body Print:");
    console.warn("API:",PROFILE_API);
    APISingletonClass.getInstance().getMethodServiceHandler(
      PROFILE_API,
      body,
      "Bearer " + this.state.token,
      responce => {
        this.setState({
          animating: false
        });
        console.log("responseJson Eula", responce);
        let responseJson = JSON.parse(responce);
        console.log("responseJson Eula", responseJson);
        if (responseJson.Status == true) {
          
          // this.storeData("profilePhoto", "");
          // this.setState({
          //   profileImage: "",
          //   filePath:[]
          // });
        console.log("Date is equal to",responseJson.Result.dob)
        //console.warn("_loginServiceHandler " + responseJson.status)
        this.setState({
          animating: false
        });
        if ((responseJson.Result.phoneNumber != null)&&(responseJson.Result.location != null)){
          this.setState({
            profileUpdated: true
          });
        }
        // this.setState({
        //   imageEditClicked: false
        // });
        // var changeDate12 = Moment(responseJson.Result.dob).format("MM-DD-YYYY");
        // var trunc12 = changeDate12.substr(0, 10);
       
          // Moment(dt).format("d MMM");
          var changeDate12 = Moment(responseJson.Result.dob).format("MM-DD-YYYY");
          var trunc12 = changeDate12.substr(0, 10);
          console.log("Inside empty Date equal",trunc1)

          if (trunc12 != "01-01-0001"){
          var changeDate = Moment(responseJson.Result.dob).format("MM-DD-YYYY");
          var trunc1 = changeDate.substr(0, 10);
          this.setState({
            dob: trunc1
          });
          }else{
            var changeDate23 = Moment().format("MM-DD-YYYY");
          var trunc23 = changeDate23.substr(0, 10);
          console.log("Inside empty Date else",trunc23)

          this.setState({
            dob: trunc23
          });
          }
          //  var trunc1 = Date.parse(trunc, "YYYY-MM-dd").toString("MM-DD-YYYY");
          this.storeData("profilePhoto", responseJson.Result.photo);

          this.storeData("ScreenShow", "Profile");
          // if (responseJson.Result.lastName == null) {
          //   this.setState({
          //     nameSatate: responseJson.Result.firstName
          //   });
          // } else {
          //   this.setState({
          //     nameSatate:
          //       responseJson.Result.firstName +
          //       " " +
          //       responseJson.Result.lastName
          //   });
          // }
          if (responseJson.Result.phoneNumber != null){
          var target = "";

          const input = responseJson.Result.phoneNumber
            .replace(/\D/g, "")
            .substring(0, 12);
          const zip = input.substring(0, 3);
          const middle = input.substring(3, 6);
          const last = input.substring(6, 12);
          console.log("Input Print", input);
          if (input.length > 6) {
            target = `(${zip}) - ${middle}  ${last}`;
          } else if (input.length > 3) {
            target = `(${zip}) - ${middle}`;
          } else if (input.length > 0) {
            target = `(${zip}`;
          }
          console.log("Target Print", target);
          this.setState({ phonenumber: target });
        }
          this.setState({
            nameSatate: responseJson.Result.firstName
          });
          this.storeData("FullName", this.state.nameSatate);
          this.setState({
            phoneNumber: target,
            location: responseJson.Result.location,
            
            profileImage: responseJson.Result.photo
          });
          console.log("ProfielImage print", this.state.profileImage);
          // setTimeout(() => {
          //   Alert.alert(
          //     "BBF",
          //     "Inside Get Profile response",
          //     [
          //       {
          //         text: "OK",
          //         onPress: () => this.GetEulas()
          //       }
          //     ],
          //     { cancelable: false }
          //   );
          // }, 200);
          this.GetEulas()
        } else {
          setTimeout(() => {
            Alert.alert(
              "BBF",
              "Something Went wrong ,Please try again.",
              [
                {
                  text: "OK",
                  onPress: () => console.log("Dislike.")
                }
              ],
              { cancelable: false }
            );
          }, 200);
        }
      },
      error => {
        this.setState({
          loading: false
        });
        // this.setState({
        //   animating: false
        // });
         setTimeout(() => {
              Alert.alert(
                "BBF",
                "Something Went wrong ,Please try again.",
                [
                  {
                    text: "OK",
                    onPress: () => console.log("Dislike.")
                  }
                ],
                { cancelable: false }
              );
            }, 200);
        console.warn("_loginServiceHandler error " + error);
      }
    );
  }
  render() {
    let deviceWidth = Dimensions.get("window").width;
    var now = new Date(),
      age = new Date(now.getFullYear(), now.getMonth());
    var profileimageurl = "";
    if (this.state.filePath.uri != null) {
      console.log("this.state.filePath.uri != null",profileimageurl)
      profileimageurl = this.state.filePath.uri;
    }
    //this.state.googleInfo = this.props.navigation.state.params.userInformation
    //this.state.isFromSocila = this.props.navigation.state.params.fromSocialLogin;
    return (
      <View style={{ backgroundColor: "gray" }}>
        <StatusBar hidden={true} />
        <Spinner
          visible={this.state.animating}
          //textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
        />
        <ImageBackground
          source={require("./Assets/main_background.png")}
          size={28}
          // borderRadius={15}
          style={{
            height: Dimensions.get("window").height,
            width: Dimensions.get("window").width,
            resizeMode: "contain"
          }}
        >
          {/* <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          > */}
          <View
            style={{
              flexDirection: "row",
              width: Dimensions.get("window").width,
              left: 0,
              backgroundColor: "transparent",
              justifyContent: "space-between"
            }}
          >
            <TouchableOpacity
              onPress={() => {
                Alert.alert(
                  "BBF",
                  "Are you sure you want to logout.",
                  [
                    { text: "YES", onPress: () => this.logout() },
                    {
                      text: "Cancel",
                      onPress: () => console.log("OK"),
                      style: "cancel"
                    }
                  ],
                  { cancelable: false }
                );
              }}
              //editable={false}
            >
              <Image
                source={require("./Assets/logout_l.png")}
                size={28}
                borderRadius={15}
                // editable={false}
                //hidden={true}
                //onPress={this.props.navigation.goBack()}
                style={{
                  height: moderateScale(60),
                  width: moderateScale(30),
                  alignSelf: "center",
                  resizeMode: "contain",
                  marginTop: moderateScale(24),
                  marginLeft: moderateScale(15),
                  //backgroundColor: "transparent",
                  justifyContent: "flex-start",
                  backgroundColor: "rgba(52, 52, 52, 0.8)"
                  //backgroundColor: "transparent"
                }}
              />
            </TouchableOpacity>
            <Text
              style={{
                fontSize: moderateScale(30),
                color: "#FEFEFE",
                textAlign: "center",
                fontWeight: "200",
                marginTop: moderateScale(40),
                alignContent: "center",
                alignSelf: "center",
                alignItems: "center",
                backgroundColor: "transparent",
                fontFamily: "SegoeUI-Light"
              }}
            >
              {" "}
              it's all about you
            </Text>
            {this.state.editable ? (
              <TouchableOpacity
                onPress={() => {
                  this.state.editable == true
                    ? this.updateProfile()
                    : console.log("OK");
                }}
              >
                <Image
                  source={require("./Assets/done.png")}
                  size={28}
                  borderRadius={15}
                  //onPress={this.props.navigation.goBack()}
                  style={{
                    height: moderateScale(60),
                    width: moderateScale(30),
                    alignSelf: "center",
                    resizeMode: "contain",
                    marginTop: moderateScale(24),
                    marginRight: moderateScale(15),
                    backgroundColor: "transparent",
                    justifyContent: "flex-start",
                    backgroundColor: "rgba(52, 52, 52, 0.5)"
                  }}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() => {
                  this.state.editable == false
                    ? Alert.alert(
                        "BBF",
                        "Are you sure you want to logout.",
                        [
                          { text: "YES", onPress: () => this.logout() },
                          {
                            text: "Cancel",
                            onPress: () => console.log("OK"),
                            style: "cancel"
                          }
                        ],
                        { cancelable: false }
                      )
                    : console.log("OK");
                }}
              >
                <Image
                  source={require("./Assets/logout_l.png")}
                  size={28}
                  borderRadius={15}
                  //onPress={this.props.navigation.goBack()}
                  style={{
                    height: moderateScale(60),
                    width: moderateScale(30),
                    alignSelf: "center",
                    resizeMode: "contain",
                    marginTop: moderateScale(24),
                    marginRight: moderateScale(15),
                    backgroundColor: "transparent",
                    justifyContent: "flex-start",
                    backgroundColor: "rgba(52, 52, 52, 0.5)"
                  }}
                />
              </TouchableOpacity>
            )}
          </View>

          <KeyboardAwareScrollView>
            <TouchableOpacity
              onPress={() => {
                this.state.editable == true
                  ? this.updateProfileImage()
                  : console.log("OK");
              }}
            >
              <View
                style={{
                  alignItems: "center",
                  justifyContent: "center",
                  marginTop: moderateScale(15)
                }}
              >
                <Image
                  style={styles.avatar}
                  marginTop={2}
                  // source={require("./Assets/BrianImage.jpeg")}

                  source={
                    this.state.imageEditClicked == true
                      ? { uri: profileimageurl }
                      : {
                          uri: BASEURL + "/" + this.state.profileImage
                        }
                  }
                  key={
                    this.state.imageEditClicked == true
                      ?  profileimageurl 
                      : 
                          BASEURL + "/" + this.state.profileImage
                        
                  } 
                  onPress={this.chooseFile}
                  // source={{ uri: profileimageurl }}
                  resizeMode="cover"
                />
                <View
                  style={{
                    flexDirection: "row",
                    backgroundColor: "green",
                    justifyContent: "space-between"
                  }}
                >
                  <Image
                    style={styles.avatar2}
                    source={require("./Assets/take_profile_picture.png")}
                    onPress={this.chooseFile}
                    resizeMode="cover"
                  />
                  <Image
                    style={styles.avatar3}
                    source={require("./Assets/adjust_picture.png")}
                    onPress={this.chooseFile}
                    resizeMode="cover"
                  />
                </View>
              </View>
            </TouchableOpacity>

            <View
              style={{
                alignContent: "center",
                alignSelf: "center",
                marginTop: 10,
                // backgroundColor: "red",
                width: 250,
                // backgroundColor: "transparent",
                // borderColor: "#7098AA",
                // borderWidth: 1,

                height: moderateScale(40)
              }}
            >
              <TextInput
                placeholder="First Name"
                placeholderTextColor="#C0C0C0"
                onChangeText={nameSatate => {
                  this.setState({ nameSatate });
                }}
                editable={this.state.editable}
                value={this.state.nameSatate}
                returnKeyType={"next"}
                style={{
                  marginLeft: 10,
                  marginRight: 10,
                  textAlign: "center",
                  alignSelf: "center",
                  position: "absolute",
                  // backgroundColor: "red",
                  marginTop: 5,
                  color: "white",
                  fontSize: moderateScale(28),
                  width: moderateScale(280),
                  fontFamily: "SegoeMonoBoot"
                }}
              />
            </View>
            <View
              style={{
                flexDirection: "row",

                justifyContent: "flex-start",
                marginLeft: 30,
                marginRight: 30,
                marginTop: moderateScale(30)
              }}
            >
              <Image
                style={{
                  marginLeft: 0,
                  marginRight: 0,
                  width: moderateScale(35),
                  height: moderateScale(35)
                }}
                source={require("./Assets/phone_number.png")}
                onPress={this.chooseFile}
                resizeMode="contain"
              />

              <TextInput
                placeholder="Phone Number"
                placeholderTextColor="#C0C0C0"
                editable={this.state.editable}
                value={this.state.phoneNumber}
                onChangeText={text => this.handleCardNumber(text)}
                returnKeyType={"next"}
                style={{
                  marginLeft: moderateScale(45),
                  marginRight: 10,
                  textAlign: "left",
                  alignItems: "flex-start",
                  position: "absolute",
                  // backgroundColor: "red",
                  marginTop: 5,
                  color: "white",
                  fontWeight: "200",
                  fontSize: moderateScale(20),
                  width: moderateScale(280),
                  fontFamily: "SegoeUI-Light"
                }}
              />
            </View>

            <View
              style={{
                flexDirection: "row",

                justifyContent: "flex-start",
                marginLeft: 30,
                marginRight: 30,
                marginTop: moderateScale(20)
              }}
            >
              <Image
                style={{
                  marginLeft: 0,
                  marginRight: 0,
                  width: moderateScale(35),
                  height: moderateScale(35)
                }}
                source={require("./Assets/location.png")}
                onPress={this.chooseFile}
                resizeMode="contain"
              />

              <TextInput
                placeholder="Location"
                placeholderTextColor="#C0C0C0"
                onChangeText={location => {
                  this.setState({ location });
                }}
                editable={this.state.editable}
                value={this.state.location}
                returnKeyType={"next"}
                style={{
                  marginLeft: moderateScale(45),
                  marginRight: 10,
                  textAlign: "left",
                  alignItems: "flex-start",
                  position: "absolute",
                  // backgroundColor: "red",
                  marginTop: 5,
                  color: "white",
                  fontWeight: "200",
                  fontSize: moderateScale(20),
                  width: moderateScale(280),
                  fontFamily: "SegoeUI-Light"
                }}
              />
            </View>
            <View
              style={{
                flexDirection: "row",
                // backgroundColor: "red",
                justifyContent: "flex-start",
                marginLeft: 30,
                marginRight: 30,
                marginTop: moderateScale(20)
              }}
            >
              <Image
                style={{
                  marginLeft: 0,
                  marginRight: 0,
                  width: moderateScale(35),
                  height: moderateScale(35)
                }}
                source={require("./Assets/birthday.png")}
                onPress={this.chooseFile}
                resizeMode="contain"
              />

              <DatePicker
                label="datepic"
                style={{
                  marginLeft: moderateScale(45),
                  marginRight: 10,
                  textAlign: "left",
                  alignItems: "flex-start",
                  position: "absolute",
                  // backgroundColor: "red",
                  marginTop: 6,
                  color: "white",
                  fontWeight: "100",
                  fontSize: moderateScale(20),
                  fontFamily: "SegoeUI-Light"
                }}
                date={this.state.dob}
                placeholder="Date of Birth"
                //minDate={}
                maxDate={age}
                mode="date"
                disabled={!this.state.editable}
                format="MM-DD-YYYY"
                showIcon={false}
                cancelBtnText="Cancel"
                confirmBtnText="Done"
                container={{
                  marginTop: 0
                }}
                // disableUnderline = {true}
                // InputProps={{
                //   disableUnderline: true
                // }}
                customStyles={{
                  dateInput: {
                    alignItems: "flex-start",
                    borderWidth: 0,
                    borderBottomWidth: 0,
                    paddingLeft: 5,
                    width: "100%"
                    //padding: 5
                  },
                  dateText: {
                    width: "100%",
                    placeholderTextColor: "#C0C0C0",
                    color: "#FEFEFE",
                    fontSize: moderateScale(20),
                    fontWeight: "200"
                  },
                  placeholderText: {
                    color: "#C0C0C0",
                    fontSize: moderateScale(20)
                  },
                  color: "red"
                }}
                onDateChange={dob => {
                  this.setState({ dob: dob });
                }}
                returnKeyType={"next"}
                //autoFocus={true}
                //onSubmitEditing={() => { this.focusTheField('gender'); }}
              />
            </View>

            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                marginTop:
                  Dimensions.get("window").height >= 812
                    ? moderateScale(30)
                    : moderateScale(5)
                // backgroundColor: "orange"
              }}
            >
              <TouchableOpacity onPress={() => this.loginOptions("4")}>
                <Image
                  source={require("./Assets/continue.png")}
                  //size={20}
                  //color="gray"
                  marginTop={5}
                  marginLeft={5}
                  resizeMode="contain"
                  style={styles.imageSize2}
                />
              </TouchableOpacity>
              <Text
                style={{
                  fontSize: moderateScale(25),
                  color: "#FFFFFF",
                  textAlign: "left",
                  fontWeight: "200",
                  marginTop: moderateScale(5)
                }}
              >
                {" "}
                continue
              </Text>
            </View>
          </KeyboardAwareScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ecf0f1"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  avatar: {
    width: moderateScale(200),
    height: moderateScale(200),
    borderRadius: moderateScale(100),
    borderWidth: moderateScale(8),
    borderColor: "#7098AA",
    alignItems: "center"
  },

  avatar2: {
    width: 25,
    height: 22,
    marginTop: -30,
    marginLeft: -20,
    position: "absolute"
  },
  avatar3: {
    width: 25,
    height: 22,
    marginTop: -30,
    marginLeft: 20,
    position: "absolute"
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 10
  },
  sharingtext: {
    color: "#AA2E2E",
    backgroundColor: "#fff",
    borderRadius: 14,
    borderColor: "#AA2E2E",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 18,
    width: 130,
    height: 30,
    alignItems: "center",
    alignContent: "center",
    textAlignVertical: "center"
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover" // or 'stretch'
  },
  footerWrapperlikeCommentShare: {
    flexWrap: "nowrap",

    flexDirection: "row",
    justifyContent: "space-evenly",
    marginLeft: 30,
    marginRight: 30
  },
  separatorlikeShare: {
    flex: 0,
    borderWidth: 1,
    borderColor: "#ECEDED"
  },
  imageSize2: {
    width: moderateScale(60),
    height: moderateScale(60),
    padding: 5,
    borderRadius: moderateScale(10)
  }
});

export default withNavigation(Profile);
