/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Alert,
  Button,
  Image,
  TextInput,
  TouchableOpacity
} from "react-native";
import Login from "./Login";
import { createStackNavigator, createAppContainer } from "react-navigation";
import Profile from "./Profile";
import EulaSetting from "./Eula";
import homePage from "./Home";
import watch from "./watching";
import report from "./Report";
import reportImage from "./ReportImage";
import search from "./Search";
import nearBy from "./NearBy";
import incident from "./Incidents";
import signUp from "./SignUp";
import SignUp from "./SignUp";
import forgot from "./Forgot";
import changePassword from "./ChangePassword";
import otpScreen from "./Otp";
import APISingletonClass, { CrossCheckkey } from "./API";
import AsyncStorage from "@react-native-community/async-storage";
import LocationClass from "./Location";

import {
  fromLeft,
  zoomIn,
  zoomOut,
  fromRight,
  fromBottom,
  fromTop
} from "react-navigation-transitions";
import OtpAdd from "./Otp";
//import EditDetailsInfo from "./EditDetailsInfo"
var screenname = "";
export default class App extends Component {
  state = {
    isLogged: "",
    responseWait: false
  };
  getData = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Pic", value);

        //     if (key == "FullName") {

        screenname = value;
        this.setState({ isLogged: value });
        console.warn("CrossCheck", screenname);
        console.log("CrossCheck", screenname);
        //   }

        //this.GetProfile();
      }
      this.setState({ responseWait: true });
    } catch (e) {
      // error reading value
    }
  };
  componentWillMount() {
    // this.getToken();
  }
  componentDidMount() {
    this.getData("ScreenShow");
  }
  // async getToken() {
  //   try {
  //     AsyncStorage.getItem("sessionId", (err, item) => {
  //       console.log("Saved APp js file " + item);
  //       if (item != null) {
  //         this.setState({ isLogged: true });
  //       } else {
  //         this.setState({ isLogged: false });
  //       }
  //       this.setState({ responseWait: true });
  //     });
  //     console.log("Final SignedIn::" + this.state.isLogged);
  //   } catch (error) {
  //     console.log("Something went wrong " + error);
  //   }
  // }

  render() {
    /* if(this.state.signedIn==false){
      return <AppContainer/>
    } */
    console.log("Final SignedIn2::" + this.state.isLogged);
    if (this.state.responseWait) {
      if (this.state.isLogged == "Profile") {
        return <AppContainer1 />;
      } else if (this.state.isLogged == "Home") {
        return <AppContainer2 />;
      } else {
        return <AppContainer />;
      }
    } else {
      return null;
    }
  }
}

const handleCustomTransition = ({ scenes }) => {
  const prevScene = scenes[scenes.length - 2];
  const nextScene = scenes[scenes.length - 1];

  // Custom transitions go there
  if (
    nextScene.route.routeName === "ProfileNav1" &&
    prevScene.route.routeName === "homePageBbf"
  ) {
    return fromBottom();
  }
  if (
    nextScene.route.routeName === "ProfileNav1" &&
    prevScene.route.routeName === "watchingVC"
  ) {
    return fromBottom();
  }
  if (
    nextScene.route.routeName === "ProfileNav1" &&
    prevScene.route.routeName === "reportVC"
  ) {
    return fromBottom();
  }

  if (
    nextScene.route.routeName === "ProfileNav1" &&
    prevScene.route.routeName === "reportImageVC"
  ) {
    return fromBottom();
  }
  if (
    nextScene.route.routeName === "ProfileNav1" &&
    prevScene.route.routeName === "searchVC"
  ) {
    return fromBottom();
  }
  if (
    nextScene.route.routeName === "ProfileNav1" &&
    prevScene.route.routeName === "nearByVC"
  ) {
    return fromBottom();
  }

  if (
    nextScene.route.routeName === "ProfileNav1" &&
    prevScene.route.routeName === "incidentsVC"
  ) {
    return fromBottom();
  }
  if (prevScene && prevScene.route.routeName === "Home") {
    return zoomIn();
  } else if (
    prevScene &&
    prevScene.route.routeName === "ScreenB" &&
    nextScene.route.routeName === "ScreenC"
  ) {
    return zoomOut();
  } else if (prevScene && prevScene.route.routeName === "homePageBbf") {
    return zoomIn();
  } else if (
    nextScene.route.routeName === "ProfileNav1" &&
    prevScene.route.routeName === "Home"
  ) {
    return fromTop();
  }
  return fromRight();
};
// const RootStack2 = createStackNavigator({
//   Home: {
//     screen: Login,
//     navigationOptions: {
//       header: null,
//       headerLeft: null
//     }
//   },
//   ProfileNav: {
//     screen: Profile,
//     navigationOptions: {
//       header: null,
//       headerLeft: null,
//       gesturesEnabled: false
//     }
//   },
//   ProfileNav1: {
//     screen: Profile,
//     navigationOptions: {
//       header: null,
//       headerLeft: null,
//       gesturesEnabled: false
//     }
//   },
//   OtpScreen: {
//     screen: OtpAdd,
//     navigationOptions: {
//       header: null,
//       headerLeft: null,
//       gesturesEnabled: false
//     }
//   },
//   eulaSetting: {
//     screen: EulaSetting,
//     navigationOptions: {
//       header: null,
//       headerLeft: null,
//       gesturesEnabled: false
//     }
//   },
//   eulaSetting1: {
//     screen: EulaSetting,
//     navigationOptions: {
//       header: null,
//       headerLeft: null,
//       gesturesEnabled: false
//     }
//   },
//   homePageBbf: {
//     screen: homePage,
//     navigationOptions: {
//       header: null,
//       headerLeft: null,
//       gesturesEnabled: false
//     }
//   },
//   watchingVC: {
//     screen: watch,
//     navigationOptions: {
//       header: null,
//       headerLeft: null,
//       gesturesEnabled: false
//     }
//   },
//   reportVC: {
//     screen: report,
//     navigationOptions: {
//       header: null,
//       headerLeft: null,
//       gesturesEnabled: false
//     }
//   },
//   reportImageVC: {
//     screen: reportImage,
//     navigationOptions: {
//       header: null,
//       headerLeft: null,
//       gesturesEnabled: false
//     }
//   },
//   searchVC: {
//     screen: search,
//     navigationOptions: {
//       header: null,
//       headerLeft: null,
//       gesturesEnabled: false
//     }
//   },
//   nearByVC: {
//     screen: nearBy,
//     navigationOptions: {
//       header: null,
//       headerLeft: null,
//       gesturesEnabled: false
//     }
//   },
//   incidentsVC: {
//     screen: incident,
//     navigationOptions: {
//       header: null,
//       headerLeft: null,
//       gesturesEnabled: false,
//       modalPresentationStyle: "overCurrentContext"
//     }
//   },
//   signUp: {
//     screen: SignUp,
//     navigationOptions: {
//       header: null,
//       headerLeft: null,
//       gesturesEnabled: false,
//       modalPresentationStyle: "overCurrentContext"
//     }
//   }
// });
const RootStack = createStackNavigator(
  {
    signUp1: {
      screen: SignUp,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false,
        modalPresentationStyle: "overCurrentContext"
      }
    },
    OtpScreen: {
      screen: OtpAdd,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    Home: {
      screen: Login,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    ProfileNav: {
      screen: Profile,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    ProfileNav1: {
      screen: Profile,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    eulaSetting: {
      screen: EulaSetting,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    eulaSetting1: {
      screen: EulaSetting,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    homePageBbf: {
      screen: homePage,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    watchingVC: {
      screen: watch,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    reportVC: {
      screen: report,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    reportImageVC: {
      screen: reportImage,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    searchVC: {
      screen: search,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    nearByVC: {
      screen: nearBy,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    incidentsVC: {
      screen: incident,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false,
        modalPresentationStyle: "overCurrentContext"
      }
    },
    signUp: {
      screen: SignUp,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false,
        modalPresentationStyle: "overCurrentContext"
      }
    },
    forGot: {
      screen: forgot,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false,
        modalPresentationStyle: "overCurrentContext"
      }
    },
    ChangePass: {
      screen: changePassword,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false,
        modalPresentationStyle: "overCurrentContext"
      }
    },
    LocationCall: {
      screen: LocationClass,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false,
        modalPresentationStyle: "overCurrentContext"
      }
    }
  },
  {
    initialRouteName: "Home",
    transitionConfig: nav => handleCustomTransition(nav)
  }
);
const RootStack1 = createStackNavigator(
  {
    signUp1: {
      screen: SignUp,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false,
        modalPresentationStyle: "overCurrentContext"
      }
    },
    OtpScreen: {
      screen: OtpAdd,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    Home: {
      screen: Login,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    ProfileNav: {
      screen: Profile,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    ProfileNav1: {
      screen: Profile,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    eulaSetting: {
      screen: EulaSetting,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    eulaSetting1: {
      screen: EulaSetting,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    homePageBbf: {
      screen: homePage,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    watchingVC: {
      screen: watch,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    reportVC: {
      screen: report,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    reportImageVC: {
      screen: reportImage,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    searchVC: {
      screen: search,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    nearByVC: {
      screen: nearBy,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    incidentsVC: {
      screen: incident,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false,
        modalPresentationStyle: "overCurrentContext"
      }
    },
    signUp: {
      screen: SignUp,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false,
        modalPresentationStyle: "overCurrentContext"
      }
    },
    forGot: {
      screen: forgot,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false,
        modalPresentationStyle: "overCurrentContext"
      }
    },
    ChangePass: {
      screen: changePassword,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false,
        modalPresentationStyle: "overCurrentContext"
      }
    },

    LocationCall: {
      screen: LocationClass,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false,
        modalPresentationStyle: "overCurrentContext"
      }
    }
  },
  {
    initialRouteName: "ProfileNav",

    transitionConfig: nav => handleCustomTransition(nav)
  }
);
const RootStack2 = createStackNavigator(
  {
    signUp1: {
      screen: SignUp,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false,
        modalPresentationStyle: "overCurrentContext"
      }
    },
    OtpScreen: {
      screen: OtpAdd,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    Home: {
      screen: Login,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    ProfileNav: {
      screen: Profile,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    ProfileNav1: {
      screen: Profile,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    eulaSetting: {
      screen: EulaSetting,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    eulaSetting1: {
      screen: EulaSetting,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    homePageBbf: {
      screen: homePage,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    watchingVC: {
      screen: watch,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    reportVC: {
      screen: report,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    reportImageVC: {
      screen: reportImage,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    searchVC: {
      screen: search,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    nearByVC: {
      screen: nearBy,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false
      }
    },
    incidentsVC: {
      screen: incident,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false,
        modalPresentationStyle: "overCurrentContext"
      }
    },
    signUp: {
      screen: SignUp,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false,
        modalPresentationStyle: "overCurrentContext"
      }
    },
    forGot: {
      screen: forgot,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false,
        modalPresentationStyle: "overCurrentContext"
      }
    },
    ChangePass: {
      screen: changePassword,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false,
        modalPresentationStyle: "overCurrentContext"
      }
    },
    LocationCall: {
      screen: LocationClass,
      navigationOptions: {
        header: null,
        headerLeft: null,
        gesturesEnabled: false,
        modalPresentationStyle: "overCurrentContext"
      }
    }
  },
  {
    initialRouteName: "homePageBbf",

    transitionConfig: nav => handleCustomTransition(nav)
  }
);
// getData = async key => {
//   try {
//     const value = await AsyncStorage.getItem(key);
//     if (value !== null) {
//       // value previously stored
//       console.log("Got Pic", value);

//       //     if (key == "FullName") {

//       screenname = value;
//       console.warn("CrossCheck", screenname);
//       console.log("CrossCheck", screenname);
//       //   }

//       //this.GetProfile();
//     }
//   } catch (e) {
//     // error reading value
//   }
// };
//const HomeContainer = createAppContainer(RootStack2);
const AppContainer = createAppContainer(RootStack);
const AppContainer1 = createAppContainer(RootStack1);
const AppContainer2 = createAppContainer(RootStack2);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ecf0f1"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 10
  },
  sharingtext: {
    color: "#AA2E2E",
    backgroundColor: "#fff",
    borderRadius: 14,
    borderColor: "#AA2E2E",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 18,
    width: 130,
    height: 30,
    alignItems: "center",
    alignContent: "center",
    textAlignVertical: "center"
  }
});
