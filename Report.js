import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  ScrollView,
  NavigatorIOS,
  Alert,
  Image,
  
  ToastAndroid,
  ActivityIndicator,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  StatusBar,
  Switch,
  FlatList
} from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";

import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import AsyncStorage from "@react-native-community/async-storage";

import { withNavigation } from "react-navigation";
//import { Switch } from "react-native-switch";
import ToggleSwitch from "toggle-switch-react-native";
var ImagePicker = require("react-native-image-picker");
import APISingletonClass, { BASEURL } from "./API";
import { ScaledSheet } from "react-native-size-matters";
import { scale, verticalScale, moderateScale } from "react-native-size-matters";

class Report extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      loading: false,
      animating: false,
      loginInProcess: false,
      switchedBool: false,
      filePath: [],
      ProfilePic: "",

      wtachList: [
        {
          name: "Brian J Virag Law Offices",
          avatar_url: "https://randomuser.me/api/portraits/med/men/77.jpg",
          subtitle: "16400 Ventura Blvd,Encino,CA"
        },
        {
          name: "ABCD Hotel Chain",
          avatar_url: "https://randomuser.me/api/portraits/med/men/77.jpg",
          subtitle: "1234 Ventura Blvd,Encino,CA"
        },
        {
          name: "EFGH Hotel Chain",
          avatar_url: "https://randomuser.me/api/portraits/med/men/77.jpg",
          subtitle: "5678 Ventura Blvd,Encino,CA"
        }
      ]
    };
  }

  loginOptions() {
    this.props.navigation.navigate("ProfileNav", {
      DataHomeComment: ""
    });
  }
  getData = async key => {
    try {
      const value = await AsyncStorage.getItem(key);
      if (value !== null) {
        // value previously stored
        console.log("Got Pic", value);
        this.setState({
          ProfilePic: value
        });

        //this.GetProfile();
      }
    } catch (e) {
      // error reading value
    }
  };
  componentDidMount() {
    this.getData("profilePhoto");
  }
  optionPress(option) {
    if (option == "0") {
    } else if (option == "1") {
    } else if (option == "2") {
    } else if (option == "3") {
    }
  }
  render() {
    return (
      <View style={{ backgroundColor: "gray" }}>
        
        <StatusBar hidden={true} />
        <ImageBackground
          source={require("./Assets/main_background.png")}
          size={28}
          // borderRadius={15}
          style={{
            height: Dimensions.get("window").height,
            width: Dimensions.get("window").width,
            resizeMode: "contain"
          }}
        >
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <View
              style={{
                flexDirection: "row",
                width: scale(230),
                left: 15,
                backgroundColor: "transparent"
              }}
            >
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image
                  source={require("./Assets/back.png")}
                  size={28}
                  borderRadius={15}
                  //onPress={this.props.navigation.goBack()}
                  style={{
                    height: moderateScale(60),
                    width: moderateScale(30),
                    alignSelf: "center",
                    resizeMode: "contain",
                    marginTop: moderateScale(24),
                    backgroundColor: "transparent",
                    justifyContent: "flex-start",
                    backgroundColor: "transparent"
                  }}
                />
              </TouchableOpacity>
              <Text
                style={{
                  fontSize: moderateScale(25),
                  color: "#FEFEFE",
                  textAlign: "center",
                  fontWeight: "200",
                  marginTop: moderateScale(40),
                  marginLeft: 0,
                  marginRight: 20,
                  backgroundColor: "transparent",
                  fontFamily: "SegoeUI-Light"
                }}
              >
                {" "}
                R E P O R T
              </Text>
            </View>

            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("ProfileNav1", {
                  DataHomeComment: ""
                })
              }
            >
              <View
                style={{
                  marginRight: 10,
                  marginTop: moderateScale(20),
                  alignSelf: "flex-end",
                  width: moderateScale(75),
                  height: moderateScale(75),
                  borderRadius: moderateScale(40),
                  borderWidth: moderateScale(3),
                  borderColor: "#525353",
                  backgroundColor: "transparent"
                }}
              >
                <Image
                  style={styles.avatar}
                  // source={require("./Assets/BrianImage.jpeg")}
                  source={{
                    uri: BASEURL + "/" + this.state.ProfilePic
                  }}
                  onPress={this.chooseFile}
                  resizeMode="cover"
                />
              </View>
            </TouchableOpacity>
          </View>
          <KeyboardAwareScrollView>
          <Text
            style={{
              fontSize: moderateScale(23),
              color: "#FEFEFE",
              textAlign: "center",
              fontWeight: "200",
              marginTop: moderateScale(20),
              marginLeft: 20,
              marginRight: 20,
              fontFamily: "SegoeUI-Light"
            }}
          >
            {" "}
            Please connect My Bed Bug
          </Text>
          <Text
            style={{
              fontSize: moderateScale(25),
              color: "#FEFEFE",
              textAlign: "center",
              fontWeight: "200",
              marginTop: moderateScale(2),
              marginLeft: 20,
              marginRight: 20,
              fontFamily: "SegoeUI-Light"
            }}
          >
            {" "}
            Finder Wand to your phone.
          </Text>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate("reportImageVC", {
                DataHomeComment: ""
              })
            }
          >
            <Image
              source={require("./Assets/stick.png")}
              size={28}
              borderRadius={15}
              style={{
                height: moderateScale(100),
                width: moderateScale(100),

                alignSelf: "center",
                justifyContent: "center",
                resizeMode: "contain",
                marginTop: moderateScale(40)
              }}
            />
          </TouchableOpacity>
          <View
            style={{
              height: 1,
              backgroundColor: "#C0C0C0",
              marginLeft: 20,
              marginRight: 20,
              marginTop: moderateScale(50)
            }}
          />

          <View
            style={{
              height: moderateScale(60),
              fontSize: moderateScale(20),
              marginTop: moderateScale(50),
              textAlign: "center",
              backgroundColor: "transparent",
              borderWidth: 3,
              borderColor: "#7098AA",
              height: moderateScale(50),
              borderRadius: moderateScale(35),
              width: scale(280),
              alignItems: "center",
              alignContent: "center",
              alignSelf: "center"
            }}
          >
            <Text
              style={{
                height: moderateScale(60),
                fontSize: moderateScale(29),
                alignItems: "center",
                width: scale(150),
                textAlign: "center",
                backgroundColor: "transparent",
                height: verticalScale(50),
                color: "white",
                fontWeight: "200",
                fontFamily: "SegoeUI-Light"
              }}
              onPress={() => console.log("hello")}
              color="black"
              accessibilityLabel="Tap on Me"
            >
              Buy Now!
            </Text>
          </View>
          <Text
            style={{
              fontSize: moderateScale(25),
              color: "#FEFEFE",
              textAlign: "center",
              fontWeight: "200",
              marginTop: moderateScale(30),
              marginLeft: 20,
              marginRight: 20,
              fontFamily: "SegoeUI-Light"
            }}
          >
            {" "}
            If you do not have My Bed Bug Finder Wand,click the blue button to
            buy now!
          </Text>
          </KeyboardAwareScrollView>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ecf0f1"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  input: {
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: "black",
    marginBottom: 10
  },
  sharingtext: {
    color: "#AA2E2E",
    backgroundColor: "#fff",
    borderRadius: 14,
    borderColor: "#AA2E2E",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 18,
    width: 130,
    height: 30,
    alignItems: "center",
    alignContent: "center",
    textAlignVertical: "center"
  },
  backgroundImage: {
    flex: 1,
    resizeMode: "cover" // or 'stretch'
  },
  footerWrapperlikeCommentShare: {
    flexWrap: "nowrap",
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginLeft: 30,
    marginRight: 30
  },
  separatorlikeShare: {
    flex: 0,
    borderWidth: 1,
    borderColor: "#ECEDED"
  },
  imageSize2: {
    width: 60,
    height: 60,
    padding: 5,
    borderRadius: 10
  },
  avatar: {
    width: moderateScale(70),
    height: moderateScale(70),
    borderRadius: moderateScale(35),
    borderWidth: moderateScale(3),
    borderColor: "#7098AA",
    alignItems: "center"
  }
});

export default withNavigation(Report);
